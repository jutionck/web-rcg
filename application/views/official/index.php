<?php
// $ip = $this->input->ip_address();
// if ($this->agent->is_browser()) {
//   $browser = $this->agent->browser() . ' ' . $this->agent->version();
// } elseif ($this->agent->is_mobile()) {
//   $browser = $this->agent->mobile();
// } else {
//   $browser = 'Data user gagal di dapatkan';
// }
// $os      = $this->agent->platform();
// if (!isset($_COOKIE['VISITOR'])) {
//   $duration = time() + 60 * 60 * 24;
//   setcookie('VISITOR', $browser, $duration);
//   $data = [
//     'ip' => $ip,
//     'os' => $os,
//     'browser' => $browser,
//     'date_created' => time()
//   ];

//   $this->db->insert('tr_visitors',  $data);
// }
?>
<div class="intro intro-carousel swiper position-relative">
  <div class="swiper-wrapper">
    <div class="swiper-slide carousel-item-a intro-item bg-image" style="background-image: url(<?= base_url('assets/official') ?>/img/slide-1.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">Subheading Here
                    78345
                  </p>
                  <h1 class="intro-title mb-4 ">
                    <span class="color-b">RCG </span> Group
                    <br> Lorem Ipsum
                  </h1>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">Hubungi Kami</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide carousel-item-a intro-item bg-image" style="background-image: url(<?= base_url('assets/official') ?>/img/slide-2.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">Subheading Here
                    <br> 78345
                  </p>
                  <h1 class="intro-title mb-4 ">
                    <span class="color-b">RCG </span> Group
                    <br> Lorem Ipsum
                  </h1>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">Hubungi Kami</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-slide carousel-item-a intro-item bg-image" style="background-image: url(<?= base_url('assets/official') ?>/img/slide-3.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <p class="intro-title-top">Subheading Here
                    <br> 78345
                  </p>
                  <h1 class="intro-title mb-4 ">
                    <span class="color-b">RCG </span> Group
                    <br> Lorem Ipsum
                  </h1>
                  <p class="intro-subtitle intro-price">
                    <a href="#"><span class="price-a">Hubungi Kami</span></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="swiper-pagination"></div>
</div>

<main id="main">
  <section class="section-services section-t8">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Our Services</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="bi bi-cart"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Design</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="bi bi-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="bi bi-calendar4-week"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Architect</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Mauris blandit
                aliquet elit, eget tincidunt
                nibh pulvinar a.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="bi bi-calendar4-week"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-header-c d-flex">
              <div class="card-box-ico">
                <span class="bi bi-card-checklist"></span>
              </div>
              <div class="card-title-c align-self-center">
                <h2 class="title-c">Construction</h2>
              </div>
            </div>
            <div class="card-body-c">
              <p class="content-c">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class="card-footer-c">
              <a href="#" class="link-c link-icon">Read more
                <span class="bi bi-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-about section-t8">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 position-relative">
          <div class="about-img-box">
            <iframe width="100%" height="500px" src="https://www.youtube.com/embed/HGl75kurxok" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
          </div>
          <div class="sinse-box">
            <h3 class="sinse-title" style="color: white;">RCG Group
              <br> Since 2015
            </h3>
            <p style="color: white;">Design & Architect</p>
          </div>
        </div>
        <div class="col-md-12 section-t8 position-relative">
          <div class="row">
            <div class="col-md-6 col-lg-5">
              <img src="<?= base_url('assets/official') ?>/img/about-2.jpg" alt="" class="img-fluid">
            </div>
            <div class="col-lg-2  d-none d-lg-block position-relative">
              <div class="title-vertical d-flex justify-content-start">
                <span>Design Architect Interior</span>
              </div>
            </div>
            <div class="col-md-6 col-lg-5 section-md-t3">
              <div class="title-box-d">
                <h3 class="title-d">RCG
                  <span class="color-d">Group</span>
                  <br> Design. Architect. Build.
              </div>
              <p class="color-text-a">
                RCG Group adalah Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget
                consectetur sed, convallis
                at tellus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum
                ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit
                neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
              </p>
              <p class="color-text-a">
                Tambahan Deskripsi di sini sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.
                Mauris blandit aliquet
                elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur sed,
                convallis at tellus.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Project Locations <br>RCG Group</h1>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Detail</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Maps
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>

  <section class="section-about">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 position-relative justify-content-between ">
          <div class="about-img-box">
            <img src="<?= base_url('assets/official') ?>/img/570-5700216_peta-indonesia-png.png" alt="" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>

  <section class="section-property section-t8">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Latest Works</h2>
            </div>
            <div class="title-link">
              <a href="property-grid.html">All Property
                <span class="bi bi-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div id="property-carousel" class="swiper">
        <div class="swiper-wrapper">
          <div class="carousel-item-b swiper-slide">
            <div class="card-box-a card-shadow">
              <div class="img-box-a">
                <img src="<?= base_url('assets/official') ?>/img/property-6.jpg" alt="" class="img-a img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-overlay-a-content">
                  <div class="card-header-a">
                    <h2 class="card-title-a">
                      <a href="#">Jagakarsa
                        <br /> Jakarta Selatan</a>
                    </h2>
                  </div>
                  <div class="card-body-a">
                    <div class="price-box d-flex">
                      <span class="price-a">IDR 700.000.000</span>
                    </div>
                    <a href="property-single.html" class="link-a">Click here to view
                      <span class="bi bi-chevron-right"></span>
                    </a>
                  </div>
                  <div class="card-footer-a">
                    <ul class="card-info d-flex justify-content-around">
                      <li>
                        <h4 class="card-info-title">Area</h4>
                        <span>340m
                          <sup>2</sup>
                        </span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Beds</h4>
                        <span>2</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Baths</h4>
                        <span>4</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Garages</h4>
                        <span>1</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-b swiper-slide">
            <div class="card-box-a card-shadow">
              <div class="img-box-a">
                <img src="<?= base_url('assets/official') ?>/img/property-3.jpg" alt="" class="img-a img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-overlay-a-content">
                  <div class="card-header-a">
                    <h2 class="card-title-a">
                      <a href="#">Jagakarsa
                        <br /> Jakarta Selatan</a>
                    </h2>
                  </div>
                  <div class="card-body-a">
                    <div class="price-box d-flex">
                      <span class="price-a">IDR 700.000.000</span>
                    </div>
                    <a href="property-single.html" class="link-a">Click here to view
                      <span class="bi bi-chevron-right"></span>
                    </a>
                  </div>
                  <div class="card-footer-a">
                    <ul class="card-info d-flex justify-content-around">
                      <li>
                        <h4 class="card-info-title">Area</h4>
                        <span>340m
                          <sup>2</sup>
                        </span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Beds</h4>
                        <span>2</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Baths</h4>
                        <span>4</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Garages</h4>
                        <span>1</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-b swiper-slide">
            <div class="card-box-a card-shadow">
              <div class="img-box-a">
                <img src="<?= base_url('assets/official') ?>/img/property-7.jpg" alt="" class="img-a img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-overlay-a-content">
                  <div class="card-header-a">
                    <h2 class="card-title-a">
                      <a href="#">Jagakarsa
                        <br /> Jakarta Selatan</a>
                    </h2>
                  </div>
                  <div class="card-body-a">
                    <div class="price-box d-flex">
                      <span class="price-a">IDR 700.000.000</span>
                    </div>
                    <a href="property-single.html" class="link-a">Click here to view
                      <span class="bi bi-chevron-right"></span>
                    </a>
                  </div>
                  <div class="card-footer-a">
                    <ul class="card-info d-flex justify-content-around">
                      <li>
                        <h4 class="card-info-title">Area</h4>
                        <span>340m
                          <sup>2</sup>
                        </span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Beds</h4>
                        <span>2</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Baths</h4>
                        <span>4</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Garages</h4>
                        <span>1</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-b swiper-slide">
            <div class="card-box-a card-shadow">
              <div class="img-box-a">
                <img src="<?= base_url('assets/official') ?>/img/property-10.jpg" alt="" class="img-a img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-overlay-a-content">
                  <div class="card-header-a">
                    <h2 class="card-title-a">
                      <a href="property-single.html">204 Montal
                        <br /> South Bela Two</a>
                    </h2>
                  </div>
                  <div class="card-body-a">
                    <div class="price-box d-flex">
                      <span class="price-a">rent | $ 12.000</span>
                    </div>
                    <a href="property-single.html" class="link-a">Click here to view
                      <span class="bi bi-chevron-right"></span>
                    </a>
                  </div>
                  <div class="card-footer-a">
                    <ul class="card-info d-flex justify-content-around">
                      <li>
                        <h4 class="card-info-title">Area</h4>
                        <span>340m
                          <sup>2</sup>
                        </span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Beds</h4>
                        <span>2</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Baths</h4>
                        <span>4</span>
                      </li>
                      <li>
                        <h4 class="card-info-title">Garages</h4>
                        <span>1</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="propery-carousel-pagination carousel-pagination"></div>
    </div>
  </section>

  <section class="section-agents section-t8">
    <div class="container">
      <div class="row">
        <div class="four col-md-4">
          <div class="counter-box colored">
            <i class="fa fa-thumbs-o-up"></i>
            <span class="counter">2147</span>
            <p>Happy Customers</p>
          </div>
        </div>
        <div class="four col-md-4">
          <div class="counter-box">
            <i class="fa fa-building-o"></i>
            <span class="counter">3154</span>
            <p>Jumlah Projek</p>
          </div>
        </div>
        <div class="four col-md-4">
          <div class="counter-box">
            <i class="fa fa-suitcase" aria="true"></i>
            <span class="counter">120000</span>
            <p>Nilai Projek</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-news section-t8">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Latest News</h2>
            </div>
            <div class="title-link">
              <a href="blog-grid.html">All News
                <span class="bi bi-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div id="news-carousel" class="swiper">
        <div class="swiper-wrapper">
          <div class="carousel-item-c swiper-slide">
            <div class="card-box-b card-shadow news-box">
              <div class="img-box-b">
                <img src="<?= base_url('assets/official') ?>/img/post-2.jpg" alt="" class="img-b img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-header-b">
                  <div class="card-category-b">
                    <a href="#" class="category-b">Tag Here</a>
                  </div>
                  <div class="card-title-b">
                    <h2 class="title-2">
                      <a href="blog-single.html">Write title here
                      </a>
                    </h2>
                  </div>
                  <div class="card-date">
                    <span class="date-b">29 Jan. 2023</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-c swiper-slide">
            <div class="card-box-b card-shadow news-box">
              <div class="img-box-b">
                <img src="<?= base_url('assets/official') ?>/img/post-5.jpg" alt="" class="img-b img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-header-b">
                  <div class="card-category-b">
                    <a href="#" class="category-b">Tag Here</a>
                  </div>
                  <div class="card-title-b">
                    <h2 class="title-2">
                      <a href="blog-single.html">Write title here
                      </a>
                    </h2>
                  </div>
                  <div class="card-date">
                    <span class="date-b">29 Jan. 2023</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-c swiper-slide">
            <div class="card-box-b card-shadow news-box">
              <div class="img-box-b">
                <img src="<?= base_url('assets/official') ?>/img/post-7.jpg" alt="" class="img-b img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-header-b">
                  <div class="card-category-b">
                    <a href="#" class="category-b">Tag Here</a>
                  </div>
                  <div class="card-title-b">
                    <h2 class="title-2">
                      <a href="blog-single.html">Write title here
                      </a>
                    </h2>
                  </div>
                  <div class="card-date">
                    <span class="date-b">29 Jan. 2023</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-c swiper-slide">
            <div class="card-box-b card-shadow news-box">
              <div class="img-box-b">
                <img src="<?= base_url('assets/official') ?>/img/post-3.jpg" alt="" class="img-b img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-header-b">
                  <div class="card-category-b">
                    <a href="#" class="category-b">Tag Here</a>
                  </div>
                  <div class="card-title-b">
                    <h2 class="title-2">
                      <a href="#">Write title here</a>
                    </h2>
                  </div>
                  <div class="card-date">
                    <span class="date-b">29 Jan. 2023</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="news-carousel-pagination carousel-pagination"></div>
    </div>
  </section>

  <section class="section-testimonials section-t8 nav-arrow-a">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Testimonials</h2>
            </div>
          </div>
        </div>
      </div>
      <div id="testimonial-carousel" class="swiper">
        <div class="swiper-wrapper">
          <div class="carousel-item-a swiper-slide">
            <div class="testimonials-box">
              <div class="row">
                <div class="col-sm-12 col-md-6">
                  <div class="testimonial-img">
                    <img src="<?= base_url('assets/official') ?>/img/testimonial-1.jpg" alt="" class="img-fluid">
                  </div>
                </div>
                <div class="col-sm-12 col-md-6">
                  <div class="testimonial-ico">
                    <i class="bi bi-chat-quote-fill"></i>
                  </div>
                  <div class="testimonials-content">
                    <p class="testimonial-text">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                      debitis hic ber quibusdam
                      voluptatibus officia expedita corpori.
                    </p>
                  </div>
                  <div class="testimonial-author-box">
                    <img src="<?= base_url('assets/official') ?>/img/mini-testimonial-1.jpg" alt="" class="testimonial-avatar">
                    <h5 class="testimonial-author">Asrun Burhan</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="carousel-item-a swiper-slide">
            <div class="testimonials-box">
              <div class="row">
                <div class="col-sm-12 col-md-6">
                  <div class="testimonial-img">
                    <img src="<?= base_url('assets/official') ?>/img/testimonial-2.jpg" alt="" class="img-fluid">
                  </div>
                </div>
                <div class="col-sm-12 col-md-6">
                  <div class="testimonial-ico">
                    <i class="bi bi-chat-quote-fill"></i>
                  </div>
                  <div class="testimonials-content">
                    <p class="testimonial-text">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                      debitis hic ber quibusdam
                      voluptatibus officia expedita corpori.
                    </p>
                  </div>
                  <div class="testimonial-author-box">
                    <img src="<?= base_url('assets/official') ?>/img/mini-testimonial-2.jpg" alt="" class="testimonial-avatar">
                    <h5 class="testimonial-author">Kevin Sanjaya</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="testimonial-carousel-pagination carousel-pagination"></div>
    </div>
  </section>
</main>
<main id="main">
  <section class="breadcrumbs">
    <div class="container">
      <ol>
        <li><a href="<?= base_url() ?>">Home</a></li>
        <li>Artikel</li>
      </ol>
    </div>
  </section>

  <?php if ($articles->num_rows() > 0) : ?>
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-8 entries">
            <?php foreach ($articles->result() as $row) :  ?>
              <article class="entry">
                <div class="entry-img">
                  <img src="<?= base_url('assets/official/img/blog/' . $row->image) ?>" alt="<?= $row->slug ?> class=" img-fluid">
                </div>
                <h2 class="entry-title">
                  <a href="<?= base_url('artikel/baca/' . $row->slug) ?>"><?= $row->title ?></a>
                </h2>
                <div class="entry-meta">
                  <ul>
                    <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="#"><?= $row->author ?></a></li>
                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="#"><time datetime="2020-01-01"><?= indonesianDate($row->created_at, 'D MMMM Y, HH:mm') ?></time></a></li>
                    <li class="d-flex align-items-center"><i class="bi bi-eye"></i> <a href="#">Dilihat <?= $row->hits ?> kali</a></li>
                  </ul>
                </div>
                <div class="entry-content">
                  <?= ellipsize($row->description, 300, 1, "....") ?>
                  <div class="read-more">
                    <a href="<?= base_url('artikel/baca/' . $row->slug) ?>">Selengkapnya</a>
                  </div>
                </div>
              </article><!-- End blog entry -->
            <?php endforeach ?>
            <div class="blog-pagination">
              <ul class="justify-content-center">
                <!-- <li><a href="#">1</a></li>
              <li class="active"><a href="#">2</a></li>
              <li><a href="#">3</a></li> -->
                <?= $pagination ?>
              </ul>
            </div>
          </div><!-- End blog entries list -->

          <div class="col-lg-4">
            <div class="sidebar">
              <h3 class="sidebar-title">Search</h3>
              <div class="sidebar-item search-form">
                <form action="<?= base_url('artikel/pencarian') ?>" method="GET">
                  <input type="text" name="judul">
                  <button type="submit"><i class="bi bi-search"></i></button>
                </form>
              </div><!-- End sidebar search formn-->
              <h3 class="sidebar-title">Artikel Lainnya</h3>
              <div class="sidebar-item recent-posts">
                <?php foreach ($articles->result() as $row) : ?>
                  <div class="post-item clearfix">
                    <img src="<?= base_url('assets/official/img/blog/' . $row->image) ?>" alt="<?= $row->slug ?>">
                    <h4>
                      <a href="<?= base_url('artikel/baca/' . $row->slug) ?>">
                        <?= $row->title ?>
                      </a>
                    </h4>
                    <time datetime="2020-01-01">
                      <?= indonesianDate($row->created_at, 'D MMMM Y, HH:mm') ?>
                    </time>
                  </div>
                <?php endforeach ?>
              </div><!-- End sidebar recent posts-->
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php else : ?>
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-md-12">
            <h5>Belum ada artikel</h5>
          </div>
        </div>
      </div>
    </section>
  <?php endif ?>
</main>
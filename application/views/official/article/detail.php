<main id="main">
  <section class="breadcrumbs">
    <div class="container">
      <ol>
        <li><a href="<?= base_url() ?>">Beranda</a></li>
        <li><a href="<?= base_url('artikel') ?>">Artikel</a></li>
        <li>Baca</li>
      </ol>
    </div>
  </section>

  <section id="blog" class="blog">
    <div class="container" data-aos="fade-up">
      <div class="row">
        <div class="col-lg-8 entries">
          <article class="entry entry-single">
            <div class="entry-img">
              <img src="<?= base_url('assets/official/img/blog/' . $article->image) ?>" alt="<?= $article->slug ?>" class="img-fluid">
            </div>
            <h2 class="entry-title">
              <a href="#"><?= $article->title ?></a>
            </h2>
            <div class="entry-meta">
              <ul>
                <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="#"><?= $article->author ?></a></li>
                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="#">
                    <?= indonesianDate($article->created_at, 'dddd, D MMMM Y, HH:mm') ?>
                  </a></li>
                <li class="d-flex align-items-center"><i class="bi bi-eye"></i> <a href="#">Dilihat <?= $article->hits ?> kali</a></li>
              </ul>
            </div>
            <div class="entry-content"><?= $article->description ?></div>
          </article><!-- End blog entry -->
        </div><!-- End blog entries list -->

        <div class="col-lg-4">
          <div class="sidebar">
            <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
              <form action="<?= base_url('artikel/pencarian') ?>" method="GET">
                <input type="text" name="judul">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div><!-- End sidebar search formn-->
            <h3 class="sidebar-title">Artikel Lainnya</h3>
            <div class="sidebar-item recent-posts">
              <?php foreach ($articles as $row) : ?>
                <div class="post-item clearfix">
                  <img src="<?= base_url('assets/official/img/blog/' . $row->image) ?>" alt="<?= $row->slug ?>">
                  <h4>
                    <a href="<?= base_url('artikel/baca/' . $row->slug) ?>">
                      <?= $row->title ?>
                    </a>
                  </h4>
                  <time datetime="2020-01-01">
                    <?= indonesianDate($row->created_at, 'D MMMM Y, HH:mm') ?>
                  </time>
                </div>
              <?php endforeach ?>
            </div><!-- End sidebar recent posts-->
          </div><!-- End sidebar -->
        </div><!-- End blog sidebar -->
      </div>
    </div>
  </section><!-- End Blog Single Section -->
</main>
<main id="main">
  <section class="breadcrumbs">
    <div class="container">
      <ol>
        <li><a href="<?= base_url() ?>">Beranda</a></li>
        <li><a href="<?= base_url('artikel') ?>">Artikel</a></li>
        <li>Pencarian</li>
      </ol>
    </div>
  </section>

  <section id="blog" class="blog">
    <div class="container" data-aos="fade-up">
      <div class="row">
        <div class="col-lg-8 entries">
          <h4>Pencarian berita</h4>
          <h5 class="mb-4">Untuk kata kunci " <span style="font-weight:bold; font-style:italic"><?= ucwords($this->input->get('judul'));  ?></span> "</h5>
          <?php if ($articles->num_rows() > 0) :
            foreach ($articles->result() as $row) :  ?>
              <article class="entry">
                <div class="entry-img">
                  <img src="<?= base_url('assets/official/img/blog/' . $row->image) ?>" alt="<?= $row->slug ?>" class="img-fluid">
                </div>
                <h2 class="entry-title">
                  <a href="<?= base_url('artikel/baca/' . $row->slug) ?>"><?= $row->title ?></a>
                </h2>
                <div class="entry-meta">
                  <ul>
                    <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="#"><?= $row->author ?></a></li>
                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="#"><time datetime="2020-01-01"><?= indonesianDate($row->created_at, 'D MMMM Y, HH:mm') ?></time></a></li>
                    <li class="d-flex align-items-center"><i class="bi bi-eye"></i> <a href="#">Dilihat <?= $row->hits ?> kali</a></li>
                  </ul>
                </div>
                <div class="entry-content">
                  <?= ellipsize($row->description, 300, 1, "....") ?>
                  <div class="read-more">
                    <a href="<?= base_url('artikel/baca/' . $row->slug) ?>">Selengkapnya</a>
                  </div>
                </div>
              </article>
            <?php endforeach;
          else : ?>
            <div class="col-lg-12">
              <h4><span style="font-style:italic;color:red">Hasil Pencarian Tidak Di temukan, Cobalah dengan kata kunci yang lain</span></h4>
            </div>
          <?php endif; ?>
        </div>

        <div class="col-lg-4">
          <div class="sidebar">
            <h3 class="sidebar-title">Search</h3>
            <div class="sidebar-item search-form">
              <form action="<?= base_url('artikel/pencarian') ?>" method="GET">
                <input type="text" name="judul">
                <button type="submit"><i class="bi bi-search"></i></button>
              </form>
            </div>
            <h3 class="sidebar-title">Artikel Lainnya</h3>
            <div class="sidebar-item recent-posts">
              <?php foreach ($recents as $row) : ?>
                <div class="post-item clearfix">
                  <img src="<?= base_url('assets/official/img/blog/' . $row->image) ?>" alt="<?= $row->slug ?>">
                  <h4>
                    <a href="<?= base_url('artikel/baca/' . $row->slug) ?>">
                      <?= $row->title ?>
                    </a>
                  </h4>
                  <time datetime="2020-01-01">
                    <?= indonesianDate($row->created_at, 'D MMMM Y, HH:mm') ?>
                  </time>
                </div>
              <?php endforeach ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
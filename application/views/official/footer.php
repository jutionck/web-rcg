<section class="section-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4">
        <div class="widget-a">
          <div class="w-header-a">
            <h3 class="w-title-a text-brand">RCG Group</h3>
          </div>
          <div class="w-body-a">
            <p class="w-text-a color-text-a">
              Deskripsi Singkat RCG Group Enim minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat duis
              sed aute irure.
            </p>
          </div>
          <div class="w-footer-a">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="#">
                  <i class="bi bi-facebook" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="bi bi-twitter" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="bi bi-instagram" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="bi bi-linkedin" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-4 section-md-t3">
        <div class="widget-a">
          <div class="w-header-a">
            <h3 class="w-title-a text-brand">Member</h3>
          </div>
          <div class="w-body-a">
            <div class="w-body-a">
              <ul class="list-unstyled">
                <li class="item-list-a">
                  <i class="bi bi-chevron-right"></i> <a href="#">Ruang Rehat</a>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-chevron-right"></i> <a href="#">Cipta Griya Kantor</a>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-chevron-right"></i> <a href="#">RCG Jogja</a>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-chevron-right"></i> <a href="#">RCG Bandung</a>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-chevron-right"></i> <a href="#">Rehat Indoestri</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-4 section-md-t3">
        <div class="widget-a">
          <div class="w-header-a">
            <h3 class="w-title-a text-brand">Contact Us</h3>
          </div>
          <div class="w-body-a">
            <div class="w-footer-a">
              <ul class="list-unstyled">
                <li class="item-list-a">
                  <i class="bi bi-whatsapp"></i> <span class="color-text-a">021-53169637</span>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-envelope-open"></i> <span class="color-text-a">cs@rcggroup.com</span>
                </li>
                <li class="item-list-a">
                  <i class="bi bi-buildings"></i> <span class="color-text-a">Jl. Kp. Pager Haur, Pagedangan, Kec. Pagedangan, Kabupaten Tangerang, Banten 15331</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav class="nav-footer">
          <ul class="list-inline">
            <li class="list-inline-item">
              <a href="<?= site_url() ?>">Home</a>
            </li>
            <li class="list-inline-item">
              <a href="<?= site_url('about') ?>">About</a>
            </li>
            <li class="list-inline-item">
              <a href="<?= site_url('portofolio') ?>">Property</a>
            </li>
            <li class="list-inline-item">
              <a href="<?= site_url('news') ?>">News</a>
            </li>
            <li class="list-inline-item">
              <a href="<?= site_url('contact') ?>">Contact</a>
            </li>
          </ul>
        </nav>
        <div class="socials-a">
          <ul class="list-inline">
            <li class="list-inline-item">
              <a href="#">
                <i class="bi bi-facebook" aria-hidden="true"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="bi bi-twitter" aria-hidden="true"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="bi bi-instagram" aria-hidden="true"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="bi bi-linkedin" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="copyright-footer">
          <p class="copyright color-text-a">
            &copy; Copyright
            <span class="color-a">EstateAgency</span> All Rights Reserved.
          </p>
        </div>

      </div>
    </div>
  </div>
</footer>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Jadwal Sholat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="text-align: center;"><iframe src="https://www.jadwalsholat.org/adzan/ajax.row.php?id=265″ frameborder=" 0″ width="220″ height=" 220″></iframe></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="preloader"></div>
<button data-toggle="modal" data-target="#exampleModal" style="border: 0;" class="btn-shalat d-flex align-items-center justify-content-center"><i class="bi bi-clock"></i></button>
<a href="#" class="btn-whatsapp d-flex align-items-center justify-content-center"><i class="bi bi-whatsapp"></i></a>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<script src="<?= base_url('assets/official') ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/official') ?>/vendor/swiper/swiper-bundle.min.js"></script>
<script src="<?= base_url('assets/official') ?>/vendor/php-email-form/validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>

<script src="<?= base_url('assets/official') ?>/js/main.js"></script>
<script>
  $(document).ready(function() {
    $('.counter').each(function() {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 4000,
        easing: 'swing',
        step: function(now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  });
</script>
<script>
  $('#exampleModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus')
  })
</script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>RCG Group Indonesia</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="<?= base_url('assets/official') ?>/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  <link href="<?= base_url('assets/official') ?>/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/official') ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/official') ?>/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url('assets/official') ?>/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href="<?= base_url('assets/official') ?>/css/style.css" rel="stylesheet">
</head>

<body>
  <div class="click-closed"></div>
  <div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Search Here</h3>
    </div>
    <span class="close-box-collapse right-boxed bi bi-x"></span>
    <div class="box-collapse-wrap form">
      <form class="form-a">
        <div class="row">
          <div class="col-md-12 mb-2">
            <div class="form-group">
              <label class="pb-2" for="Type">Keyword</label>
              <input type="text" class="form-control form-control-lg form-control-a" placeholder="Keyword">
            </div>
          </div>
          <div class="col-md-6 mb-2">
            <div class="form-group mt-3">
              <label class="pb-2" for="Type">Opsi 1</label>
              <select class="form-control form-select form-control-a" id="Type">
                <option>RCG Jogja</option>
                <option>RCG Bandung</option>
                <option>Ruang Rehat</option>
                <option>Rehat Indonesia</option>
              </select>
            </div>
          </div>
          <div class="col-md-6 mb-2">
            <div class="form-group mt-3">
              <label class="pb-2" for="city">Opsi 2</label>
              <select class="form-control form-select form-control-a" id="city">
                <option>All City</option>
                <option>Alabama</option>
                <option>Arizona</option>
                <option>California</option>
                <option>Colorado</option>
              </select>
            </div>
          </div>

          <div class="col-md-12 mt-5">
            <button type="submit" class="btn btn-b">Search</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="<?= site_url() ?>"><span class="color-b">RCG </span>Group</a>

      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link <?= ($this->uri->segment(1)  === null ? 'active' : '') ?>" href="<?= site_url() ?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= ($this->uri->segment(1)  === 'about' ? 'active' : '') ?>" href="<?= site_url('about') ?>">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= ($this->uri->segment(1)  === 'portofolio' ? 'active' : '') ?>" href="<?= site_url('portofolio') ?>">Portfolio</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle <?= ($this->uri->segment(1)  === 'member' ? 'active' : '') ?>" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Members</a>
            <div class="dropdown-menu">
              <a class="dropdown-item <?= ($this->uri->segment(2)  === 'ruang-rehat' ? 'active' : '') ?>" href="<?= site_url('member/ruang-rehat') ?>">Ruang Rehat</a>
              <a class="dropdown-item <?= ($this->uri->segment(2)  === 'cipta-griya-kontraktor' ? 'active' : '') ?>" href="<?= site_url('member/cipta-griya-kontraktor') ?>">Cipta Griya Kontraktor</a>
              <a class="dropdown-item <?= ($this->uri->segment(2)  === 'rcg-jogja' ? 'active' : '') ?>" href="<?= site_url('member/rcg-jogja') ?>">RCG Jogja</a>
              <a class="dropdown-item <?= ($this->uri->segment(2)  === 'rcg-bandung' ? 'active' : '') ?>" href="<?= site_url('member/rcg-bandung') ?>">RCG Bandung</a>
              <a class="dropdown-item <?= ($this->uri->segment(2)  === 'rcg-indoestri' ? 'active' : '') ?>" href="<?= site_url('member/rcg-indoestri') ?>">Rehat Indoestri</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= ($this->uri->segment(1)  === 'roadmap' ? 'active' : '') ?>" href="<?= site_url('roadmap') ?>">Roadmap</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= ($this->uri->segment(1)  === 'contact' ? 'active' : '') ?>" href="<?= site_url('contact') ?>">Contact</a>
          </li>
        </ul>
      </div>
      <button type="button" class="btn btn-b-n navbar-toggle-box navbar-toggle-box-collapse" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01">
        <i class="bi bi-search"></i>
      </button>

    </div>
  </nav>
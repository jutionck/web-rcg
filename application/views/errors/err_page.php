<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>404 - beasiswabilampung.com</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="<?= base_url('assets/backoffice/') ?>img/favicon.png" rel="icon">
  <link href="<?= base_url('assets/backoffice/') ?>img/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="<?= base_url('assets/backoffice/') ?>css/style.css" rel="stylesheet">
</head>

<body>

  <main>
    <div class="container">

      <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
        <h1>404</h1>
        <h2>Oopps sepertinya kamu tersesat.</h2>
        <img src="<?= base_url('assets/') ?>img/not-found.svg" class="img-fluid py-5" alt="Page Not Found">
        <div class="credits">
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </section>
    </div>
  </main>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <script src="<?= base_url('assets/backoffice/') ?>vendor/apexcharts/apexcharts.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/chart.js/chart.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/echarts/echarts.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/quill/quill.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/simple-datatables/simple-datatables.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/tinymce/tinymce.min.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>vendor/php-email-form/validate.js"></script>
  <script src="<?= base_url('assets/backoffice/') ?>js/main.js"></script>

</body>

</html>
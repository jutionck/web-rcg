<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $university->id; ?>">
            <input type="hidden" class="form-control" name="university_logo_old" value="<?= set_value('university_logo') ? set_value('university_logo') : $university->university_logo; ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="name" value="<?= set_value('name') ? set_value('name') : $university->name; ?>">
                <div class="invalid-feedback">
                  <?= form_error('name'); ?>
                </div>
                <label for="floatingName">Kampus</label>
              </div>
            </div>
            <div class="col-md-12">
              <img src="<?= base_url('assets/official/img/university/' . $university->university_logo) ?>" class="img-fluid" alt="<?= $university->university_logo ?>">
            </div>
            <div class="col-md-12">
              <input type="file" class="form-control" id="image-source" placeholder="Your Name" name="university_logo">
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
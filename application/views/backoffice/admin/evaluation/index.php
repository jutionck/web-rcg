<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <div class="row">
          <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-4">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="university" required>
                    <?php
                    if (!isset($_GET['university'])) {
                      echo '<option selected="">Kampus</option>';
                    }
                    ?>
                    <?php foreach ($university as $row) : ?>
                      <option value="<?= $row->id ?>"><?= $row->name ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                  <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url('backoffice/admin/hasil_evaluasi') ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                      <a href="<?= site_url('backoffice/export/rekap_hasil_evaluasi/' . $this->input->get('university') . '?tahun=' . $this->input->get('tahun')) ?>" class="btn btn-success" style="margin-top: 25px;">Export</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Universitas</th>
                  <th scope="col">IPK</th>
                  <th scope="col">Cuti</th>
                  <th scope="col">Menerima Beasiswa</th>
                  <th scope="col">Keterangan</th>
                  <th scope="col">Hasil</th>
                  <th scope="col">Transkrip</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($evaluations as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $row->year ?></td>
                    <td><?= $row->name ?></td>
                    <td><?= $row->university ?></td>
                    <td><?= $row->gpa_evaluation ?></td>
                    <td>
                      <?php
                      if ($row->paid_leave == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      if ($row->receive_another_scholarship == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td><?= $row->description ?></td>
                    <td>
                      <?php
                      if ($row->evaluation_result == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <a href="<?= base_url('assets/backoffice/upload/transcript/evaluation/' . $row->transcript_file) ?>" class="btn btn-success btn-sm">Unduh</a>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
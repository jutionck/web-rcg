<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Hasil Wawancara</li>
      <li class="breadcrumb-item active"> <?= $sub_title ?></li>
    </ol>
  </nav>
</div>
<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <p><?= $desc ?></p>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : @$evaluation->id; ?>">
            <input type="hidden" name="applicants_id" value="<?= $pendaftar->id ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="name" value="<?= set_value('name') ? set_value('name') : $pendaftar->name; ?>" readonly>
                <label for="floatingName">Nama</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="npm" value="<?= set_value('npm') ? set_value('npm') : $pendaftar->npm; ?>" readonly>
                <label for="floatingName">NPM</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="faculty" value="<?= set_value('faculty') ? set_value('faculty') : $pendaftar->faculty; ?>" readonly>
                <label for="floatingName">Fakultas</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="major" value="<?= set_value('major') ? set_value('major') : $pendaftar->major; ?>" readonly>
                <label for="floatingName">Jurusan</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('gpa_evaluation') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="gpa_evaluation" value="<?= set_value('gpa_evaluation') ? set_value('gpa_evaluation') : @$evaluation->gpa_evaluation; ?>">
                <div class="invalid-feedback">
                  <?= form_error('gpa_evaluation'); ?>
                </div>
                <label for="floatingName">IPK Setelah Evaluasi</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <select class="form-select" id="floatingSelect" aria-label="State" name="paid_leave">
                  <option selected="">Pilih</option>
                  <?php foreach ($option as $row) :
                    if (@$evaluation->paid_leave == $row->answer) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->answer' $selected>$row->answer</option>";
                  endforeach ?>
                </select>
                <label for="floatingTextarea">Cuti Akademik/Lulus</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <select class="form-select" id="floatingSelect" aria-label="State" name="receive_another_scholarship">
                  <option selected="">Pilih</option>
                  <?php foreach ($option as $row) :
                    if (@$evaluation->receive_another_scholarship == $row->answer) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->answer' $selected>$row->answer</option>";
                  endforeach ?>
                </select>
                <label for="floatingTextarea">Menerima Beasiswa Lain</label>
              </div>
            </div>
            <div class="col-12">
              <div class="form-floating">
                <textarea class="form-control" placeholder="Deskripsi website" id="floatingTextarea" style="height: 100px;" name="description"><?= @$evaluation->description ?></textarea>
                <label for="floatingTextarea">Keterangan</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <select class="form-select" id="floatingSelect" aria-label="State" name="evaluation_result">
                  <option selected="">Pilih</option>
                  <?php foreach ($option as $row) :
                    if (@$evaluation->evaluation_result == $row->answer) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->answer' $selected>$row->answer</option>";
                  endforeach ?>
                </select>
                <label for="floatingTextarea">Hasil Evaluasi</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="file" class="form-control" id="image-source" placeholder="Your Name" name="transcript_file" value="<?= set_value('transcript_file') ? set_value('transcript_file') : @$evaluation->transcript_file; ?>">
                <label for="floatingTextarea">Transkrip</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-danger">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
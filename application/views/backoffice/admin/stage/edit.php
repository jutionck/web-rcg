<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $stage->id; ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="title" value="<?= set_value('title') ? set_value('title') : $stage->title; ?>">
                <div class="invalid-feedback">
                  <?= form_error('title'); ?>
                </div>
                <label for="floatingName">Alur</label>
              </div>
            </div>
            <div class="col-12">
              <div class="form-floating">
                <textarea class="form-control <?= form_error('description') ? 'is-invalid' : ''; ?>" placeholder="Deskripsi website" id="floatingTextarea" style="height: 100px;" name="description"><?= $stage->description ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('description'); ?>
                </div>
                <label for="floatingTextarea">Deskripsi</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
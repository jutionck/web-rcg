<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <p><?= $desc ?></p>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $about->id; ?>">
            <input type="hidden" class="form-control" name="desc_image_old" value="<?= set_value('desc_image') ? set_value('desc_image') : $about->desc_image; ?>">
            <input type="hidden" class="form-control" name="sub_title_image_old" value="<?= set_value('sub_title_image') ? set_value('sub_title_image') : $about->sub_title_image; ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="title" value="<?= set_value('title') ? set_value('title') : $about->title; ?>">
                <div class="invalid-feedback">
                  <?= form_error('title'); ?>
                </div>
                <label for="floatingName">Judul</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="sub_title" value="<?= set_value('sub_title') ? set_value('sub_title') : $about->sub_title; ?>">
                <label for="floatingName">Sub Judul</label>
              </div>
            </div>
            <div class="col-12">
              <div class="form-floating">
                <textarea class="form-control" placeholder="Deskripsi website" id="floatingTextarea" style="height: 100px;" name="description"><?= $about->description ?></textarea>
                <label for="floatingTextarea">Deskripsi</label>
              </div>
            </div>
            <div class="col-md-6">
              <img src="<?= base_url('assets/official/img/' . $about->desc_image) ?>" class="img-fluid" alt="<?= $about->title ?>">
            </div>
            <div class="col-md-6">
              <img src="<?= base_url('assets/official/img/' . $about->sub_title_image) ?>" class="img-fluid" alt="<?= $about->title ?>">
            </div>
            <div class="col-md-6">
              <input type="file" class="form-control" id="image-source" placeholder="Your Name" name="desc_image" value="<?= set_value('desc_image') ? set_value('desc_image') : $about->desc_image; ?>">
            </div>
            <div class="col-md-6">
              <input type="file" class="form-control" id="image-source" placeholder="Your Name" name="sub_title_image" value="<?= set_value('sub_title_image') ? set_value('sub_title_image') : $about->sub_title_image; ?>">
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
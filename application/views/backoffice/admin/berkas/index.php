<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <a href="<?= base_url('backoffice/admin/berkas/tambah'); ?>" class="btn btn-primary"><i class="bi bi-plus-lg"></i> Tambah</a>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Judul</th>
                  <th scope="col">File</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($berkas as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $row->title ?></td>
                    <td><a href="<?= base_url('assets/backoffice/upload/interview_file/' . $row->file) ?>"><?= $row->file ?></a></td>
                    <td>
                      <div class="btn-group">
                        <a href=" <?= base_url('backoffice/admin/berkas/edit/' . $row->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                        <button type="button" class="btn btn-danger delete-berkas" data-id="<?= $row->id ?>"><i class="bi bi-trash"></i></button>
                      </div>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
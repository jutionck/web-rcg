<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <p><?= $desc ?></p>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('email') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="Your Name" name="email">
                <div class="invalid-feedback">
                  <?= form_error('email'); ?>
                </div>
                <label for="floatingName">Email</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="name">
                <label for="floatingName">Nama</label>
              </div>
            </div>
            <div class="col-12">
              <div class="form-floating">
                <textarea class="form-control" placeholder="Deskripsi website" id="floatingTextarea" style="height: 100px;" name="acara"></textarea>
                <label for="floatingTextarea">Kegiatan</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-success">Kirim Email</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <div class="row">
          <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-4">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="university" required>
                    <?php
                    if (!isset($_GET['university'])) {
                      echo '<option selected="">Kampus</option>';
                    }
                    ?>
                    <?php foreach ($university as $row) : ?>
                      <option value="<?= $row->id ?>"><?= $row->name ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                  <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url($redirect) ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                      <a href="<?= site_url('backoffice/export/rekap_data_kegiatan/' . $this->input->get('university') . '?tahun=' . $this->input->get('tahun')) ?>" class="btn btn-success" style="margin-top: 25px;">Export</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama Mahasiswa</th>
                  <th scope="col">Kampus</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($activities as $activity) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $activity->name ?></td>
                    <td><?= $activity->university ?></td>
                    <td>
                      <a href="<?= base_url($redirect) ?>/detail/<?= $activity->applicants_id ?>" class="btn btn-warning">DETAIL</a>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
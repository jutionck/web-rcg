<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <a href="<?= base_url($redirect); ?>" class="btn btn-primary"><i class="bi bi-arrow-left"></i> Kembali</a>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table class="table">
            <tr>
              <td width="200px">Nama Mahasiswa</td>
              <td>: <strong><?= $activity->name ?></strong></td>
            </tr>
            <tr>
              <td width="200px">Kampus</td>
              <td>: <strong><?= $activity->university ?></strong></td>
            </tr>
          </table>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Kegiatan</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Kehadiran</th>
                  <th scope="col">Resume/Alasan</th>
                  <th scope="col">Bukti</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($activities as $activity) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $activity->activity_name ?></td>
                    <td><?= $activity->activity_type ?></td>
                    <td><?= indonesianDate($activity->activity_date, 'D MMMM Y') ?></td>
                    <td>
                      <?= ($activity->attendance == 'Hadir') ? '<span class="badge bg-warning text-dark">Hadir</span>' : '<span class="badge bg-danger">Tidak Hadir</span>' ?>
                    </td>
                    <td>
                      <?php
                      if ($activity->attendance == "Hadir") {
                        echo '<strong>Resume: </strong><br>';
                        echo $activity->activity_resume;
                      } else {
                        echo '<strong>Alasan: </strong><br>';
                        echo $activity->activity_reason;
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      if ($activity->attendance == "Hadir") {
                        if (strContains($activity->activity_file, '.jpg') || strContains($activity->activity_file, '.png') || strContains($activity->activity_file, '.jpeg')) { ?>
                          <img src="<?= base_url('assets/backoffice/upload/activity/') ?><?= $activity->activity_file ?>" alt="<?= $activity->activity_file ?>" width="100px">
                        <?php } else { ?>
                          <a href="<?= base_url('assets/backoffice/upload/activity/') ?><?= $activity->activity_file ?>"><?= $activity->activity_file ?></a>
                      <?php }
                      } ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
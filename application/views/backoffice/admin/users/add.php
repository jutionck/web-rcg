<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('username') ? 'is-invalid' : ''; ?>" id="floatingName" name="username" placeholder="Your Name">
                <div class="invalid-feedback">
                  <?= form_error('username'); ?>
                </div>
                <label for="floatingName">Username</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" name="email" placeholder="Your Name">
                <label for="floatingName">Email</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="password" class="form-control <?= form_error('password') ? 'is-invalid' : ''; ?>" id="floatingName" name="password" placeholder="Your Name">
                <div class="invalid-feedback">
                  <?= form_error('password'); ?>
                </div>
                <label for="floatingName">Password</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="password" class="form-control <?= form_error('passconf') ? 'is-invalid' : ''; ?>" id="floatingName" name="passconf" placeholder="Your Name">
                <div class="invalid-feedback">
                  <?= form_error('passconf'); ?>
                </div>
                <label for="floatingName">Ulangi Password</label>
              </div>
            </div>
            <div class="col-6">
              <div class="form-floating">
                <select class="form-select <?= form_error('role_id') ? 'is-invalid' : ''; ?>" id="floatingSelect" aria-label="State" name="role_id">
                  <option selected="">Pilih</option>
                  <?php foreach ($roles as $row) : ?>
                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                  <?php endforeach ?>
                </select>
                <div class="invalid-feedback">
                  <?= form_error('role_id'); ?>
                </div>
                <label for="floatingTextarea">Role</label>
              </div>
            </div>
            <div class="col-6">
              <div class="form-floating">
                <select class="form-select <?= form_error('university_id') ? 'is-invalid' : ''; ?>" id="floatingSelect" aria-label="State" name="university_id">
                  <option selected="">Pilih</option>
                  <?php foreach ($university as $row) : ?>
                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                  <?php endforeach ?>
                </select>
                <div class="invalid-feedback">
                  <?= form_error('university_id'); ?>
                </div>
                <label for="floatingTextarea">Kampus</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
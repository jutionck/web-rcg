<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">students</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <a href="<?= base_url('backoffice/admin/master/user/tambah'); ?>" class="btn btn-primary"><i class="bi bi-plus-lg"></i> Tambah</a>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <ul class="nav nav-pills mb-3 mt-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="pills-students-tab" data-bs-toggle="pill" data-bs-target="#pills-students" type="button" role="tab" aria-controls="pills-students" aria-selected="true">Student</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="pills-approver-tab" data-bs-toggle="pill" data-bs-target="#pills-approver" type="button" role="tab" aria-controls="pills-approver" aria-selected="false">Approver</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="pills-opearator-tab" data-bs-toggle="pill" data-bs-target="#pills-opearator" type="button" role="tab" aria-controls="pills-opearator" aria-selected="false">Operator</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="pills-interviewer-tab" data-bs-toggle="pill" data-bs-target="#pills-interviewer" type="button" role="tab" aria-controls="pills-interviewer" aria-selected="false">Interviewer</button>
            </li>
          </ul>
          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-students" role="tabpanel" aria-labelledby="pills-students-tab" tabindex="0">
              <div class="table-responsive">
                <table class="table datatable">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Username</th>
                      <th scope="col">Email</th>
                      <th scope="col">Role</th>
                      <th scope="col">Kampus</th>
                      <th scope="col">Terakhir Akses</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($studentUser as $user) : ?>
                      <tr>
                        <th scope="row"><?= $no++ ?></th>
                        <td><?= $user->username ?></td>
                        <td><?= $user->username; ?> </td>
                        <td><span class="badge rounded-pill bg-warning"><?= $user->role_name ?></span></td>
                        <td><?= $user->university ?></td>
                        <td>
                          <?php if ($user->last_login) : ?>
                            <?= indonesianDate($user->last_login, 'D MMMM Y HH:mm:ss') ?>
                          <?php else :
                            echo '-';
                          endif ?>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href=" <?= base_url('backoffice/admin/master/user/edit/' . $user->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                            <button type="button" class="btn btn-danger delete-user" data-id="<?= $user->id ?>"><i class="bi bi-trash"></i></button>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-approver" role="tabpanel" aria-labelledby="pills-approver-tab" tabindex="0">
            <div class="table-responsive">
                <table class="table datatable">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Username</th>
                      <th scope="col">Email</th>
                      <th scope="col">Role</th>
                      <th scope="col">Kampus</th>
                      <th scope="col">Terakhir Akses</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($approverUser as $user) : ?>
                      <tr>
                        <th scope="row"><?= $no++ ?></th>
                        <td><?= $user->username ?></td>
                        <td><?= $user->email; ?> </td>
                        <td><span class="badge rounded-pill bg-primary"><?= $user->role_name ?></span></td>
                        <td><?= $user->university ?></td>
                        <td>
                          <?php if ($user->last_login) : ?>
                            <?= indonesianDate($user->last_login, 'D MMMM Y HH:mm:ss') ?>
                          <?php else :
                            echo '-';
                          endif ?>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href=" <?= base_url('backoffice/admin/master/user/edit/' . $user->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                            <button type="button" class="btn btn-danger delete-user" data-id="<?= $user->id ?>"><i class="bi bi-trash"></i></button>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-opearator" role="tabpanel" aria-labelledby="pills-opearator-tab" tabindex="0">
              <div class="table-responsive">
                <table class="table datatable">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Username</th>
                      <th scope="col">Email</th>
                      <th scope="col">Role</th>
                      <th scope="col">Kampus</th>
                      <th scope="col">Terakhir Akses</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($operatorUser as $user) : ?>
                      <tr>
                        <th scope="row"><?= $no++ ?></th>
                        <td><?= $user->username ?></td>
                        <td><?= $user->email ?></td>
                        <td><span class="badge rounded-pill bg-success"><?= $user->role_name ?></span></td>
                        <td><?= $user->university ?></td>
                        <td>
                          <?php if ($user->last_login) : ?>
                            <?= indonesianDate($user->last_login, 'D MMMM Y HH:mm:ss') ?>
                          <?php else :
                            echo '-';
                          endif ?>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href=" <?= base_url('backoffice/admin/master/user/edit/' . $user->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                            <button type="button" class="btn btn-danger delete-user" data-id="<?= $user->id ?>"><i class="bi bi-trash"></i></button>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-interviewer" role="tabpanel" aria-labelledby="pills-interviewer-tab" tabindex="0">
            <div class="table-responsive">
                <table class="table datatable">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Username</th>
                      <th scope="col">Role</th>
                      <th scope="col">Terakhir Akses</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($interviewerUser as $user) : ?>
                      <tr>
                        <th scope="row"><?= $no++ ?></th>
                        <td><?= $user->username ?></td>
                        <td><span class="badge rounded-pill bg-dark"><?= $user->role_name ?></span></td>
                        <td>
                          <?php if ($user->last_login) : ?>
                            <?= indonesianDate($user->last_login, 'D MMMM Y HH:mm:ss') ?>
                          <?php else :
                            echo '-';
                          endif ?>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href=" <?= base_url('backoffice/admin/master/user/edit/' . $user->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                            <button type="button" class="btn btn-danger delete-user" data-id="<?= $user->id ?>"><i class="bi bi-trash"></i></button>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");

?>
<div style="text-align: center;">
  <h3>
    REKAPITULASI <br>
    HASIL WAWANCARA MAHASISWA CALON PENERIMA BEASISWA BI REGULER <?= strtoupper($university->name) ?>
  </h3>
</div>
<table border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>NAMA MAHASISWA</th>
      <th>NPM</th>
      <th>JURUSAN</th>
      <th>USIA * <br> (< 23 Tahun) </th>
      <th>IPK * <br> (≥ 3,50)</th>
      <th>SEMESTER <br> (Saat Ini)</th>
      <th>JUMLAH <br> SKS <br>(Saat Ini)</th>
      <th>SURAT <br> KET <br> (SKTM)</th>
      <th>SCORE <br> Data dan <br> Syarat <br> Administrasi</th>
      <th>SCORE <br> Pengetahuan <br> tentang <br> Bank Sentral</th>
      <th>SCORE <br> Pengetahuan <br> Beasiswa BI <br> dan GenBI</th>
      <th>SCORE <br> Pengalaman <br> Organisasi</th>
      <th>SCORE <br> Karya Ilmiah <br> dan Prestasi</th>
      <th>SCORE <br> Motivasi dan <br> Kepribadian</th>
      <th>NILAI <br> AKHIR</th>
      <th>REKOMENDASI</th>
      <th>KETERANGAN</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= $row->npm ?></td>
        <td><?= $row->major ?></td>
        <td><?= calculateAge($row->birth_date) ?></td>
        <td><?= $row->gpa ?></td>
        <td><?= $row->semester ?></td>
        <td><?= $row->sks ?></td>
        <td><?= ($row->sktm != null) ? 'Ya' : 'Tidak' ?></td>
        <td><?= likertScala(ceil(($row->gpa_score + $row->administration_score)/2)) ?></td>
        <td><?= likertScala($row->bank_central_score) ?></td>
        <td><?= likertScala($row->genbi_score) ?></td>
        <td><?= likertScala($row->organization_score) ?></td>
        <td><?= likertScala($row->scientific_work_score) ?></td>
        <td><?= likertScala($row->motivation_score) ?></td>
        <td><?= ceil((likertScala(ceil(($row->gpa_score + $row->administration_score)/2)) + likertScala($row->bank_central_score) + likertScala($row->genbi_score) + likertScala($row->organization_score) + likertScala($row->scientific_work_score) + likertScala($row->motivation_score))/6) ?></td>
        <td><?= ($row->recomendation != null) ? 'REKOMENDASI' : '-' ?></td>
        <td><?= $row->note ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");

?>
<div style="text-align: center;">
  <h3>
    REKAPITULASI <br>
    HASIL WAWANCARA AKHIR MAHASISWA CALON PENERIMA BEASISWA REGULER PERGURUAN TINGGI NEGERI <br>
    <?= $university->name ?>
  </h3>
  <p><?= indonesianDate(@$row->interview_date, 'dddd, D MMMM Y') ?></p>
</div>
<table border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>NAMA MAHASISWA</th>
      <th>USIA * <br> (< 23 Tahun) </th>
      <th>IPK * <br> (≥ 3,50)</th>
      <th>SEMESTER <br> (Saat Ini)</th>
      <th>JUMLAH <br> SKS <br>(Saat Ini)</th>
      <th>SURAT <br> KET <br> (SKTM)</th>
      <th>SCORE <br> IPK</th>
      <th>CATATAN</th>
      <th>SCORE <br> Data dan <br> Syarat <br> Administrasi</th>
      <th>CATATAN</th>
      <th>SCORE <br> Pengetahuan <br> tentang <br> Bank Sentral</th>
      <th>CATATAN</th>
      <th>SCORE <br> Pengetahuan <br> Beasiswa BI <br> dan GenBI</th>
      <th>CATATAN</th>
      <th>SCORE <br> Pengalaman <br> Organisasi</th>
      <th>CATATAN</th>
      <th>SCORE <br> Karya Ilmiah <br> dan Prestasi</th>
      <th>CATATAN</th>
      <th>SCORE <br> Motivasi dan <br> Kepribadian</th>
      <th>CATATAN</th>
      <th>NILAI <br> AKHIR</th>
      <th>REKOMENDASI</th>
      <th>KETERANGAN</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= calculateAge($row->birth_date) ?></td>
        <td><?= $row->gpa ?></td>
        <td><?= $row->semester ?></td>
        <td><?= $row->sks ?></td>
        <td><?= $row->sktm ?></td>
        <td><?= $row->gpa_score ?></td>
        <td><?= $row->gpa_note ?></td>
        <td><?= $row->administration_score ?></td>
        <td><?= $row->administration_note ?></td>
        <td><?= $row->bank_central_score ?></td>
        <td><?= $row->bank_central_note ?></td>
        <td><?= $row->genbi_score ?></td>
        <td><?= $row->genbi_note ?></td>
        <td><?= $row->organization_score ?></td>
        <td><?= $row->organization_note ?></td>
        <td><?= $row->scientific_work_score ?></td>
        <td><?= $row->scientific_work_note ?></td>
        <td><?= $row->motivation_score ?></td>
        <td><?= $row->motivation_note ?></td>
        <td><?= $row->result_score ?></td>
        <td><?= $row->recomendation ?></td>
        <td><?= $row->note ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
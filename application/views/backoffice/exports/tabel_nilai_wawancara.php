<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");
?>
<html>
  <head>
  <style>
      .line-th > th {
        border: 1px solid black;
        background-color: rgb(155, 211, 155);
      }

      .line-tbody > tr > td {
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
  <table>
      <thead>
        <tr>
          <th colspan="15">
            TABEL NILAI WAWANCARA <br />SELEKSI MAHASISWA CALON PENERIMA
            BEASISWA BANK INDONESIA REGULER PERGURUAN TINGGI NEGERI <?= strtoupper($row->university) ?>
          </th>
        </tr>
        <tr>
          <th colspan="15"><small> Tanggal, <?= date('d m Y') ?> </small></th>
        </tr>
        <tr>
          <th>1</th>
          <th style="text-align: left">Nama Mahasiswa</th>
          <td colspan="5" style="text-align: left">: <?= $row->name ?></td>
        </tr>
        <tr>
          <th>2</th>
          <th style="text-align: left">No Induk Mahasiswa</th>
          <td colspan="5" style="text-align: left">: <?= $row->npm ?></td>
        </tr>
        <tr>
          <th>3</th>
          <th style="text-align: left">Angkatan Tahun</th>
          <td colspan="5" style="text-align: left">: -</td>
        </tr>
        <tr>
          <th>4</th>
          <th style="text-align: left">Fakultas</th>
          <td colspan="5" style="text-align: left">: <?= $row->faculty ?></td>
        </tr>
        <tr>
          <th>5</th>
          <th style="text-align: left">Program Studi</th>
          <td colspan="5" style="text-align: left">: <?= $row->major ?></td>
        </tr>
        <tr class="line-th">
          <th rowspan="2">No</th>
          <th rowspan="2">PERSYARATAN DAN ITEM PENILAIAN</th>
          <th rowspan="2">JAWABAN</th>
          <th colspan="7">SCORE PENILAIAN</th>
          <th rowspan="2">BATAS <br />ATAS<br />SCORE</th>
          <th rowspan="2">NILAI</th>
          <th rowspan="2">BOBOT</th>
          <th rowspan="2">NILAI <br />AKHIR</th>
          <th rowspan="2">KETERANGAN</th>
        </tr>
        <tr class="line-th">
          <th>
            TIDAK<br />SESUAI<br /><small><small>( 10 - 49) </small></small>
          </th>
          <th>
            KURANG<br />SESUAI<br /><small><small>( 50 - 59) </small></small>
          </th>
          <th>
            AGAK<br />SESUAI<br /><small><small>( 60 - 69) </small></small>
          </th>
          <th>
            CUKUP<br />SESUAI<br /><small><small>( 70 - 79) </small></small>
          </th>
          <th>
            SESUAI<br /><small><small>( 80 - 89) </small></small>
          </th>
          <th>
            SANGAT<br />SESUAI<br /><small><small>( 90 - 100) </small></small>
          </th>
          <th>TOTAL SCORE</th>
        </tr>
      </thead>
      <tbody class="line-tbody">
        <tr>
          <td><strong>A</strong></td>
          <td colspan="2">
            <strong>DATA UMUM DAN PERSYARATAN ADMINISTRASI</strong>
          </td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td>1</td>
          <td>Tempat, Tanggal Lahir</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2</td>
          <td>Usia (<23 tahun) *KEBIJAKAN AFFIRMATIVE</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3</td>
          <td>Saat wawancara berada di semester</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>4</td>
          <td>Jumlah SKS yang diselesaikan ( ≥ 40 SKS )</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>5</td>
          <td>
            Surat Rekomendasi dari 1 (satu) tokoh (akademik / non akademik)
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>6</td>
          <td>Personal Resume & Motivation Letter (Dlm Bhs Indonesia)</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>7</td>
          <td>Surat Keterangan Tidak Mampu dari Kelurahan/Desa domisili ybs</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>8</td>
          <td>Tidak Menerima Beasiswa dari lembaga lain</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>9</td>
          <td>Tidak ikatan dinas dengan lembaga tertentu</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>10</td>
          <td>Telah diseleksi awal oleh perguruan tinggi</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>11</td>
          <td>Bakat</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>12</td>
          <td>Minat</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>13</td>
          <td>IPK ( ≥ 3,00 ) *KEBIJAKAN AFFIRMATIVE</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td style="text-align: center">95</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>B</strong></td>
          <td colspan="2"><strong>PENGETAHUAN KEBANKSENTRALAN</strong></td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>
            Dapat menjelaskan apa itu Bank Indonesia sebagai Bank Sentral,<br />
            tujuan tunggal, kedudukan dalam tata negara dan hubungannya <br />
            dengan pemerintah (di pusat dan daerah), mengenal Dewan Gub <br />
            Bank Indonesia, Tugas dan Wewenang BI serta dapat memberikan <br />
            contoh kebijakan yang dikeluarkan BI
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>C</strong></td>
          <td colspan="2">
            <strong>PENGETAHUAN TENTANG BEASISWA BI DAN GENBI</strong>
          </td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>
            Dapat menjelaskan program beasiswa BI dan tujuannya, alasan <br />
            mengikuti program, manfaatnya bagi mahasiwa, kampus & BI <br />
            Usulan, ide dan harapan mengenai beasiswa BI <br />
            Mengetahui dan dapat menjelaskan apa itu komunitas GenBI <br />
            Tujuan serta manfaat komunitas GenBI (bagi mhs, kampus dan BI)
            <br />
            Usulan ide dan harapan mengenai komunitas GenBI
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>D</strong></td>
          <td colspan="2">
            <strong>PENGALAMAN ORGANISASI SOSIAL DAN EVENT</strong>
          </td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>
            Jenis organisasi dan sebagai apa. Pengalaman mengikuti event2 baik
            <br />
            skala nasional / internasional dan sebagai apa <br />
            Bersedia aktif berpartisipasi kegiatan BI <br />
            Terlibat aktivitas sosial yang bermanfaat bagi masy & lingkungan
            <br />
            Jenis aktivitas sosial yang diikuti <br />
            Lama terlibat dan jabatan yang dipegang <br />
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>E</strong></td>
          <td colspan="2">
            <strong>KARYA ILMIAH DAN PRESTASI YANG DIPEROLEH</strong>
          </td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td>1</td>
          <td>
            Karya Ilmiah (dipublikasikan / tidak) (bidang apa, thn, dimana, dll)
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2</td>
          <td>Prestasi atau penghargaan akademik (thn, dimana, bidang, dll)</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3</td>
          <td>Prestasi lainnya</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>F</strong></td>
          <td colspan="2">
            <strong>MOTIVASI, KEPRIBADIAN ( ≥ 75 ) DAN LAINNYA</strong>
          </td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">0</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">100</td>
          <td style="text-align: center">95</td>
          <td style="text-align: center">25%</td>
          <td style="text-align: center">23,75</td>
          <td></td>
        </tr>
        <tr>
          <td>1</td>
          <td>Sifat dan Karakter, Sikap, Perilaku dan Pembawaan</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>2</td>
          <td>Inisiatif dan kreatifitas</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>3</td>
          <td>Motivasi (pendalaman Motivation Letter)</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr style="background-color: rgb(132, 189, 228); text-align: center">
          <td colspan="3" style="text-align: right">
            <strong>TOTAL PENILAIAN AKHIR</strong>
          </td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th colspan="8" style="text-align: left">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. <br> Fuga, vel. Quaerat dolores fuga eos sit maiores asperiores praesentium, <br> soluta nemo vitae ullam dolor facilis facere odit! <br> Porro facere asperiores libero?
          </th>
          <th colspan="7" style="text-align: center">
            PEWAWANCARA :
            <br>
            <br>
            Jution Candra Kirana
          </th>
        </tr>
      </tfoot>
      <tfoot>
        <tr>
          <th colspan="8" style="text-align: left">
            CATATAN & REKOMENDASI PEWAWANCARA
          </th>
          <th colspan="7" style="text-align: center">
            Jakarta, .......................................... 2020
          </th>
        </tr>
      </tfoot>
    </table>
  </body>
</html>
<div style="text-align: center;">
  <h3>
    TABEL NILAI WAWANCARA <br>
    SELEKSI MAHASISWA CALON PENERIMA BEASISWA BANK INDONESIA REGULER PERGURUAN TINGGI NEGERI <?= strtoupper($university->name) ?>
  </h3>
  <p>Tanggal, <?= $row->created_at ?></p>
</div>
<table>
  <tr><td>1</td></tr>
</table>
<table border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>NAMA MAHASISWA</th>
      <th>NPM</th>
      <th>JURUSAN</th>
      <th>USIA * <br> (< 23 Tahun) </th>
      <th>IPK * <br> (≥ 3,50)</th>
      <th>SEMESTER <br> (Saat Ini)</th>
      <th>JUMLAH <br> SKS <br>(Saat Ini)</th>
      <th>SURAT <br> KET <br> (SKTM)</th>
      <th>SCORE <br> Data dan <br> Syarat <br> Administrasi</th>
      <th>SCORE <br> Pengetahuan <br> tentang <br> Bank Sentral</th>
      <th>SCORE <br> Pengetahuan <br> Beasiswa BI <br> dan GenBI</th>
      <th>SCORE <br> Pengalaman <br> Organisasi</th>
      <th>SCORE <br> Karya Ilmiah <br> dan Prestasi</th>
      <th>SCORE <br> Motivasi dan <br> Kepribadian</th>
      <th>NILAI <br> AKHIR</th>
      <th>REKOMENDASI</th>
      <th>KETERANGAN</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= $row->npm ?></td>
        <td><?= $row->major ?></td>
        <td><?= calculateAge($row->birth_date) ?></td>
        <td><?= $row->gpa ?></td>
        <td><?= $row->semester ?></td>
        <td><?= $row->sks ?></td>
        <td><?= ($row->sktm != null) ? 'Ya' : 'Tidak' ?></td>
        <td><?= likertScala(ceil(($row->gpa_score + $row->administration_score)/2)) ?></td>
        <td><?= likertScala($row->bank_central_score) ?></td>
        <td><?= likertScala($row->genbi_score) ?></td>
        <td><?= likertScala($row->organization_score) ?></td>
        <td><?= likertScala($row->scientific_work_score) ?></td>
        <td><?= likertScala($row->motivation_score) ?></td>
        <td><?= ceil((likertScala(ceil(($row->gpa_score + $row->administration_score)/2)) + likertScala($row->bank_central_score) + likertScala($row->genbi_score) + likertScala($row->organization_score) + likertScala($row->scientific_work_score) + likertScala($row->motivation_score))/6) ?></td>
        <td><?= ($row->recomendation != null) ? 'REKOMENDASI' : '-' ?></td>
        <td><?= $row->note ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
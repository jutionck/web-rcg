<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");

?>
<div style="text-align: center;">
  <h3>
    REKAPITULASI <br>
    DATA MAHASISWA CALON PENERIMA BEASISWA REGULER PERGURUAN TINGGI NEGERI <br>
    <?= $university->name ?>
  </h3>
  <p><?= indonesianDate(@$row->interview_date, 'dddd, D MMMM Y') ?></p>
</div>
<table border="1">
  <thead>
    <tr>
      <th>Jadwal Wawancara</th>
      <th>No</th>
      <th>Nama Lengkap</th>
      <th>NPM</th>
      <th>Jurusan</th>
      <th>Jenis Kelamin</th>
      <th>Agama</th>
      <th>Golongan Darah</th>
      <th>IPK</th>
      <th>IP Semester Terakhir</th>
      <th>Tempat tanggal Lahir (Kota,ddmmyyyy)</th>
      <th>No Handphone</th>
      <th>Alamat Lengkap</th>
      <th>Alamat Domisili</th>
      <th>Nama Akun Instagram</th>
      <th>Nama Ibu</th>
      <th>Nama Ayah</th>
      <th>Pekerjaan Ibu</th>
      <th>Pekerjaan Ayah</th>
      <th>Minat dan Bakat</th>
      <th>Keterampilan Hidup (Lifeskill)</th>
      <th>Potensi Diri</th>
      <th>Aktivitas Sosial Yang Biasa Dilakukan di Wilayah Tinggal</th>
      <th>Motivasi Mendaftar Beasiswa Bank Indonesia</th>
      <th>Apakah bersedia aktif di GenBI</th>
      <th>Alasan bersedia aktif di GenBI</th>
      <th>Usulan kegiatan untuk pengembangan GenBI</th>
      <th>Link Minat dan Bakat</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td>-</td>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= $row->npm ?></td>
        <td><?= $row->major ?></td>
        <td><?= $row->gender ?></td>
        <td><?= $row->religion ?></td>
        <td><?= $row->blood ?></td>
        <td><?= $row->gpa ?></td>
        <td><?= $row->gp ?></td>
        <td><?= $row->place_of_birth ?>, <?= indonesianDate($row->birth_date, 'D MMMM Y') ?></td>
        <td><?= $row->no_hp ?></td>
        <td><?= $row->address ?></td>
        <td><?= $row->domicile ?></td>
        <td><?= $row->instagram ?></td>
        <td><?= $row->mother ?></td>
        <td><?= $row->father ?></td>
        <td><?= $row->mother_occupation ?></td>
        <td><?= $row->father_occupation ?></td>
        <td><?= $row->interest_talent ?></td>
        <td><?= $row->life_skill ?></td>
        <td><?= $row->self_potential ?></td>
        <td><?= $row->activity ?></td>
        <td><?= $row->motivation_to_apply ?></td>
        <td><?= $row->genbi_ ?></td>
        <td><?= $row->reason_of_genbi ?></td>
        <td><?= $row->suggestion_for_genbi ?></td>
        <td><?= $row->link_interest_talent ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
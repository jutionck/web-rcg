<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Form A1</title>
  <style>
    table,
    th,
    td {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th,
    td {
      padding: 5px;
    }
  </style>
</head>

<body>

  <table style="width: 100%;">
    <tr>
      <td colspan="4">
        <img src="./assets/backoffice/img/bi-b.png" alt="Logo Bank Indonesia" width="18%" />
      </td>
      <td colspan="2" style="width:20%; text-align: center; font-size:2rem;"><strong>FORM A.1</strong></td>
      <td rowspan="2" style="text-align: center;">
        <?php if ($pendaftar->photo) :  ?>
          <img src="./assets/backoffice/upload/photo/<?= $pendaftar->photo ?>" alt="Photo 3x4" width="100px" />
        <?php else :
          echo '3x4';
        endif ?>
      </td>
    </tr>
    <tr>
      <td colspan="6" style="font-size:3rem; height:100px;"> <strong>BIODATA MAHASISWA</strong></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2" style="text-align: center;">NAMA LENGKAP</td>
      <td colspan="3" rowspan="2"><?= $pendaftar->name ?></td>
      <td style="text-align: center; width: 14%;">JENIS KELAMIN</td>
      <td><?= $pendaftar->gender ?></td>
    </tr>
    <tr>
      <td style="text-align: center;">AGAMA</td>
      <td><?= $pendaftar->religion ?></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2" style="text-align: center;">TEMPAT, TANGGAL LAHIR</td>
      <td colspan="3" rowspan="2"><?= $pendaftar->place_of_birth ?>, <?= indonesianDate($pendaftar->birth_date, 'D MMMM Y') ?></td>
      <td style="text-align: center;">GOL DARAH</td>
      <td><?= $pendaftar->blood ?></td>
    </tr>
    <tr>
      <td style="text-align: center;">SUKU BANGSA</td>
      <td><?= $pendaftar->ethnic ?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">PERGURUAN TINGGI</td>
      <td colspan="3"><?= $pendaftar->university ?></td>
      <td style="text-align: center;">IP SEMESTER TERAKHIR</td>
      <td><?= $pendaftar->gp ?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">FAKULTAS & JURUSAN</td>
      <td colspan="5"><?= $pendaftar->faculty ?> & <?= $pendaftar->major ?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">ALAMAT LENGKAP (DOMISILI SAAT INI)</td>
      <td colspan="5"><?= $pendaftar->address ?></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2" style="text-align: center;">KONTAK PERSONAL</td>
      <td style="text-align: center;">NO TELP/HANDPHONE</td>
      <td colspan="2" style="text-align: center;">EMAIL ADDRESS</td>
      <td colspan="2" style="text-align: center;">FACEBOOK/TWITTER/INSTAGRAM</td>
    </tr>
    <tr>
      <td><?= $pendaftar->no_hp ?></td>
      <td colspan="2"><?= $pendaftar->email ?></td>
      <td colspan="2"><?= $pendaftar->instagram ?></td>
    </tr>
    <tr>
      <td rowspan="2" style="text-align: center;">NAMA LENGKAP</td>
      <td style="text-align: center;">BAPAK</td>
      <td colspan="5"><?= $pendaftar->father ?> (<?= $pendaftar->father_occupation ?>)</td>
    </tr>
    <tr>
      <td style="text-align: center;">IBU</td>
      <td colspan="5"><?= $pendaftar->mother ?> (<?= $pendaftar->mother_occupation ?>)</td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">MINAT & BAKAT</td>
      <td colspan="5"><?= $pendaftar->interest_talent ?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">KETERAMPILAN HIDUP</td>
      <td colspan="5"><?= $pendaftar->life_skill ?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center;">POTENSI DIRI</td>
      <td colspan="5"><?= $pendaftar->self_potential ?></td>
    </tr>
    <tr>
      <td colspan="2" style="width: 8%; text-align:center;">AKTIVITAS SOSIAL YANG BIASA & BISA DILAKUKAN DI WILAYAH TINGGAL/KAMPUS</td>
      <td colspan="5"><?= $pendaftar->activity ?></td>
    </tr>
    <tr>
      <td colspan="7"><strong> APAKAH ANDA SIAP BERPERAN AKTIF DALAM KEPENGURUSAN & KEGIATAN KOMUNITAS PENERIMA BEASISWA BANK INDONESIA</strong></td>
    </tr>
    <tr>
      <td colspan="2"><?= $pendaftar->genbi_ ?></td>
      <td colspan="5">alasan:
        <p><?= $pendaftar->reason_of_genbi ?></p>
      </td>
    </tr>
    <tr>
      <td colspan="5">SARAN-SARAN UNTUK PENGEMBANGAN KOMUNITAS PENERIMA BEASISWA BANK INDONESIA:
        <p><?= $pendaftar->suggestion_for_genbi ?></p>
      </td>
      <td colspan="2" style="text-align:center;">
        <p>Lampung, <?= indonesianDate($pendaftar->created_at, 'D MMMM Y') ?></p>
        <br><br><br><br><br><br><br><br><br><br>

        <?= $pendaftar->name ?>
      </td>
    </tr>
    <tr>
      <td colspan="7">
        Dengan ini saya menyatakan:
        <ol>
          <li>
            Mengikuti dan mengetahui segala persyaratan, peraturan, ketentuan dan arahan yang berlaku dalam program Beasiswa Bank Indonesia
          </li>
          <li>
            Menjaga nama baik Bank Indonesia serta berkontribusi positif dalam pengelolaan Generasi Baru Indonesia dan berperan aktif dalam kegiatan-kegiatan yang diselenggarakan oleh Bank Indonesia sebagai bentuk tanggung jawab moral sebagai insan akademi yang berkarakter
          </li>
        </ol>
      </td>
    </tr>
  </table>
</body>

</html>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");

?>
<div style="text-align: center;">
  <h3>
    REKAPITULASI <br>
    HASIL EVALUASI PENDAFTAR BEASISWA REGULER PERGURUAN TINGGI NEGERI <br>
    <?= $university->name ?>
  </h3>
  <p><?= indonesianDate(@$row->interview_date, 'dddd, D MMMM Y') ?></p>
</div>
<table border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>NAMA MAHASISWA</th>
      <th>UNIVERSITAS</th>
      <th>IPK</th>
      <th>CUTI</th>
      <th>MENERIMA BEASISWA</th>
      <th>KETERANGAN</th>
      <th>HASIL</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= $row->university ?></td>
        <td><?= $row->gpa_evaluation ?></td>
        <td><?= $row->paid_leave ?></td>
        <td><?= $row->receive_another_scholarship ?></td>
        <td><?= $row->description ?></td>
        <td><?= $row->evaluation_result ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
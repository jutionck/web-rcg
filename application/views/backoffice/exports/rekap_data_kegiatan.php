<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition:attachment; filename=" . $title . ".xls");
header("Pragma: no-cache");
header("Expires:0");

?>
<div style="text-align: center;">
  <h3>
    REKAP <br>
    DATA KEGIATAN MAHASISWA <br>
    <?= $university->name ?> <br>
    Tahun <?= indonesianDate(@$row->created_at, 'Y') ?>
  </h3>
</div>
<table border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Mahasiswa</th>
      <th>Nama Kegiatan </th>
      <th>Tanggal Pelaksanaan</th>
      <th>Kehadiran</th>
      <th>Jenis Kegiatan</th>
      <th>Resume Kegiatan</th>
      <th>Alasan Tidak Hadir</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;
    foreach ($allData as $row) : ?>
      <tr>
        <td><?= $no++ ?></td>
        <td><?= $row->name ?></td>
        <td><?= $row->activity_name ?></td>
        <td><?= indonesianDate($row->activity_date, 'DD MMMM Y') ?></td>
        <td><?= $row->attendance ?></td>
        <td><?= $row->activity_type ?></td>
        <td><?= $row->activity_resume ?></td>
        <td><?= $row->activity_reason ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
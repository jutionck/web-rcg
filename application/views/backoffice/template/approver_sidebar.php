<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'dashboard' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/dashboard') ?>">
        <i class="bi bi-grid"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-heading">Menu</li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'pendaftar_beasiswa' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/pendaftar_beasiswa') ?>">
        <i class="bi bi-person"></i>
        <span>Pendaftar Beasiswa</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'kegiatan_mahasiswa' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/kegiatan_mahasiswa') ?>" class="<?= ($this->uri->segment(3)  === 'kegiatan_mahasiswa' ? 'active' : '') ?>">
        <i class="bi bi-wrench"></i>
        <span>Kegiatan Mahasiswa</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'laporan_kegiatan_mahasiswa' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/laporan_kegiatan_mahasiswa') ?>" class="<?= ($this->uri->segment(3)  === 'laporan_kegiatan_mahasiswa' ? 'active' : '') ?>">
        <i class="bi bi-file-text-fill"></i>
        <span>Laporan Kegiatan Mahasiswa</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'hasil_wawancara' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/hasil_wawancara') ?>" class="<?= ($this->uri->segment(3)  === 'hasil_wawancara' ? 'active' : '') ?>">
        <i class="bi bi-award"></i>
        <span>Hasil Akhir</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'hasil_evaluasi' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/approver/hasil_evaluasi') ?>" class="<?= ($this->uri->segment(3)  === 'hasil_evaluasi' ? 'active' : '') ?>">
        <i class="bi bi-emoji-smile"></i>
        <span>Hasil Evaluasi</span>
      </a>
    </li>
  </ul>
</aside>

<main id="main" class="main">
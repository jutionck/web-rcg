<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'dashboard' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/operator/dashboard') ?>">
        <i class="bi bi-grid"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-heading">Entry Data</li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'pendaftar_beasiswa' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/operator/pendaftar_beasiswa') ?>">
        <i class="bi bi-person"></i>
        <span>Pendaftar Beasiswa</span>
      </a>
    </li>
  </ul>
</aside>

<main id="main" class="main">
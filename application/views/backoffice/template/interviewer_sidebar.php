<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'dashbaord' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/interviewer/dashboard') ?>" class="<?= ($this->uri->segment(3)  === 'dashbaord' ? 'active' : '') ?>">
        <i class="bi bi-grid"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-heading">Pages</li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'wawancara' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/interviewer/wawancara') ?>" class="<?= ($this->uri->segment(3)  === 'wawancara' ? 'active' : '') ?>">
        <i class="bi bi-question-circle"></i>
        <span>Wawancara Peserta</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'hasil_wawancara' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/interviewer/hasil_wawancara') ?>" class="<?= ($this->uri->segment(3)  === 'hasil_wawancara' ? 'active' : '') ?>">
        <i class="bi bi-award"></i>
        <span>Hasil Akhir</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'hasil_evaluasi' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/interviewer/hasil_evaluasi') ?>" class="<?= ($this->uri->segment(3)  === 'hasil_evaluasi' ? 'active' : '') ?>">
        <i class="bi bi-emoji-smile"></i>
        <span>Hasil Evaluasi</span>
      </a>
    </li>
  </ul>
</aside>

<main id="main" class="main">
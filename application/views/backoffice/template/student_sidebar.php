<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'dashboard' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/student/dashboard') ?>">
        <i class="bi bi-grid"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-heading">Menu</li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'biodata' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/student/biodata') ?>">
        <i class="bi bi-person"></i>
        <span>Biodata</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= ($this->uri->segment(3)  === 'kegiatan' ? '' : 'collapsed') ?>" href="<?= base_url('backoffice/student/kegiatan') ?>" class="<?= ($this->uri->segment(3)  === 'kegiatan' ? 'active' : '') ?>">
        <i class="bi bi-wrench"></i>
        <span>Kegiatan</span>
      </a>
    </li>
  </ul>
</aside>

<main id="main" class="main">
<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Pendaftar Beasiswa</li>
      <li class="breadcrumb-item active"> <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section profile">
  <div class="row">
    <div class="col-xl-4">
      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
          <img src="<?= base_url('assets/backoffice/upload/photo/' . $pendaftar->photo) ?>" alt="Profile" class="img-thumbnail">
          <h2><?= $pendaftar->name; ?></h2>
          <h3><?= $pendaftar->university ?></h3>
          <div class="social-links mt-2">
            <?= ($pendaftar->twitter) ? '<a href="https://twitter.com/' . $pendaftar->twitter . '" class="twitter" target="_blank"><i class="bi bi-twitter"></i></a>' : '' ?>
            <?= ($pendaftar->facebook) ? '<a href="https://facebook.com/' . $pendaftar->facebook . '" class="twitter" target="_blank"><i class="bi bi-facebook"></i></a>' : '' ?>
            <?= ($pendaftar->instagram) ? '<a href="https://instagram.com/' . $pendaftar->instagram . '" class="instagram" target="_blank"><i class="bi bi-instagram"></i></a>' : '' ?>
            <?= ($pendaftar->linkedin) ? '<a href="https://linkedin.com/in/' . $pendaftar->linkedin . '" class="linkedin" target="_blank"><i class="bi bi-linkedin"></i></a>' : '' ?>
          </div>
          <div class="btn-group mt-3">
            <a href="<?= base_url('backoffice/admin/wawancara') ?>" class="btn btn-secondary btn-sm">
              <i class="bi bi-arrow-left"></i> Kembali
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8">
    <?php if ($this->session->flashdata('success')) : ?>  
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php elseif ($this->session->flashdata('error')) : ?>
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <i class="bi bi-exclamation-octagon me-1"></i>
        <?= $this->session->flashdata('error') ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    <?php endif; ?>
      <div class="card">
        <div class="card-body pt-3">
          <ul class="nav nav-tabs nav-tabs-bordered">
            <li class="nav-item">
              <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Biodata Pendaftar</button>
            </li>
            <li class="nav-item">
              <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-score">Penilaian</button>
            </li>
          </ul>
          <div class="tab-content pt-2">
            <div class="tab-pane fade show active profile-overview" id="profile-overview">
              <h5 class="card-title">Profil Details</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Usia</div>
                <div class="col-lg-9 col-md-8"><?= $age ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">NPM</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->npm ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Fakultas / Jurusan</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->faculty ?> / <?= $pendaftar->major ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">IPK</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->gpa ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Jumlah SKS</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->number_of_credits ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Semester</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->semester ?></div>
              </div>
              <h5 class="card-title">Data Pendukung</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Bakat/Minat</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->interest_talent ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Link Minat/Bakat</div>
                <div class="col-lg-9 col-md-8">
                  <a href=" <?= $pendaftar->link_interest_talent ?>"> <?= $pendaftar->link_interest_talent ?></a>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Prestasi/Organisasi</div>
                <div class="col-lg-9 col-md-8">
                  <a href="<?= base_url('assets/backoffice/upload/organization_achievement/' . $pendaftar->organization_achievement_file) ?>"> <?= $pendaftar->organization_achievement_file ?></a>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Motivation Letter</div>
                <div class="col-lg-9 col-md-8">
                  <a href="<?= base_url('assets/backoffice/upload/motivation_letter/' . $pendaftar->motivation_letter_file) ?>"> <?= $pendaftar->motivation_letter_file ?></a>
                </div>
              </div>
            </div>

            <div class="tab-pane fade profile-file pt-3" id="profile-score">
              <form class="row g-3" method="POST" name="interviewScore" action="">
                <input type="hidden" name="applicants_id" value="<?= $pendaftar->id ?>">
                <input type="hidden" name="id" value="<?= @$interview->id ?>">
                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="gpa_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->gpa_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">IPK</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control <?= form_error('gpa_score') ? 'is-invalid' : ''; ?>" id="floatingName" placeholder="IPK Keterangan" name="gpa_note" value="<?= set_value('gpa_note') ? set_value('gpa_note') : @$interview->gpa_note; ?>">
                    <div class="invalid-feedback">
                      <?= form_error('gpa_note'); ?>
                    </div>
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="administration_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->administration_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Penghasilan Orang Tua</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="administration_note" value="<?= set_value('administration_note') ? set_value('administration_note') : @$interview->administration_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="bank_central_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->bank_central_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Pengetahuan Kebanksentralan</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="bank_central_note" value="<?= set_value('bank_central_note') ? set_value('gpa_note') : @$interview->gpa_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="genbi_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->genbi_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Pengetahuan BI dan GenBI</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="genbi_note" value="<?= set_value('genbi_note') ? set_value('genbi_note') : @$interview->genbi_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="organization_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->organization_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <div class="invalid-feedback">
                      <?= form_error('position'); ?>
                    </div>
                    <label for="floatingTextarea">Pengalaman Organisasi</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="organization_note" value="<?= set_value('organization_note') ? set_value('organization_note') : @$interview->organization_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="scientific_work_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->scientific_work_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Prestasi/Karya Ilmiah</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Your Name" name="scientific_work_note" value="<?= set_value('scientific_work_note') ? set_value('scientific_work_note') : @$interview->scientific_work_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="motivation_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->motivation_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Motivasi dan Kepribadian</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Catatan untuk nilai ini" name="motivation_note" value="<?= set_value('motivation_note') ? set_value('motivation_note') : @$interview->motivation_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-floating">
                    <select class="form-select" id="floatingSelect" aria-label="State" name="sktm_score">
                      <option selected="">Pilih</option>
                      <?php foreach ($score as $row) :
                        if (@$interview->sktm_score == $row->score) {
                          $selected = "selected";
                        } else {
                          $selected = "";
                        }
                        echo "<option value='$row->score' $selected>$row->score</option>";
                      endforeach ?>
                    </select>
                    <label for="floatingTextarea">Surat Keterangan Tidak Mampu</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Catatan SKTM" name="sktm_note" value="<?= set_value('sktm_note') ? set_value('sktm_note') : @$interview->sktm_note; ?>">
                    <label for="floatingName">Keterangan</label>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="floatingName" placeholder="Catatan wawancara" name="note" value="<?= set_value('note') ? set_value('note') : @$interview->note; ?>">
                    <label for="floatingName">Catatan</label>
                  </div>
                </div>
                <?php if (@$interview->result_score) : ?>
                  <div class="col-md-12">
                    <div class="form-floating">
                      <input type="text" class="form-control" id="floatingName" placeholder="Nilai akhir" name="result_score" readonly value="<?= set_value('result_score') ? set_value('result_score') : @$interview->result_score; ?>">
                      <label for="floatingName">Nilai Akhir</label>
                    </div>
                  </div>
                <?php endif ?>
                <div class="col-md-12 mt-3">
                  <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
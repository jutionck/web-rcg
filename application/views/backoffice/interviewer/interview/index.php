<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <div class="row">
          <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-4">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="university" required>
                    <?php
                    if (!isset($_GET['university'])) {
                      echo '<option selected="">Kampus</option>';
                    }
                    ?>
                    <?php foreach ($university as $row) : ?>
                      <option value="<?= $row->id ?>"><?= $row->name ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                  <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url('backoffice/admin/wawancara') ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                      <a href="<?= site_url('backoffice/export/rekap_data_wawancara/' . $this->input->get('university') . '?tahun=' . $this->input->get('tahun')) ?>" class="btn btn-success" style="margin-top: 25px;">Export</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Perguruan Tinggi</th>
                  <th scope="col">Fakultas/Jurusan</th>
                  <th scope="col">IPK</th>
                  <th scope="col">IP Semester</th>
                  <th scope="col">Status Wawancara</th>
                  <th scope="col">Aksi</th>
                  <th scope="col">Hasil</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($pendaftar as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td>
                      <a href="<?= base_url('backoffice/admin/wawancara/penilaian/' . $row->id); ?>"><?= $row->name ?></a>
                    </td>
                    <td><?= $row->university ?></td>
                    <td><?= $row->faculty ?> / <?= $row->major ?></td>
                    <td><?= $row->gpa ?></td>
                    <td><?= $row->gp ?></td>
                    <td>
                      <?php
                      if ($row->recomendation == 'Lulus') {
                        echo '<span class="badge rounded-pill bg-success">Lulus</span>';
                      } else if ($row->recomendation == 'Tidak Lulus') {
                        echo '<span class="badge rounded-pill bg-danger">Tidak Lulus</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-light text-dark">Pending</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <div class="btn-group">
                        <?php $rplcPhone = str_replace('08', '628', $row->no_hp) ?>
                        <a href="https://api.whatsapp.com/send?phone=<?= $rplcPhone ?>" class="btn btn-success btn-sm" target="_blank"><i class="bi bi-whatsapp"></i></a>
                        <a href="<?= base_url('backoffice/admin/wawancara/penilaian/' . $row->id); ?>" class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Penilaian data"><i class="bi bi-journal-text"></i></a>
                        <a href="<?= base_url('backoffice/admin/wawancara/penilaian/' . $row->id); ?>" class="btn btn-success btn-sm" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Export data"><i class="bi bi-file-earmark-excel-fill"></i></a>
                      </div>
                    </td>
                    <td width="16%">
                      <?php
                      if ($row->recomendation == null) { ?>
                        <div class="btn-group">
                          <button type="button" class="btn btn-outline-dark btn-sm approval-interview" data-id="<?= $row->id ?>" data-uri="<?= $this->uri->segment(5); ?>" title="Hasil Wawancara"> <span class="badge bg-info">Lulus</span> / <span class="badge bg-danger">Tidak Lulus</span></button>
                        </div>
                      <?php } else { ?>

                      <?php } ?>

                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
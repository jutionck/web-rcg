<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $user->id; ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" name="username" placeholder="Your Name" value="<?= set_value('username') ? set_value('username') : $user->username; ?>" readonly>
                <label for="floatingName">Username</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="password" class="form-control <?= form_error('password') ? 'is-invalid' : ''; ?>" id="floatingName" name="password" placeholder="Your Name">
                <div class="invalid-feedback">
                  <?= form_error('password'); ?>
                </div>
                <label for="floatingName">Password</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="password" class="form-control <?= form_error('passconf') ? 'is-invalid' : ''; ?>" id="floatingName" name="passconf" placeholder="Your Name">
                <div class="invalid-feedback">
                  <?= form_error('passconf'); ?>
                </div>
                <label for="floatingName">Ulangi Password</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url('backoffice/' . strtolower($this->session->userdata('role')) . '/dashboard') ?>" class="btn btn-warning">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
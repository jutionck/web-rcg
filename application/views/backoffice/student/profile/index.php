<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section profile">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-xl-4">
      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
          <img src="<?= base_url('assets/backoffice/upload/photo/' . @$profile->photo) ?>" alt="Profile" class="img-thumbnail">
          <h2><?= $profile->name; ?></h2>
          <h3><?= $profile->university ?></h3>
          <div class="social-links mt-2">
            <?= ($profile->twitter) ? '<a href="https://twitter.com/' . $profile->twitter . '" class="twitter" target="_blank"><i class="bi bi-twitter"></i></a>' : '' ?>
            <?= ($profile->facebook) ? '<a href="https://facebook.com/' . $profile->facebook . '" class="twitter" target="_blank"><i class="bi bi-facebook"></i></a>' : '' ?>
            <?= ($profile->instagram) ? '<a href="https://instagram.com/' . $profile->instagram . '" class="instagram" target="_blank"><i class="bi bi-instagram"></i></a>' : '' ?>
            <?= ($profile->linkedin) ? '<a href="https://linkedin.com/in/' . $profile->linkedin . '" class="linkedin" target="_blank"><i class="bi bi-linkedin"></i></a>' : '' ?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8">
      <div class="card">
        <div class="card-body pt-3">
          <ul class="nav nav-tabs nav-tabs-bordered">
            <li class="nav-item"> <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button></li>
          </ul>
          <div class="tab-content pt-2">
            <div class="tab-pane fade profile-overview active show" id="profile-overview">
              <form method="POST" enctype="multipart/form-data" action="">
                <input type="hidden" class="form-control" name="profile_id" value="<?= set_value('id') ? set_value('id') : $profile->id; ?>">
                <input type="hidden" class="form-control" name="photo_old" value="<?= set_value('photo') ? set_value('photo') : $profile->photo; ?>">
                <div class="row mb-3">
                  <label for="name" class="col-md-4 col-lg-3 col-form-label">Nama Lengkap</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="name" type="text" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" id="name" value="<?= set_value('name') ? set_value('name') : $profile->name; ?>">
                    <div class="invalid-feedback">
                      <?= form_error('name'); ?>
                    </div>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="description" class="col-md-4 col-lg-3 col-form-label">Deskripsi Singkat</label>
                  <div class="col-md-8 col-lg-9">
                    <textarea name="description" class="form-control" id="description" style="height: 100px"><?= $profile->description ?></textarea>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="university" class="col-md-4 col-lg-3 col-form-label">Nama Kampus</label>
                  <div class="col-md-8 col-lg-9">
                    <input type="text" class="form-control" id="university" value="<?= $profile->university ?>" readonly>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="major" class="col-md-4 col-lg-3 col-form-label">Jurusan</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="major" type="text" class="form-control" id="major" value="<?= $profile->major ?>">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="npm" class="col-md-4 col-lg-3 col-form-label">NPM</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="npm" type="text" class="form-control <?= form_error('npm') ? 'is-invalid' : ''; ?>" id="npm" value="<?= set_value('npm') ? set_value('npm') : $profile->npm; ?>">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="gpa" class="col-md-4 col-lg-3 col-form-label">IPK</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="gpa" type="text" class="form-control" id="gpa" value="<?= $profile->gpa ?>">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="no_hp" class="col-md-4 col-lg-3 col-form-label">No Whatsapp</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="no_hp" type="text" class="form-control" id="no_hp" value="<?= $profile->no_hp ?>">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="Phone" class="col-md-4 col-lg-3 col-form-label">Photo</label>
                  <div class="col-md-8 col-lg-9">
                    <input name="photo" type="file" class="form-control" id="Phone">
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-md-4 col-lg-3 col-form-label"></label>
                  <div class="col-md-8 col-lg-9">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
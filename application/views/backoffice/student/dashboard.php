<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section dashboard">
  <div class="row">
    <div class="col-lg-12">
      <div class="card recent-sales">
        <div class="card-body">
          <h5 class="card-title">Informasi Kegiatan</h5>
          <p>Selamat datang anda login sebagai <span class="badge bg-dark">mahasiswa</span> dari <strong> <?= $information->university ?></strong>.</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Link Kegiatan</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Jam</th>
                  <th scope="col" width=200>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                if (count($activities)) :
                  foreach ($activities as $activity) :
                ?>
                    <tr>
                      <th scope="row"><?= $no++ ?></th>
                      <td><?= $activity->title ?></td>
                      <td><?= $activity->location ?></td>
                      <td>
                        <?php if (str_contains($activity->activity_link, 'http')) {
                          echo '<a href="' . $activity->activity_link . '" target="_blank">' . $activity->activity_link . '</a>';
                        } ?>
                      </td>
                      <td><?= indonesianDate($activity->activity_date, 'D MMMM Y') ?></td>
                      <td><?= indonesianDate($activity->activity_time, 'HH:mm') ?></td>
                      <td>
                        <div class="btn-group">
                          <a href=" <?= base_url('backoffice/student/kegiatan/hadir/' . $activity->id); ?>" class="btn btn-warning" title="Edit data">Hadir</a>
                          <a href=" <?= base_url('backoffice/student/kegiatan/tidak_hadir/' . $activity->id); ?>" class="btn btn-danger" title="Edit data">Tidak Hadir</a>
                        </div>
                      </td>
                    </tr>
                <?php endforeach;
                else :
                  echo '<tr><td colspan="7" class="text-center">Belum ada kegiatan</td></tr>';
                endif ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
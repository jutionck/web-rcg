<div class="pagetitle">
  <h1>Form <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Form <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?> Lainnya</h5>
          <form action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="applicants_id" value="<?= @$applicant_id->id ?>">
            <div class="row mb-3">
              <label for="inputText" class="col-sm-2 col-form-label">Nama Kegiatan <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <input type="text" name="activity_name" class="form-control <?= form_error('activity_name') ? 'is-invalid' : ''; ?>">
                <div class="invalid-feedback">
                  <?= form_error('activity_name'); ?>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputEmail" class="col-sm-2 col-form-label">Tanggal Pelaksanaan <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <input type="date" name="activity_date" class="form-control <?= form_error('activity_date') ? 'is-invalid' : ''; ?>">
                <div class="invalid-feedback">
                  <?= form_error('activity_date'); ?>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Jenis Kegiatan <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <select class="form-select <?= form_error('activity_type') ? 'is-invalid' : ''; ?>" name="activity_type" aria-label="Default select example">
                  <option selected>Pilih</option>
                  <option value="online">Online</option>
                  <option value="offline">Offline</option>
                  <option value="prestasi">Prestasi</option>
                </select>
                <div class="invalid-feedback">
                  <?= form_error('activity_type'); ?>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Kehadiran <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <select class="form-select <?= form_error('attendance') ? 'is-invalid' : ''; ?>" name="attendance" aria-label="Default select example">
                  <option selected>Pilih</option>
                  <option value="Hadir">Hadir</option>
                  <option value="Tidak Hadir">Tidak Hadir</option>
                </select>
                <div class="invalid-feedback">
                  <?= form_error('attendance'); ?>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputPassword" class="col-sm-2 col-form-label">Resume Kegiatan <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <textarea class="form-control <?= form_error('activity_resume') ? 'is-invalid' : ''; ?>" name="activity_resume" style="height: 100px"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('activity_resume'); ?>
                </div>
              </div>
            </div>
            <div class="row mb-3">
              <label for="inputNumber" class="col-sm-2 col-form-label">Dokumentasi Kehadiran <span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <input class="form-control" name="activity_file" type="file" id="formFile">
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label"><span class="text-danger">*</span> Wajib Diisi</label>
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="<?= base_url($redirect) ?>" class="btn btn-warning">Batal</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
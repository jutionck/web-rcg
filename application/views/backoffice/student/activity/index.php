<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <a href="<?= base_url('backoffice/student/kegiatan/tambah'); ?>" class="btn btn-primary"><i class="bi bi-plus-lg"></i> Kegiatan Lainnya</a>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Link Kegiatan</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Jam</th>
                  <th scope="col" width=200>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($activities as $activity) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $activity->title ?></td>
                    <td><?= $activity->location ?></td>
                    <td>
                      <?php if (str_contains($activity->activity_link, 'http')) {
                        echo '<a href="' . $activity->activity_link . '" target="_blank">' . $activity->activity_link . '</a>';
                      } ?>
                    </td>
                    <td><?= indonesianDate($activity->activity_date, 'D MMMM Y') ?></td>
                    <td><?= indonesianDate($activity->activity_time, 'HH:mm') ?></td>
                    <td>
                      <div class="btn-group">
                        <a href=" <?= base_url('backoffice/student/kegiatan/hadir/' . $activity->id); ?>" class="btn btn-warning" title="Edit data">Hadir</a>
                        <a href=" <?= base_url('backoffice/student/kegiatan/tidak_hadir/' . $activity->id); ?>" class="btn btn-danger" title="Edit data">Tidak Hadir</a>
                      </div>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title">Detail Kegiatan</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Kehadiran</th>
                  <th scope="col">Resume/Alasan</th>
                  <th scope="col">Bukti</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($activity_details as $activity) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $activity->activity_name ?></td>
                    <td><?= $activity->activity_type ?></td>
                    <td><?= indonesianDate($activity->activity_date, 'D MMMM Y') ?></td>
                    <td>
                      <?= ($activity->attendance == 'Hadir') ? '<span class="badge bg-warning text-dark">Hadir</span>' : '<span class="badge bg-danger">Tidak Hadir</span>' ?>
                    </td>
                    <td>
                      <?php
                      if ($activity->attendance == "Hadir") {
                        echo '<strong>Resume: </strong><br>';
                        echo $activity->activity_resume;
                      } else {
                        echo '<strong>Alasan: </strong><br>';
                        echo $activity->activity_reason;
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      if ($activity->attendance == "Hadir") {
                        if (strContains($activity->activity_file, '.jpg') || strContains($activity->activity_file, '.png') || strContains($activity->activity_file, '.jpeg')) { ?>
                          <img src="<?= base_url('assets/backoffice/upload/activity/') ?><?= $activity->activity_file ?>" alt="<?= $activity->activity_file ?>" width="100px">
                        <?php } else { ?>
                          <a href="<?= base_url('assets/backoffice/upload/activity/') ?><?= $activity->activity_file ?>"><?= $activity->activity_file ?></a>
                      <?php }
                      } ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
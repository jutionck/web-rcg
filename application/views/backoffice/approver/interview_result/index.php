<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <div class="row">
            <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                    <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url('backoffice/approver/hasil_wawancara') ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Perguruan Tinggi</th>
                  <th scope="col">Fakultas/Jurusan</th>
                  <th scope="col">IPK</th>
                  <th scope="col">IP Semester</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($pendaftar as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td>
                      <a href="<?= base_url('backoffice/approver/hasil_wawancara/evaluasi/' . $row->id); ?>"><?= $row->name ?></a>
                    </td>
                    <td><?= $row->university ?></td>
                    <td><?= $row->faculty ?> / <?= $row->major ?></td>
                    <td><?= $row->gpa ?></td>
                    <td><?= $row->gp ?></td>
                    <td>
                      <?php if ($periode != NULL) :
                        if ($row->evaluation_result === 'Ya') { ?>
                          <a href="<?= base_url('backoffice/approver/hasil_wawancara/evaluasi/' . $row->id); ?>" class="btn btn-success btn-sm" title="Penilaian data">Approve</a>
                        <?php } else if ($row->evaluation_result === 'Tidak') { ?>
                          <a href="<?= base_url('backoffice/approver/hasil_wawancara/evaluasi/' . $row->id); ?>" class="btn btn-danger btn-sm" title="Penilaian data">Reject</a>
                        <?php } else { ?>
                          <a href="<?= base_url('backoffice/approver/hasil_wawancara/evaluasi/' . $row->id); ?>" class="btn btn-info btn-sm" title="Penilaian data"> Evaluasi</a>
                        <?php }
                      else :
                        if ($row->evaluation_result === 'Ya') { ?>
                          <button class="btn btn-success btn-sm disabled" title="Penilaian data">Approve</button>
                        <?php } else if ($row->evaluation_result === 'Tidak') { ?>
                          <button class="btn btn-danger btn-sm disabled" title="Penilaian data">Reject</button>
                        <?php } else { ?>
                          <button class="btn btn-info btn-sm disabled" title="Penilaian data"> Evaluasi</button>
                      <?php }
                      endif ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
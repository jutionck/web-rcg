<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <div class="row">
            <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                    <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url('backoffice/approver/hasil_evaluasi') ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Universitas</th>
                  <th scope="col">IPK</th>
                  <th scope="col">Cuti</th>
                  <th scope="col">Menerima Beasiswa</th>
                  <th scope="col">Keterangan</th>
                  <th scope="col">Hasil</th>
                  <th scope="col">Transkrip</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($evaluations as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $row->year ?></td>
                    <td><?= $row->name ?></td>
                    <td><?= $row->university ?></td>
                    <td><?= $row->gpa_evaluation ?></td>
                    <td>
                      <?php
                      if ($row->paid_leave == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      if ($row->receive_another_scholarship == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td><?= $row->description ?></td>
                    <td>
                      <?php
                      if ($row->evaluation_result == 'Ya') {
                        echo '<span class="badge rounded-pill bg-success">Ya</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-warning text-dark">Tidak</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <?php if ($row->transcript_file) :  ?>
                        <a href="<?= base_url('assets/backoffice/upload/transcript/evaluation/' . $row->transcript_file) ?>" class="btn btn-success btn-sm">Unduh</a>
                      <?php endif ?>
                    </td>
                    <td>
                      <?php if ($periode != NULL) : ?>
                        <a href="<?= base_url('backoffice/approver/hasil_evaluasi/edit/' . $row->applicants_id); ?>" class="btn btn-secondary btn-sm" title="Ubah data"> Edit</a>
                      <?php else : ?>
                        <button class="btn secondary-info btn-sm disabled" title="Penilaian data"> Edit</button>
                      <?php
                      endif ?>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><?= $sub_title ?></h5>
          <form class="row g-3 needs-validation" novalidate action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $activity->id; ?>">
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('title') ? 'is-invalid' : ''; ?>" id="floatingName" name="title" placeholder="Your Name" value="<?= set_value('title') ? set_value('title') : $activity->title; ?>">
                <div class="invalid-feedback">
                  <?= form_error('title'); ?>
                </div>
                <label for="floatingName">Nama Kegiatan</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('location') ? 'is-invalid' : ''; ?>" id="floatingName" name="location" placeholder="Your Name" value="<?= set_value('location') ? set_value('location') : $activity->location; ?>">
                <div class="invalid-feedback">
                  <?= form_error('location'); ?>
                </div>
                <label for="floatingName">Lokasi Kegiatan</label>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-floating">
                <input type="text" class="form-control" id="floatingName" name="activity_link" placeholder="Your Name" value="<?= set_value('activity_link') ? set_value('activity_link') : $activity->activity_link; ?>">
                <label for="floatingName">Link Kegiatan</label>
                <div id="emailHelp" class="form-text"> <i>Isi ini jika kegiatan bersifat daring</i></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('activity_date') ? 'is-invalid' : ''; ?>" id="floatingName" name="activity_date" placeholder="Your Name" value="<?= set_value('activity_date') ? set_value('activity_date') : $activity->activity_date; ?>">
                <div class="invalid-feedback">
                  <?= form_error('activity_date'); ?>
                </div>
                <label for="floatingName">Tanggal Kegiatan</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <input type="text" class="form-control <?= form_error('activity_time') ? 'is-invalid' : ''; ?>" id="floatingName" name="activity_time" placeholder="Your Name" value="<?= set_value('activity_time') ? set_value('activity_time') : $activity->activity_time; ?>">
                <div class="invalid-feedback">
                  <?= form_error('activity_time'); ?>
                </div>
                <label for="floatingName">Waktu Kegiatan</label>
              </div>
            </div>
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
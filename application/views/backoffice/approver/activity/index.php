<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <a href="<?= base_url('backoffice/approver/kegiatan_mahasiswa/tambah'); ?>" class="btn btn-primary"><i class="bi bi-plus-lg"></i> Tambah</a>
              </div>
            </div>
          </div>
          <div class="row">
            <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                    <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url($redirect) ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Link Kegiatan</th>
                  <th scope="col">Tanggal</th>
                  <th scope="col">Jam</th>
                  <th scope="col">Dibuat Oleh</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($activities as $activity) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $activity->title ?></td>
                    <td>
                      <?= $activity->location ?>
                    </td>
                    <td><a href="<?= $activity->activity_link ?>" target="_blank"><?= $activity->activity_link ?></a></td>
                    <td><?= indonesianDate($activity->activity_date, 'D MMMM Y') ?></td>
                    <td><?= indonesianDate($activity->activity_time, 'HH:mm') ?></td>
                    <td>
                      <?php
                      if ($activity->university_id) {
                        echo $activity->university;
                      } else {
                        echo 'Admin';
                      }
                      ?>
                    </td>
                    <td>
                      <div class="btn-group">
                        <?php
                        if ($activity->university_id) { ?>
                          <a href=" <?= base_url('backoffice/approver/kegiatan_mahasiswa/edit/' . $activity->id); ?>" class="btn btn-warning" title="Edit data"><i class="ri-edit-line"></i> </a>
                          <button type="button" class="btn btn-danger delete-activity-approver" data-id="<?= $activity->id ?>"><i class="bi bi-trash"></i></button>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
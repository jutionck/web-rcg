<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>
<section class="section">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h5 class="mt-3">Biodata Form A1</h5>
          <hr>
        </div>
      </div>
      <form action="" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
        <input type="hidden" class="form-control" name="id" value="<?= set_value('id') ? set_value('id') : $pendaftar->id; ?>">
        <input type="hidden" class="form-control" name="photo_old" value="<?= set_value('photo') ? set_value('photo') : $pendaftar->photo; ?>">
        <input type="hidden" class="form-control" name="ktp_file_old" value="<?= set_value('ktp_file') ? set_value('ktp_file') : $pendaftar->ktp_file; ?>">
        <input type="hidden" class="form-control" name="student_identity_file_old" value="<?= set_value('student_identity_file') ? set_value('student_identity_file') : $pendaftar->student_identity_file; ?>">
        <input type="hidden" class="form-control" name="transcript_file_old" value="<?= set_value('transcript_file') ? set_value('transcript_file') : $pendaftar->transcript_file; ?>">
        <input type="hidden" class="form-control" name="sktm_file_old" value="<?= set_value('sktm_file') ? set_value('sktm_file') : $pendaftar->sktm_file; ?>">
        <input type="hidden" class="form-control" name="academic_recomend_file_old" value="<?= set_value('academic_recomend_file') ? set_value('academic_recomend_file') : $pendaftar->academic_recomend_file; ?>">
        <input type="hidden" class="form-control" name="statement_letter_file_old" value="<?= set_value('statement_letter_file') ? set_value('statement_letter_file') : $pendaftar->statement_letter_file; ?>">
        <input type="hidden" class="form-control" name="motivation_letter_file_old" value="<?= set_value('motivation_letter_file') ? set_value('motivation_letter_file') : $pendaftar->motivation_letter_file; ?>">
        <input type="hidden" class="form-control" name="organization_achievement_file_old" value="<?= set_value('organization_achievement_file') ? set_value('organization_achievement_file') : $pendaftar->organization_achievement_file; ?>">

        <div class="row">
          <div class="col-md-3">
            <img src="<?= ($pendaftar->photo) ? base_url('assets/backoffice/upload/photo/' . $pendaftar->photo) : 'https://i.stack.imgur.com/l60Hf.png' ?>" class="img-fluid" alt="...">
            <input type="file" class="form-control mt-3" name="photo">
            <div id="emailHelp" class="form-text">Upload photo 3x4 berformat (.jpg, .jpeg) Maksimal 500 kb</div>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-6">
                <label for="name" class="text-form text-form form-label">Nama Lengkap (Sesuai KTP)<span class="text-danger">*</span></label>
                <input type="text" name="name" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" id="name" placeholder="Nama Lengkap" value="<?= set_value('name') ? set_value('name') : $pendaftar->name; ?>">
                <div class="invalid-feedback">
                  <?= form_error('name'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="npm" class="text-form text-form form-label">NPM <span class="text-danger">*</span></label>
                <input type="text" name="npm" class="form-control <?= form_error('npm') ? 'is-invalid' : ''; ?>" id="npm" placeholder="NPM" value="<?= set_value('npm') ? set_value('npm') : $pendaftar->npm; ?>">
                <div class="invalid-feedback">
                  <?= form_error('npm'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="place_of_birth" class="text-form form-label">Tempat Lahir <span class="text-danger">*</span></label>
                <input type="text" name="place_of_birth" class="form-control <?= form_error('place_of_birth') ? 'is-invalid' : ''; ?>" id="place_of_birth" placeholder="Tempat Lahir" value="<?= set_value('place_of_birth') ? set_value('place_of_birth') : $pendaftar->place_of_birth; ?>">
                <div class="invalid-feedback">
                  <?= form_error('place_of_birth'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="birth_date" class="text-form form-label">Tanggal Lahir <span class="text-danger">*</span></label>
                <input type="date" name="birth_date" class="form-control <?= form_error('birth_date') ? 'is-invalid' : ''; ?>" id="birth_date" placeholder="Tempat Lahir" required value="<?= set_value('birth_date') ? set_value('birth_date') : $pendaftar->birth_date; ?>">
                <div class="invalid-feedback">
                  <?= form_error('birth_date'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="gender" required>
                  <?php if ($pendaftar->gender == 'Laki-laki') : ?>
                    <option value="Laki-laki" selected>Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>";
                  <?php else : ?>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan" selected>Perempuan</option>";
                  <?php endif ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Agama <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="religion" required>
                  <?php foreach ($religion as $row) :
                    if ($pendaftar->religion == $row->name) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->name' $selected>$row->name</option>";
                  endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Golongan Darah <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="blood" required>
                  <?php foreach ($blood as $row) :
                    if ($pendaftar->blood == $row->name) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->name' $selected>$row->name</option>";
                  endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="ethnic" class="text-form form-label">Suku Bangsa <span class="text-danger">*</span></label>
                <input type="text" class="form-control <?= form_error('ethnic') ? 'is-invalid' : ''; ?>" id="ethnic" placeholder="Suku bangsa" name="ethnic" value="<?= set_value('ethnic') ? set_value('ethnic') : $pendaftar->ethnic; ?>">
                <div class="invalid-feedback">
                  <?= form_error('ethnic'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="university_id" class="text-form form-label">Perguruan Tinggi <span class="text-danger">*</span></label>
                <select id="university_id" class="form-select" name="university_id" required>
                  <?php foreach ($university as $row) :
                    if ($pendaftar->university_id == $row->id) {
                      $selected = "selected";
                    } else {
                      $selected = "";
                    }
                    echo "<option value='$row->id' $selected>$row->name</option>";
                  endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="faculty" class="text-form form-label">Fakultas <span class="text-danger">*</span></label>
                <input type="text" name="faculty" class="form-control <?= form_error('faculty') ? 'is-invalid' : ''; ?>" id="faculty" placeholder="Fakultas" value="<?= set_value('faculty') ? set_value('faculty') : $pendaftar->faculty; ?>">
                <div class="invalid-feedback">
                  <?= form_error('faculty'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="major" class="text-form form-label">Jurusan <span class="text-danger">*</span></label>
                <input type="text" name="major" class="form-control <?= form_error('major') ? 'is-invalid' : ''; ?>" id="major" placeholder="Jurusan" value="<?= set_value('major') ? set_value('major') : $pendaftar->major; ?>">
                <div class="invalid-feedback">
                  <?= form_error('major'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="number_of_credits" class="text-form form-label">Jumlah SKS <span class="text-danger">*</span></label>
                <input type="number" name="number_of_credits" class="form-control <?= form_error('number_of_credits') ? 'is-invalid' : ''; ?>" id="number_of_credits" placeholder="Jumlah SKS" value="<?= set_value('number_of_credits') ? set_value('number_of_credits') : $pendaftar->number_of_credits; ?>">
                <div class="invalid-feedback">
                  <?= form_error('number_of_credits'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="semester" class="text-form form-label">Semester <span class="text-danger">*</span></label>
                <input type="text" name="semester" class="form-control <?= form_error('semester') ? 'is-invalid' : ''; ?>" id="semester" placeholder="Semester" value="<?= set_value('semester') ? set_value('semester') : $pendaftar->semester; ?>">
                <div class="invalid-feedback">
                  <?= form_error('semester'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="gpa" class="text-form form-label">IPK <span class="text-danger">*</span></label>
                <input type="text" name="gpa" class="form-control <?= form_error('ipk') ? 'is-invalid' : ''; ?>" id="gpa" placeholder="IPK" value="<?= set_value('gpa') ? set_value('gpa') : $pendaftar->gpa; ?>">
                <div class="invalid-feedback">
                  <?= form_error('gpa'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="gp" class="text-form form-label">IP Semester Terakir <span class="text-danger">*</span></label>
                <input type="text" name="gp" class="form-control <?= form_error('gp') ? 'is-invalid' : ''; ?>" id="gp" placeholder="IPS Terakhir" value="<?= set_value('gp') ? set_value('gp') : $pendaftar->gp; ?>">
                <div class="invalid-feedback">
                  <?= form_error('gp'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="address" class="text-form form-label">Alamat Lengkap (Sesuai KTP)<span class="text-danger">*</span></label>
                <textarea name="address" class="form-control <?= form_error('address') ? 'is-invalid' : ''; ?>" id="exampleFormControlTextarea1" rows="3"><?= $pendaftar->address ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('address'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="domicile" class="text-form form-label">Alamat Domisili<span class="text-danger">*</span></label>
                <textarea name="domicile" class=" form-control <?= form_error('domicile') ? 'is-invalid' : ''; ?>" id="exampleFormControlTextarea1" rows="3"><?= $pendaftar->domicile ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('domicile'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="no_hp" class="text-form form-label">No Handphone <span class="text-danger">*</span></label>
                <input type="text" name="no_hp" class="form-control <?= form_error('no_hp') ? 'is-invalid' : ''; ?>" id="no_hp" value="<?= set_value('no_hp') ? set_value('no_hp') : $pendaftar->no_hp; ?>">
                <div class="invalid-feedback">
                  <?= form_error('no_hp'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="email" class="text-form form-label">Email <span class="text-danger">*</span></label>
                <input type="text" name="email" class="form-control <?= form_error('email') ? 'is-invalid' : ''; ?>" id="email" value="<?= set_value('email') ? set_value('email') : $pendaftar->email; ?>">
                <div class="invalid-feedback">
                  <?= form_error('email'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="facebook" class="text-form form-label">Facebook</label>
                <input type="text" name="facebook" class="form-control" id="facebook" value="<?= set_value('facebook') ? set_value('facebook') : $pendaftar->facebook; ?>">
              </div>
              <div class="col-md-6">
                <label for="twitter" class="text-form form-label">Twitter</label>
                <input type="text" name="twitter" class="form-control" id="twitter" value="<?= set_value('twitter') ? set_value('twitter') : $pendaftar->twitter; ?>">
              </div>
              <div class="col-md-6">
                <label for="instagram" class="text-form form-label">Instagram</label>
                <input type="text" name="instagram" class="form-control" id="instagram" value="<?= set_value('instagram') ? set_value('instagram') : $pendaftar->instagram; ?>">
              </div>
              <div class="col-md-6">
                <label for="linkedin" class="text-form form-label">LinkedIn</label>
                <input type="text" name="linkedin" class="form-control" id="inputZip" value="<?= set_value('linkedin') ? set_value('linkedin') : $pendaftar->linkedin; ?>">
              </div>
              <div class="col-md-6">
                <label for="father" class="text-form form-label">Nama Ayah <span class="text-danger">*</span></label>
                <input type="text" name="father" class="form-control <?= form_error('father') ? 'is-invalid' : ''; ?>" id="father" value="<?= set_value('father') ? set_value('father') : $pendaftar->father; ?>">
                <div class="invalid-feedback">
                  <?= form_error('father'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="father_occupation" class="text-form form-label">Pekerjaan Ayah <span class="text-danger">*</span></label>
                <input type="text" name="father_occupation" class="form-control <?= form_error('father_occupation') ? 'is-invalid' : ''; ?>" id="father_occupation" value="<?= set_value('father_occupation') ? set_value('father_occupation') : $pendaftar->father_occupation; ?>">
                <div class="invalid-feedback">
                  <?= form_error('father_occupation'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="mother" class="text-form form-label">Nama Ibu <span class="text-danger">*</span></label>
                <input type="text" name="mother" class="form-control <?= form_error('mother') ? 'is-invalid' : ''; ?>" id="mother" value="<?= set_value('mother') ? set_value('mother') : $pendaftar->mother; ?>">
                <div class="invalid-feedback">
                  <?= form_error('mother'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="mother_occupation" class="text-form form-label">Pekerjaan Ibu <span class="text-danger">*</span></label>
                <input type="text" name="mother_occupation" class=" form-control <?= form_error('mother_occupation') ? 'is-invalid' : ''; ?>" id="mother_occupation" value="<?= set_value('mother_occupation') ? set_value('mother_occupation') : $pendaftar->mother_occupation; ?>">
                <div class="invalid-feedback">
                  <?= form_error('mother_occupation'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="interest_talent" class="text-form form-label">Minat dan Bakat<span class="text-danger">*</span></label>
                <textarea name="interest_talent" class="form-control <?= form_error('interest_talent') ? 'is-invalid' : ''; ?>" id="interest_talent" rows="3"><?= $pendaftar->interest_talent ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('interest_talent'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="life_skill" class="text-form form-label">Keterampilan Hidup<span class="text-danger">*</span></label>
                <textarea name="life_skill" class="form-control <?= form_error('life_skill') ? 'is-invalid' : ''; ?>" id="life_skill" rows="3"><?= $pendaftar->life_skill ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('life_skill'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="self_potential" class="text-form form-label">Potensi Diri<span class="text-danger">*</span></label>
                <textarea name="self_potential" class="form-control <?= form_error('self_potential') ? 'is-invalid' : ''; ?>" id="self_potential" rows="3"><?= $pendaftar->self_potential ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('self_potential'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="activity" class="text-form form-label">Aktivitas Sosial Yang Biasa Dilakukan di Wilayah Tinggal <span class="text-danger">*</span></label>
                <textarea name="activity" class="form-control <?= form_error('activity') ? 'is-invalid' : ''; ?>" id="activity" rows="3"><?= $pendaftar->activity ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('activity'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="genbi_" class="text-form form-label">Bersedia aktif di GenBI <span class="text-danger">*</span></label>
                <select id="genbi_" class="form-select" name="genbi_" required>
                  <?php if ($pendaftar->genbi_ == 'Ya') : ?>
                    <option value="Ya" selected>Ya</option>
                    <option value="Tidak">Tidak</option>";
                  <?php else : ?>
                    <option value="Ya">Ya</option>
                    <option value="Tidak" selected>Tidak</option>";
                  <?php endif ?>
                </select>
              </div>
              <div class="col-md-9">
                <label for="reason_of_genbi" class="text-form form-label">Alasan</label>
                <input type="text" name="reason_of_genbi" class="form-control" id="reason_of_genbi" value="<?= set_value('reason_of_genbi') ? set_value('reason_of_genbi') : $pendaftar->reason_of_genbi; ?>">
              </div>
              <div class="col-md-12">
                <label for="suggestion_for_genbi" class="text-form form-label">Usulan Kegiatan Untuk Pengembangan GenBI <span class="text-danger">*</span></label>
                <textarea name="suggestion_for_genbi" class="form-control <?= form_error('suggestion_for_genbi') ? 'is-invalid' : ''; ?>" id="suggestion_for_genbi" rows="3"><?= $pendaftar->suggestion_for_genbi ?></textarea>
                <div class="invalid-feedback">
                  <?= form_error('suggestion_for_genbi'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="link_interest_talent" class="text-form form-label">Link Minat dan Bakat</label>
                <input type="text" class="form-control" name="link_interest_talent" id="link_interest_talent" value="<?= set_value('link_interest_talent') ? set_value('link_interest_talent') : $pendaftar->link_interest_talent; ?>">
              </div>
              <h4 class="mt-3">Data Pendukung</h4>
              <div class="col-md-12">
                <label for="inputState" class="text-form form-label">Surat Keterangan Tidak Mampu <span class="text-danger">*</span></label>
                <select id="sktm_option" class="form-select sktm_option" name="sktm_option" required>
                  <?php if ($pendaftar->sktm_option == 'Ya') : ?>
                    <option value="Ya" selected>Ya</option>
                    <option value="Tidak">Tidak</option>";
                  <?php else : ?>
                    <option value="Ya">Ya</option>
                    <option value="Tidak" selected>Tidak</option>";
                  <?php endif ?>
                </select>
              </div>
              <div class="col-md-12" id="sktm" style="display: none;">
                <label for="inputState" class="text-form form-label">Upload Surat Keterangan Tidak Mampu</label>
                <input type="file" class="form-control" name="sktm">
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Berkas</th>
                          <th>Ubah</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/student_identity/' . $pendaftar->student_identity_file) ?>" target="_blank">KTM</a>
                          </td>
                          <td>
                            <input type="file" name="student_identity" class="form-control" value="<?= set_value('student_identity_file') ? set_value('student_identity_file') : $pendaftar->student_identity_file; ?>">
                          </td>
                        </tr>
                        <tr>
                          <td><a href="<?= base_url('assets/backoffice/upload/ktp/' . $pendaftar->ktp_file) ?>" target="_blank">KTP</a></td>
                          <td>
                            <input type="file" name="ktp" class="form-control">
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/transcript/' . $pendaftar->transcript_file) ?>" target="_blank">Transkrip</a>
                          </td>
                          <td>
                            <input type="file" name="transcript" class="form-control">
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/academic_recomendation/' . $pendaftar->academic_recomend_file) ?>" target="_blank">Surat Rekomendasi Akademik</a>
                          </td>
                          <td>
                            <input type="file" name="academic_recomendation" class="form-control">
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/statement_letter/' . $pendaftar->statement_letter_file) ?>" target="_blank">Surat Pernyataan Tidak Sedang Menerima Beasiswa</a>
                          </td>
                          <td>
                            <input type="file" name="statement_letter" class="form-control">
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/motivation_letter/' . $pendaftar->motivation_letter_file) ?>" target="_blank">Motivation Letter</a>
                          </td>
                          <td>
                            <input type="file" name="motivation_letter" class="form-control">
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/organization_achievement/' . $pendaftar->organization_achievement_file) ?>" target="_blank">Organisasi dan Prestasi</a>
                          </td>
                          <td>
                            <input type="file" name="organization_achievement" class="form-control">
                          </td>
                        </tr>

                        <?php
                        if ($pendaftar->sktm_file) : ?>
                          <td>
                            <a href="<?= base_url('assets/backoffice/upload/sktm/' . $pendaftar->sktm_file) ?>" target="_blank">Surat Keterangan Tidak Mampu</a>
                          </td>
                          <td>
                            <input type="file" class="form-control" name="sktm">
                          </td>
                        <?php endif ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 mt-3">
            <span class="text-danger">(*) Wajib Diisi</span>
          </div>
          <div class="col-md-9 mt-3">
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<div class="pagetitle">
  <h1>Data <?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Data <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section dashboard">
  <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <i class="bi bi-check-circle me-1"></i>
      <?= $this->session->flashdata('success') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php elseif ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <i class="bi bi-exclamation-octagon me-1"></i>
      <?= $this->session->flashdata('error') ?>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="card recent-sales">
        <div class="card-header d-block">
          <div class="d-flex flex-grow-1 min-width-zero card-content">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div>
                <h5 class="card-title"><?= $sub_title ?></h5>
              </div>
              <div>
                <?php if ($periode != NULL) : ?>
                  <a href="<?= base_url('backoffice/operator/pendaftar_beasiswa/tambah'); ?>" class="btn btn-primary"><i class="bi bi-plus-lg"></i> Tambah</a>
                <?php else :
                  echo '<button class="btn btn-primary" disabled><i class="bi bi-plus-lg"></i> Tambah</button>';
                endif ?>
              </div>
            </div>
          </div>
          <div class="row">
            <form action="" method="GET" class="row">
              <div class="row">
                <div class="col-3">
                  <label for="inputState" class="form-label"></label>
                  <select id="inputState" class="form-select" name="tahun" required>
                    <?php showYears(5, 0, $_GET['tahun']); ?>
                  </select>
                </div>
                <div class="col-5">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Cari</button>
                    <?php if ($this->input->get('tahun')) : ?>
                      <a href="<?= base_url($redirect) ?>" class="btn btn-danger" style="margin-top: 25px;">Reset</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table datatable">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Perguruan Tinggi</th>
                  <th scope="col">Fakultas/Jurusan</th>
                  <th scope="col">IPK</th>
                  <th scope="col">IP Semester</th>
                  <th scope="col">Approval</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1;
                foreach ($pendaftar as $row) : ?>
                  <tr>
                    <th scope="row"><?= $no++ ?></th>
                    <td>
                      <a href="<?= base_url('backoffice/operator/pendaftar_beasiswa/detail/' . $row->id); ?>"><?= $row->name ?></a>
                    </td>
                    <td><?= $row->university ?></td>
                    <td><?= $row->faculty ?> / <?= $row->major ?></td>
                    <td><?= $row->gpa ?></td>
                    <td><?= $row->gp ?></td>
                    <td>
                      <?php
                      if ($row->approval == 'Terima') {
                        echo '<span class="badge rounded-pill bg-success">Terima</span>';
                      } else if ($row->approval == 'Tolak') {
                        echo '<span class="badge rounded-pill bg-danger">Tolak</span>';
                      } else {
                        echo '<span class="badge rounded-pill bg-light text-dark">Pending</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <div class="btn-group">
                        <?php if ($periode != NULL) : ?>
                          <a href=" <?= base_url('backoffice/operator/pendaftar_beasiswa/edit/' . $row->id); ?>" class="btn btn-warning btn-sm" title="Edit data"><i class="ri-edit-line"></i> </a>
                          <button type="button" class="btn btn-danger btn-sm delete-pendaftar-op" data-id="<?= $row->id ?>"><i class="bi bi-trash"></i></button>
                        <?php else :
                          echo '<button class="btn btn-warning btn-sm" title="Edit data" disabled><i class="ri-edit-line"></i> </button>';
                          echo '<button class="btn btn-danger btn-sm" disabled><i class="bi bi-trash"></i></button>';
                        endif ?>
                        <a href="<?= base_url('backoffice/operator/pendaftar_beasiswa/detail/' . $row->id); ?>" class="btn btn-light btn-sm" title="Detail data"><i class="ri-eye-line"></i> </a>
                        <a href="<?= base_url('backoffice/export/form_a1/' . $row->id); ?>" class="btn btn-success btn-sm" title="Download form A1"><i class="ri-download-2-line"></i> </a>
                      </div>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
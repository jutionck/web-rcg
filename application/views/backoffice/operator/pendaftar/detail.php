<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active">Pendaftar Beasiswa</li>
      <li class="breadcrumb-item active"> <?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section profile">
  <div class="row">
    <div class="col-xl-4">
      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
          <img src="<?= base_url('assets/backoffice/upload/photo/' . $pendaftar->photo) ?>" alt="Profile" class="img-thumbnail">
          <h2><?= $pendaftar->name; ?></h2>
          <h3><?= $pendaftar->university ?></h3>
          <div class="social-links mt-2">
            <?= ($pendaftar->twitter) ? '<a href="https://twitter.com/' . $pendaftar->twitter . '" class="twitter" target="_blank"><i class="bi bi-twitter"></i></a>' : '' ?>
            <?= ($pendaftar->facebook) ? '<a href="https://facebook.com/' . $pendaftar->facebook . '" class="twitter" target="_blank"><i class="bi bi-facebook"></i></a>' : '' ?>
            <?= ($pendaftar->instagram) ? '<a href="https://instagram.com/' . $pendaftar->instagram . '" class="instagram" target="_blank"><i class="bi bi-instagram"></i></a>' : '' ?>
            <?= ($pendaftar->linkedin) ? '<a href="https://linkedin.com/in/' . $pendaftar->linkedin . '" class="linkedin" target="_blank"><i class="bi bi-linkedin"></i></a>' : '' ?>
          </div>
          <a href="<?= $this->agent->referrer(); ?>" class="mt-3"><i class="bi bi-arrow-left-circle" style="font-size: 2rem; color: cornflowerblue;"></i></a>
        </div>
      </div>
    </div>
    <div class="col-xl-8">
      <div class="card">
        <div class="card-body pt-3">
          <ul class="nav nav-tabs nav-tabs-bordered">
            <li class="nav-item">
              <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Biodata Pendaftar</button>
            </li>
            <li class="nav-item">
              <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-file">Berkas</button>
            </li>
          </ul>
          <div class="tab-content pt-2">
            <div class="tab-pane fade show active profile-overview" id="profile-overview">
              <h5 class="card-title">Detail</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">NPM</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->npm ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Tempat, Tanggal Lahir</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->place_of_birth ?>, <?= indonesianDate($pendaftar->birth_date, 'D MMMM Y') ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Jenis Kelamin</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->gender ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Usia</div>
                <div class="col-lg-9 col-md-8"><?= $age ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Agama</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->religion ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Golongan Darah</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->blood ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Suku Bangsa</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->ethnic ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Alamat (KTP)</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->address ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label ">Alamat Domisili</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->domicile ?></div>
              </div>
              <h5 class="card-title">Data Orang Tua</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Ayah</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->father ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Pekerjaan Ayah</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->father_occupation ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Ibu</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->mother ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Pekerjaan Ibu</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->mother_occupation ?></div>
              </div>
              <h5 class="card-title">Pendidikan</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Fakultas/Jurusan</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->faculty ?>/<?= $pendaftar->major ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">IPK</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->gpa ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">IP Semester</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->gp ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Jumlah SKS</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->number_of_credits ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Semester</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->semester ?></div>
              </div>
              <h5 class="card-title">Data Pendukung</h5>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Bakat/Minat</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->interest_talent ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Link Minat/Bakat</div>
                <div class="col-lg-9 col-md-8">
                  <?php if ($pendaftar->link_interest_talent) : ?>
                    <a href=" <?= $pendaftar->link_interest_talent ?>"> <?= $pendaftar->link_interest_talent ?></a>
                  <?php else :
                    echo '-';
                  endif
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Keterampilan Hidup</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->life_skill ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Potensi Diri</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->self_potential ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Aktivitas Sosial</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->activity ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Alasan Ikut GenBI</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->reason_of_genbi ?></div>
              </div>
              <div class="row">
                <div class="col-lg-3 col-md-4 label">Usulan Kegiatan GenBI</div>
                <div class="col-lg-9 col-md-8"><?= $pendaftar->suggestion_for_genbi ?></div>
              </div>
            </div>

            <div class="tab-pane fade profile-file pt-3" id="profile-file">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Nama Berkas</th>
                    <th>File</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>KTM</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/student_identity/' . $pendaftar->student_identity_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>KTP</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/ktp/' . $pendaftar->ktp_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Transkrip</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/transcript/' . $pendaftar->transcript_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Surat Rekomendasi Akademik</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/academic_recomendation/' . $pendaftar->academic_recomend_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Surat Pernyataan Tidak Sedang Menerima Beasiswa</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/statement_letter/' . $pendaftar->statement_letter_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Motivation Letter</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/motivation_letter/' . $pendaftar->motivation_letter_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>
                  <tr>
                    <td>Organisasi dan Prestasi</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/organization_achievement/' . $pendaftar->organization_achievement_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  </tr>

                  <?php
                  if ($pendaftar->sktm_file) : ?>
                    <td>Surat Keterangan Tidak Mampu</td>
                    <td class="text-center">
                      <a href="<?= base_url('assets/backoffice/upload/sktm/' . $pendaftar->sktm_file) ?>" class="btn btn-info btn-sm" target="_blank">LIHAT</a>
                    </td>
                  <?php endif ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>
<section class="section">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h5 class="mt-3">Biodata Form A1</h5>
          <hr>
        </div>
      </div>
      <form action="" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
        <div class="row">
          <div class="col-md-3">
            <img src="https://i.stack.imgur.com/l60Hf.png" class="img-fluid" alt="...">
            <input type="file" class="form-control mt-3" name="photo">
            <?php if (isset($error)) : ?>
              <div class="invalid-feedback"><?= $error ?></div>
            <?php endif; ?>
            <div id="emailHelp" class="form-text">Upload photo 3x4 berformat (.jpg, .jpeg) Maksimal 500 kb</div>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-6">
                <label for="name" class="text-form text-form form-label">Nama Lengkap (Sesuai KTP)<span class="text-danger">*</span></label>
                <input type="text" name="name" class="form-control <?= form_error('name') ? 'is-invalid' : ''; ?>" id="name" placeholder="Nama Lengkap">
                <div class="invalid-feedback">
                  <?= form_error('name'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="npm" class="text-form text-form form-label">NPM <span class="text-danger">*</span></label>
                <input type="text" name="npm" class="form-control <?= form_error('npm') ? 'is-invalid' : ''; ?>" id="npm" placeholder="NPM">
                <div class="invalid-feedback">
                  <?= form_error('npm'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="place_of_birth" class="text-form form-label">Tempat Lahir <span class="text-danger">*</span></label>
                <input type="text" name="place_of_birth" class="form-control <?= form_error('place_of_birth') ? 'is-invalid' : ''; ?>" id="place_of_birth" placeholder="Tempat Lahir">
                <div class="invalid-feedback">
                  <?= form_error('place_of_birth'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="birth_date" class="text-form form-label">Tanggal Lahir <span class="text-danger">*</span></label>
                <input type="date" name="birth_date" class="form-control <?= form_error('birth_date') ? 'is-invalid' : ''; ?>" id="birth_date" placeholder="Tempat Lahir" required>
                <div class="invalid-feedback">
                  <?= form_error('birth_date'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="gender" required>
                  <option selected>Pilih...</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Agama <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="religion" required>
                  <option selected>Pilih...</option>
                  <?php foreach ($religion as $row) : ?>
                    <option value="<?= $row->name ?>"><?= $row->name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="inputState" class="text-form form-label">Golongan Darah <span class="text-danger">*</span></label>
                <select id="inputState" class="form-select" name="blood" required>
                  <option selected>Pilih...</option>
                  <?php foreach ($blood as $row) : ?>
                    <option value="<?= $row->name ?>"><?= $row->name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="ethnic" class="text-form form-label">Suku Bangsa <span class="text-danger">*</span></label>
                <input type="text" class="form-control <?= form_error('ethnic') ? 'is-invalid' : ''; ?>" id="ethnic" placeholder="Suku bangsa" name="ethnic">
                <div class="invalid-feedback">
                  <?= form_error('ethnic'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="university_id" class="text-form form-label">Perguruan Tinggi <span class="text-danger">*</span></label>
                <select id="university_id" class="form-select" name="university_id" required>
                  <?php foreach ($university as $row) : ?>
                    <option value="<?= $row->id ?>"><?= $row->name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-md-6">
                <label for="faculty" class="text-form form-label">Fakultas <span class="text-danger">*</span></label>
                <input type="text" name="faculty" class="form-control <?= form_error('faculty') ? 'is-invalid' : ''; ?>" id="faculty" placeholder="Fakultas">
                <div class="invalid-feedback">
                  <?= form_error('faculty'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="major" class="text-form form-label">Jurusan <span class="text-danger">*</span></label>
                <input type="text" name="major" class="form-control <?= form_error('major') ? 'is-invalid' : ''; ?>" id="major" placeholder="Jurusan">
                <div class="invalid-feedback">
                  <?= form_error('major'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="number_of_credits" class="text-form form-label">Jumlah SKS <span class="text-danger">*</span></label>
                <input type="number" name="number_of_credits" class="form-control <?= form_error('number_of_credits') ? 'is-invalid' : ''; ?>" id="number_of_credits" placeholder="Jumlah SKS">
                <div class="invalid-feedback">
                  <?= form_error('number_of_credits'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="semester" class="text-form form-label">Semester <span class="text-danger">*</span></label>
                <input type="text" name="semester" class="form-control <?= form_error('semester') ? 'is-invalid' : ''; ?>" id="semester" placeholder="Semester">
                <div class="invalid-feedback">
                  <?= form_error('semester'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="gpa" class="text-form form-label">IPK <span class="text-danger">*</span></label>
                <input type="text" name="gpa" class="form-control <?= form_error('ipk') ? 'is-invalid' : ''; ?>" id="gpa" placeholder="IPK">
                <div class="invalid-feedback">
                  <?= form_error('gpa'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="gp" class="text-form form-label">IP Semester Terakir <span class="text-danger">*</span></label>
                <input type="text" name="gp" class="form-control <?= form_error('gp') ? 'is-invalid' : ''; ?>" id="gp" placeholder="IPS Terakhir">
                <div class="invalid-feedback">
                  <?= form_error('gp'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="address" class="text-form form-label">Alamat Lengkap (Sesuai KTP)<span class="text-danger">*</span></label>
                <textarea name="address" class="form-control <?= form_error('address') ? 'is-invalid' : ''; ?>" id="exampleFormControlTextarea1" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('address'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="domicile" class="text-form form-label">Alamat Domisili<span class="text-danger">*</span></label>
                <textarea name="domicile" class=" form-control <?= form_error('domicile') ? 'is-invalid' : ''; ?>" id="exampleFormControlTextarea1" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('domicile'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="no_hp" class="text-form form-label">No Handphone <span class="text-danger">*</span></label>
                <input type="text" name="no_hp" class="form-control <?= form_error('no_hp') ? 'is-invalid' : ''; ?>" id="no_hp">
                <div class="invalid-feedback">
                  <?= form_error('no_hp'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="email" class="text-form form-label">Email <span class="text-danger">*</span></label>
                <input type="text" name="email" class="form-control <?= form_error('email') ? 'is-invalid' : ''; ?>" id="email">
                <div class="invalid-feedback">
                  <?= form_error('email'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="facebook" class="text-form form-label">Facebook</label>
                <input type="text" name="facebook" class="form-control" id="facebook">
                <div id="emailHelp" class="form-text">Masukkan username e.g genbilampung</div>
              </div>
              <div class="col-md-6">
                <label for="twitter" class="text-form form-label">Twitter</label>
                <input type="text" name="twitter" class="form-control" id="twitter">
                <div id="emailHelp" class="form-text">Masukkan username e.g genbilampung</div>
              </div>
              <div class="col-md-6">
                <label for="instagram" class="text-form form-label">Instagram</label>
                <input type="text" name="instagram" class="form-control" id="instagram">
                <div id="emailHelp" class="form-text">Masukkan username e.g genbilampung</div>
              </div>
              <div class="col-md-6">
                <label for="linkedin" class="text-form form-label">LinkedIn</label>
                <input type="text" name="linkedin" class="form-control" id="inputZip">
                <div id="emailHelp" class="form-text">Masukkan username e.g genbilampung</div>
              </div>
              <div class="col-md-6">
                <label for="father" class="text-form form-label">Nama Ayah <span class="text-danger">*</span></label>
                <input type="text" name="father" class="form-control <?= form_error('father') ? 'is-invalid' : ''; ?>" id="father">
                <div class="invalid-feedback">
                  <?= form_error('father'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="father_occupation" class="text-form form-label">Pekerjaan Ayah <span class="text-danger">*</span></label>
                <input type="text" name="father_occupation" class="form-control <?= form_error('father_occupation') ? 'is-invalid' : ''; ?>" id="father_occupation">
                <div class="invalid-feedback">
                  <?= form_error('father_occupation'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="mother" class="text-form form-label">Nama Ibu <span class="text-danger">*</span></label>
                <input type="text" name="mother" class="form-control <?= form_error('mother') ? 'is-invalid' : ''; ?>" id="mother">
                <div class="invalid-feedback">
                  <?= form_error('mother'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <label for="mother_occupation" class="text-form form-label">Pekerjaan Ibu <span class="text-danger">*</span></label>
                <input type="text" name="mother_occupation" class=" form-control <?= form_error('mother_occupation') ? 'is-invalid' : ''; ?>" id="mother_occupation">
                <div class="invalid-feedback">
                  <?= form_error('mother_occupation'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="interest_talent" class="text-form form-label">Minat dan Bakat<span class="text-danger">*</span></label>
                <textarea name="interest_talent" class="form-control <?= form_error('interest_talent') ? 'is-invalid' : ''; ?>" id="interest_talent" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('interest_talent'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="life_skill" class="text-form form-label">Keterampilan Hidup<span class="text-danger">*</span></label>
                <textarea name="life_skill" class="form-control <?= form_error('life_skill') ? 'is-invalid' : ''; ?>" id="life_skill" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('life_skill'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="self_potential" class="text-form form-label">Potensi Diri<span class="text-danger">*</span></label>
                <textarea name="self_potential" class="form-control <?= form_error('self_potential') ? 'is-invalid' : ''; ?>" id="self_potential" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('self_potential'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="activity" class="text-form form-label">Aktivitas Sosial Yang Biasa Dilakukan di Wilayah Tinggal <span class="text-danger">*</span></label>
                <textarea name="activity" class="form-control <?= form_error('activity') ? 'is-invalid' : ''; ?>" id="activity" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('activity'); ?>
                </div>
              </div>
              <div class="col-md-3">
                <label for="genbi_" class="text-form form-label">Bersedia aktif di GenBI <span class="text-danger">*</span></label>
                <select id="genbi_" class="form-select" name="genbi_" required>
                  <option selected>Pilih...</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>
              </div>
              <div class="col-md-9">
                <label for="reason_of_genbi" class="text-form form-label">Alasan</label>
                <input type="text" name="reason_of_genbi" class="form-control" id="reason_of_genbi">
              </div>
              <div class="col-md-12">
                <label for="suggestion_for_genbi" class="text-form form-label">Usulan Kegiatan Untuk Pengembangan GenBI <span class="text-danger">*</span></label>
                <textarea name="suggestion_for_genbi" class="form-control <?= form_error('suggestion_for_genbi') ? 'is-invalid' : ''; ?>" id="suggestion_for_genbi" rows="3"></textarea>
                <div class="invalid-feedback">
                  <?= form_error('suggestion_for_genbi'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <label for="link_interest_talent" class="text-form form-label">Link Minat dan Bakat</label>
                <input type="text" class="form-control" name="link_interest_talent" id="link_interest_talent">
              </div>
              <h4 class="mt-3">Data Pendukung</h4>
              <div class="col-md-6">
                <label for="ktm" class="text-form form-label">Upload KTM <span class="text-danger">*</span></label>
                <input type="file" name="student_identity" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-6">
                <label for="ktp" class="text-form form-label">Upload KTP <span class="text-danger">*</span></label>
                <input type="file" name="ktp" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-6">
                <label for="transcript" class="text-form form-label">Upload Transkrip <span class="text-danger">*</span></label>
                <input type="file" name="transcript" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-6">
                <label for="academic_recomendation" class="text-form form-label">Upload Surat Rekomendasi Akademik <span class="text-danger">*</span></label>
                <input type="file" name="academic_recomendation" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-6">
                <label for="statement_letter" class="text-form form-label">Upload Surat Pernyataan Tidak Sedang Menerima Beasiswa <span class="text-danger">*</span></label>
                <input type="file" name="statement_letter" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-6">
                <label for="motivation_letter" class="text-form form-label">Upload Motivation Letter <span class="text-danger">*</span></label>
                <input type="file" name="motivation_letter" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
              <div class="col-md-12">
                <label for="organization_achievement" class="text-form form-label">Upload Bukti Organisasi dan Prestasi <span class="text-danger">*</span></label>
                <input type="file" name="organization_achievement" class="form-control" required>
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Jika file lebih dari 1 silahkan dijadikan 1, upload file berformat (.pdf) Maksimal 3 Mb</div>
              </div>
              <div class="col-md-12">
                <label for="inputState" class="text-form form-label">Surat Keterangan Tidak Mampu <span class="text-danger">*</span></label>
                <select id="sktm_option" class="form-select sktm_option" name="sktm_option" required>
                  <option selected>Pilih...</option>
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>
              </div>
              <div class="col-md-12" id="sktm" style="display: none;">
                <label for="inputState" class="text-form form-label">Upload Surat Keterangan Tidak Mampu</label>
                <input type="file" class="form-control" name="sktm">
                <?php if (isset($error)) : ?>
                  <div class="invalid-feedback"><?= $error ?></div>
                <?php endif; ?>
                <div id="emailHelp" class="form-text">Upload file berformat (.pdf, .jpg, .jpeg) Maksimal 1 Mb</div>
              </div>
            </div>
          </div>
          <div class="col-md-3 mt-3">
            <span class="text-danger">(*) Wajib Diisi</span>
          </div>
          <div class="col-md-9 mt-3">
            <div class="col-md-12 d-grid gap-2 d-md-flex justify-content-md-end">
              <button type="submit" class="btn btn-primary">SIMPAN</button>
              <a href="<?= base_url($redirect) ?>" class="btn btn-warning">BATAL</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
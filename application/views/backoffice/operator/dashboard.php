<div class="pagetitle">
  <h1><?= $sub_title ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item active"><?= $sub_title ?></li>
    </ol>
  </nav>
</div>

<section class="section dashboard">
  <div class="row">
    <div class="col-lg-12">
      <div class="card recent-sales">
        <div class="card-body">
          <h5 class="card-title">Informasi</h5>
          <p>Selamat datang anda login sebagai <span class="badge bg-dark">operator</span> dari <strong> <?= $information->university ?></strong>.</p>

          <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
            <div class="dataTable-container">
              <table class="table table-borderless datatable dataTable-table">
                <thead>
                  <tr>
                    <th scope="col" data-sortable="" style="width: 10.9576%;"><a href="#" class="dataTable-sorter">#</a></th>
                    <th scope="col" data-sortable="" style="width: 23.9883%;"><a href="#" class="dataTable-sorter">Keterangan</a></th>
                    <th scope="col" data-sortable="" style="width: 15.1038%;"><a href="#" class="dataTable-sorter">Tanggal Mulai</a></th>
                    <th scope="col" data-sortable="" style="width: 15.1038%;"><a href="#" class="dataTable-sorter">Tanggal Selesai</a></th>
                    <th scope="col" data-sortable="" style="width: 15.1038%;"><a href="#" class="dataTable-sorter">Status</a></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($periode as $row) : ?>
                    <tr>
                      <th scope="row"><a href="#"><?= $no++ ?></a></th>
                      <td><?= $row->title ?> </td>
                      <td><?= indonesianDate($row->start_time, 'D MMMM Y') ?> </td>
                      <td><?= indonesianDate($row->finish_time, 'D MMMM Y') ?> </td>
                      <td><?= getTextPeriod($row->finish_time) ?></td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_benefits extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_master_model', 'AMM');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/benefit';
  }

  public function index()
  {
    $data = [
      'title'     => 'Benefit | beasiswabilampung.com',
      'sub_title' => 'Benefit',
      'desc'      => 'Di bawah ini adalah data benefit website beasiswa bank indonesia',
      'benefits'  => $this->AMM->getBenefits()->result()
    ];
    $page = '/backoffice/admin/benefit/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Benefit | beasiswabilampung.com',
        'sub_title' => 'Tambah Benefit',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/benefit/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $benefit_logo       = $_FILES['benefit_logo'];

      if ($benefit_logo != '') {
        $config['upload_path'] = './assets/official/img/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('benefit_logo')) {
          $benefit_logo = "";
        } else {
          $benefit_logo = $this->upload->data('file_name');
        }
      }

      $setValue = [
        'title'           => $data['title'],
        'benefit_logo'    => $benefit_logo,
      ];

      $save = $this->AMM->save($setValue, 'benefit');
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $benefit      = $this->AMM->getBenefits(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Benefit | beasiswabilampung.com',
        'sub_title' => 'Edit Benefit',
        'benefit'   => $benefit,
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/benefit/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($benefit));
      $data = $this->input->post();
      $benefit_logo       = $_FILES['benefit_logo'];

      if ($benefit_logo != '') {
        $config['upload_path'] = './assets/official/img/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('benefit_logo')) {
          $benefit_logo = "";
        } else {
          $benefit_logo = $this->upload->data('file_name');
        }
      }

      if ($benefit_logo == '') {
        $setValue = [
          'id'              => $data['id'],
          'title'           => $data['title'],
          'benefit_logo'    => $data['benefit_logo_old'],
        ];
      } else {
        $setValue = [
          'id'              => $data['id'],
          'title'           => $data['title'],
          'benefit_logo'    => $benefit_logo,
        ];
      }
      $save = $this->AMM->save($setValue, 'benefit');
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->AMM->getBenefits(['id' => $id])->row();
    if ($data) {
      $delete    = $this->AMM->delete(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Manfaat',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

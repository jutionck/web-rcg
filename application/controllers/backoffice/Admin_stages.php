
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_stages extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_stage_model', 'Stage');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/alur';
  }

  public function index()
  {
    $data = [
      'title'     => 'Alur | beasiswabilampung.com',
      'sub_title' => 'Alur',
      'desc'      => 'Di bawah ini adalah data alur website beasiswa bank indonesia',
      'stages'    => $this->Stage->getStages()->result()
    ];
    $page = '/backoffice/admin/stage/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Alur | beasiswabilampung.com',
        'sub_title' => 'Tambah Alur',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/stage/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();

      $setValue = [
        'title'       => $data['title'],
        'description' => $data['description'],
      ];

      $save = $this->Stage->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->Stage->getStages(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Alur | beasiswabilampung.com',
        'sub_title' => 'Edit Alur',
        'stage'     => $getId,
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/stage/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();

      $setValue = [
        'id'          => $data['id'],
        'title'       => $data['title'],
        'description' => $data['description'],
      ];
      $save = $this->Stage->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->Stage->getStages(['id' => $id])->row();
    if ($data) {
      $delete    = $this->Stage->delete(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Alur',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'description',
      'Deskripsi',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

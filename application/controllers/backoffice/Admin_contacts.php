
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_contacts extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_contacts_model', 'Contact');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/contacts';
  }

  public function index()
  {
    $contact = $this->Contact->getContacts()->row();
    $this->_validation();
    if ($this->form_validation->run() == false) {
      $data = [
        'title'     => 'Contact | beasiswabilampung.com',
        'sub_title' => 'Contact',
        'desc'      => 'Di bawah ini adalah data contact website beasiswa bank indonesia',
        'contact'     => $contact
      ];
      $page = '/backoffice/admin/contact/index';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($contact));
      $data = $this->input->post();
      $setValue = [
        'id'          => $data['id'],
        'address'     => $data['address'],
        'telp'        => $data['telp'],
        'email'       => $data['email'],
        'other_email' => $data['other_email'],
        'open_hours'  => $data['open_hours'],
      ];

      $save = $this->Contact->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'address',
      'Alamat',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'telp',
      'Telphone',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'email',
      'Email',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'open_hours',
      'Jam operasional',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

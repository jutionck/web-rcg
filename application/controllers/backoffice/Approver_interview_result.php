
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approver_interview_result extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_interview_model', 'OP');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'approver';
    cek_login('Approver');
    $this->redirectUrl = 'backoffice/approver/hasil_wawancara/';
  }

  public function index()
  {
    $university      = $this->session->userdata('username');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $pendaftar       = $this->OP->getPendaftarWithUniversityFinal(['YEAR(a.created_at)' => $tahun, 'a.university_id' => $university->university_id, 'c.recomendation' => 'Lulus'])->result();
    } else {
      $pendaftar       = $this->OP->getPendaftarWithUniversityFinal('', $university->university_id)->result();
    }

    $data = [
      'title'       => 'Hasil Wawancara | beasiswabilampung.com',
      'sub_title'   => 'Pendaftar Wawancara Lulus',
      'desc'        => 'Di bawah ini adalah data pendaftar wawancara beasiswa bank indonesia yang lulus',
      'pendaftar'   => $pendaftar,
      'university'  => $this->University->getUniversity()->result(),
      'periode'     => $this->Setting->getPeriod('', '', 'evaluation')->row(),
    ];
    $page = '/backoffice/approver/interview_result/index';
    pageBackend($this->role, $page, $data);
  }

  public function evaluation($id)
  {
    $get        = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $evaluation  = $this->OP->getDataEvaluation($id)->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Evaluasi | beasiswabilampung.com',
        'sub_title'   => 'Evaluasi',
        'desc'        => 'Di bawah ini adalah form evaluasi penerima beasiswa bank indonesia',
        'pendaftar'   => $get,
        'evaluation'  => $evaluation,
        'option'      => $this->OP->getOptionValue(),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/approver/interview_result/evaluasi';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $transcript = $_FILES['transcript_file'];
      if ($transcript != '') {
        $new_name = $data['applicants_id'] . '_' . $_FILES["transcript_file"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/transcript/evaluation/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('transcript_file')) {
          $transcript = "";
        } else {
          $transcript = $this->upload->data('file_name');
        }
      }
      if (@$evaluation->id) {
        $setValue = [
          'id'                          => $data['id'],
          'applicants_id'               => $data['applicants_id'],
          'gpa_evaluation'              => $data['gpa_evaluation'],
          'paid_leave'                  => $data['paid_leave'],
          'receive_another_scholarship' => $data['receive_another_scholarship'],
          'description'                 => $data['description'],
          'evaluation_result'           => $data['evaluation_result'],
          'transcript_file'             => $transcript,
        ];
      } else {
        $setValue = [
          'applicants_id'               => $data['applicants_id'],
          'gpa_evaluation'              => $data['gpa_evaluation'],
          'paid_leave'                  => $data['paid_leave'],
          'receive_another_scholarship' => $data['receive_another_scholarship'],
          'description'                 => $data['description'],
          'evaluation_result'           => $data['evaluation_result'],
          'transcript_file'             => $transcript,
        ];
      }
      $save = $this->OP->saveEvaluation($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function evaluationReport()
  {
    $university      = $this->session->userdata('username');
    $tahun           = $this->input->get('tahun');
    if ($university && $tahun) {
      $evaluations   = $this->OP->getEvaluationReport($tahun, $university->university_id)->result();
    } else {
      $evaluations     = $this->OP->getEvaluationReport(null, $university->university_id, true)->result();
    }

    $data = [
      'title'       => 'Hasil Evaluasi | beasiswabilampung.com',
      'sub_title'   => 'Hasil Evaluasi Pendaftar',
      'desc'        => 'Di bawah ini adalah data evaluasi pendaftar beasiswa bank indonesia yang lulus',
      'evaluations' => $evaluations,
      'university'  => $this->University->getUniversity()->result(),
      'periode'     => $this->Setting->getPeriod('', '', 'evaluation')->row(),
    ];
    $page = '/backoffice/approver/evaluation/index';
    pageBackend($this->role, $page, $data);
  }

  public function evaluationReportUpdate($id)
  {
    $get        = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $evaluation  = $this->OP->getDataEvaluation($id)->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Ubah Data Evaluasi | beasiswabilampung.com',
        'sub_title'   => 'Ubah Data  Evaluasi',
        'desc'        => 'Di bawah ini adalah form ubah data evaluasi penerima beasiswa bank indonesia',
        'pendaftar'   => $get,
        'evaluation'  => $evaluation,
        'option'      => $this->OP->getOptionValue(),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/approver/evaluation/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $transcript = $_FILES['transcript_file'];
      if ($transcript != '') {
        $new_name = $data['applicants_id'] . '_' . $_FILES["transcript_file"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/transcript/evaluation/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('transcript_file')) {
          $transcript = "";
        } else {
          $transcript = $this->upload->data('file_name');
        }
      }
      $setValue = [
        'id'                          => $data['id'],
        'applicants_id'               => $data['applicants_id'],
        'gpa_evaluation'              => $data['gpa_evaluation'],
        'paid_leave'                  => $data['paid_leave'],
        'receive_another_scholarship' => $data['receive_another_scholarship'],
        'description'                 => $data['description'],
        'evaluation_result'           => $data['evaluation_result'],
        'transcript_file'             => $transcript,
      ];
      $save = $this->OP->saveEvaluation($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect('backoffice/approver/hasil_evaluasi');
    }
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'gpa_evaluation',
      'Keterangan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

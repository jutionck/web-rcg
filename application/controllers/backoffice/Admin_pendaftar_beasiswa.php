
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_pendaftar_beasiswa extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_pendaftar_beasiswa_model', 'OP');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/pendaftar_beasiswa';
  }

  public function index()
  {
    $university      = $this->input->get('university');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['YEAR(a.created_at)' => $tahun, 'a.university_id' => $university])->result();
    } else {
      $pendaftar       = $this->OP->getPendaftarWithUniversity()->result();
    }

    $data = [
      'title'       => 'Pendaftar Beasiswa | beasiswabilampung.com',
      'sub_title'   => 'Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data pendaftar beasiswa bank indonesia',
      'pendaftar'   => $pendaftar,
      'university'  => $this->University->getUniversity()->result()
    ];
    $page = '/backoffice/admin/pendaftar/index';
    pageBackend($this->role, $page, $data);
  }

  public function detail($id)
  {
    $get = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $data = [
      'title'       => 'Detail Pendaftar | beasiswabilampung.com',
      'sub_title'   => 'Detail Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data detail pendaftar beasiswa bank indonesia',
      'pendaftar'   => $get,
      'age'         => calculateAge($get->birth_date),
      'redirect'    => $this->redirectUrl
    ];
    $page = '/backoffice/admin/pendaftar/detail';
    pageBackend($this->role, $page, $data);
  }

  public function delete($id)
  {
    $data      = $this->OP->getPendaftar(['id' => $id])->row();
    if ($data) {
      $delete       = $this->OP->delete(['id' => $id]);
      if ($delete > 0) {
        $this->OP->deleteRefInterview(['applicants_id' => $id]);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_users extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_users_model', 'User');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/user';
  }

  public function index()
  {
    $data = [
      'title'     => 'User Akun | beasiswabilampung.com',
      'sub_title' => 'User Akun',
      'desc'      => 'Di bawah ini adalah data User Akun website beasiswa bank indonesia',
      'studentUser'     => $this->User->getUserWithRoleUniveristy('Student')->result(),
      'approverUser'    => $this->User->getUserWithRoleUniveristy('Approver')->result(),
      'operatorUser'    => $this->User->getUserWithRoleUniveristy('Operator')->result(),
      'interviewerUser' => $this->User->getUserWithRoleInterviewer()->result(),
    ];
    $page = '/backoffice/admin/users/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation('insert');
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'User Akun | beasiswabilampung.com',
        'sub_title' => 'Tambah user akun',
        'roles'     => $this->User->getRoles()->result(),
        'university' => $this->University->getUniversity()->result(),
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/users/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();

      $setValue = [
        'username'      => $data['username'],
        'password'      => password_hash($data['password'], PASSWORD_DEFAULT),
        'email'         => $data['email'],
        'role_id'       => $data['role_id'],
        'university_id' => $data['university_id'],
      ];

      $save = $this->User->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Tambah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->User->getUsers(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'User Akun | beasiswabilampung.com',
        'sub_title'   => 'Edit user akun',
        'user'        => $getId,
        'roles'       => $this->User->getRoles()->result(),
        'university'  => $this->University->getUniversity()->result(),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/admin/users/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();

      if ($data['password']) {
        $setValue = [
          'id'            => $data['id'],
          'username'      => $data['username'],
          'password'      => password_hash($data['password'], PASSWORD_DEFAULT),
          'email'         => $data['email'],
          'role_id'       => $data['role_id'],
          'university_id' => $data['university_id'],
        ];
      } else {
        $setValue = [
          'id'            => $data['id'],
          'username'      => $data['username'],
          'email'         => $data['email'],
          'role_id'       => $data['role_id'],
          'university_id' => $data['university_id'],
        ];
      }

      $save = $this->User->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->User->getUsers(['id' => $id])->row();
    if ($data) {
      $delete    = $this->User->delete(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation($type = null)
  {
    if ($type == 'insert') {
      $this->form_validation->set_rules(
        'username',
        'Username',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'password',
        'Password',
        'trim|required|min_length[6]',
        [
          'required'    => '%s wajib di isi',
          'min_length'  => '%s wajib minimal 6 karakter'
        ]
      );

      $this->form_validation->set_rules(
        'passconf',
        'Password Confirmation',
        'trim|required|matches[password]',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'role_id',
        'Role user',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'university_id',
        'Kampus',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );
    } else {
      $this->form_validation->set_rules(
        'username',
        'Username',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'role_id',
        'Role user',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'university_id',
        'Kampus',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );
    }
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_article extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_article_model', 'Article');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/artikel';
  }

  public function index()
  {
    $data = [
      'title'     => 'Artikel | beasiswabilampung.com',
      'sub_title' => 'Artikel',
      'desc'      => 'Di bawah ini adalah data artikel website beasiswa bank indonesia',
      'articles'  => $this->Article->getArticles()->result()
    ];
    $page = '/backoffice/admin/article/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Artikel | beasiswabilampung.com',
        'sub_title' => 'Tambah Artikel',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/article/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $image = $_FILES['image'];
      if ($image != '') {
        $new_name = $data['slug'] . '_' . $_FILES["image"]['name'];
        $config['upload_path'] = './assets/official/img/blog/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
          $image = "";
        } else {
          $image = $this->upload->data('file_name');
        }
      }
      $setValue = [
        'title'       => $data['title'],
        'slug'        => newSlug($data['title']),
        'description' => $data['description'],
        'author'      => $data['author'],
        'image'       => $image,
      ];

      $save = $this->Article->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->Article->getArticles(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Artikel | beasiswabilampung.com',
        'sub_title' => 'Edit Artikel',
        'article'   => $getId,
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/article/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();
      $image = $_FILES['image'];
      if ($image != '') {
        $new_name = $data['slug'] . '_' . $_FILES["image"]['name'];
        $config['upload_path'] = './assets/official/img/blog/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
          $image = "";
        } else {
          $image = $this->upload->data('file_name');
        }
      }
      if ($image == '') {
        $setValue = [
          'id'          => $data['id'],
          'title'       => $data['title'],
          'slug'        => newSlug($data['title']),
          'description' => $data['description'],
          'author'      => $data['author'],
          'image'       => $data['image_old'],
        ];
      } else {
        $setValue = [
          'id'          => $data['id'],
          'title'       => $data['title'],
          'slug'        => newSlug($data['title']),
          'description' => $data['description'],
          'author'      => $data['author'],
          'image'       => $image,
        ];
      }
      $save = $this->Article->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->Article->getArticles(['id' => $id])->row();
    if ($data) {
      $delete    = $this->Article->delete(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Judul',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'description',
      'Deskripsi',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

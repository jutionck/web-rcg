
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Interviewer_dashboard extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->role = 'interviewer';
    cek_login('Interviewer');
  }

  public function index()
  {
    $data = [
      'title' => 'Dashboard | beasiswabilampung.com',
    ];

    $page = '/backoffice/interviewer/dashboard';
    pageBackend($this->role, $page, $data);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operator_pendaftar_beasiswa extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Operator_pendaftar_beasiswa_model', 'OP');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'operator';
    cek_login('Operator');
    $this->redirectUrl = 'backoffice/operator/pendaftar_beasiswa';
  }

  public function index()
  {
    $getUniversityId = $this->session->userdata('username');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['university_id' => $getUniversityId->university_id], $tahun)->result();
    } else {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['university_id' => $getUniversityId->university_id])->result();
    }
    $data = [
      'title'       => 'Pendaftar Beasiswa | beasiswabilampung.com',
      'sub_title'   => 'Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data pendaftar beasiswa bank indonesia',
      'pendaftar'   => $pendaftar,
      'redirect'    => $this->redirectUrl,
      'periode'     => $this->Setting->getPeriod('','','registration')->row(),
    ];
    $page = '/backoffice/operator/pendaftar/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Data Pendaftar | beasiswabilampung.com',
        'sub_title'   => 'Tambah Data Pendaftar',
        'university'  => $this->University->getUniversityByUser(['username' => $this->session->userdata('user')])->result(),
        'blood'       => $this->OP->getBloodGroup(),
        'religion'    => $this->OP->getReligion(),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/operator/pendaftar/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $photo                    = $_FILES['photo'];
      $ktp                      = $_FILES['ktp'];
      $student_identity         = $_FILES['student_identity'];
      $transcript               = $_FILES['transcript'];
      $sktm                     = $_FILES['sktm'];
      $academic_recomendation   = $_FILES['academic_recomendation'];
      $statement_letter         = $_FILES['statement_letter'];
      $motivation_letter        = $_FILES['motivation_letter'];
      $organization_achievement = $_FILES['organization_achievement'];

      if ($photo != '') {
        $new_name = $data['npm'] . '_' . $_FILES["photo"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/photo/';
        $config['allowed_types'] = 'jpg|jpeg';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('photo')) {
          $photo = "";
        } else {
          $photo = $this->upload->data('file_name');
        }
      }

      if ($ktp != '') {
        $new_name = $data['npm'] . '_' . $_FILES["ktp"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/ktp/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('ktp')) {
          $ktp = "";
        } else {
          $ktp = $this->upload->data('file_name');
        }
      }

      if ($student_identity != '') {
        $new_name = $data['npm'] . '_' . $_FILES["student_identity"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/student_identity/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('student_identity')) {
          $student_identity = "";
        } else {
          $student_identity = $this->upload->data('file_name');
        }
      }

      if ($transcript != '') {
        $new_name = $data['npm'] . '_' . $_FILES["transcript"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/transcript/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('transcript')) {
          $transcript = "";
        } else {
          $transcript = $this->upload->data('file_name');
        }
      }

      if ($sktm != '') {
        $new_name = $data['npm'] . '_' . $_FILES["sktm"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/sktm/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('sktm')) {
          $sktm = "";
        } else {
          $sktm = $this->upload->data('file_name');
        }
      }

      if ($academic_recomendation != '') {
        $new_name = $data['npm'] . '_' . $_FILES["academic_recomendation"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/academic_recomendation/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('academic_recomendation')) {
          $academic_recomendation = "";
        } else {
          $academic_recomendation = $this->upload->data('file_name');
        }
      }

      if ($statement_letter != '') {
        $new_name = $data['npm'] . '_' . $_FILES["statement_letter"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/statement_letter/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('statement_letter')) {
          $statement_letter = "";
        } else {
          $statement_letter = $this->upload->data('file_name');
        }
      }

      if ($motivation_letter != '') {
        $new_name = $data['npm'] . '_' . $_FILES["motivation_letter"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/motivation_letter/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('motivation_letter')) {
          $motivation_letter = "";
        } else {
          $motivation_letter = $this->upload->data('file_name');
        }
      }

      if ($organization_achievement != '') {
        $new_name = $data['npm'] . '_' . $_FILES["organization_achievement"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/organization_achievement/';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('organization_achievement')) {
          $organization_achievement = "";
        } else {
          $organization_achievement = $this->upload->data('file_name');
        }
      }

      if ($sktm) {
        $setValue = [
          'name'                          => htmlspecialchars($data['name']),
          'npm'                           => $data['npm'],
          'place_of_birth'                => $data['place_of_birth'],
          'birth_date'                    => $data['birth_date'],
          'faculty'                       => $data['faculty'],
          'major'                         => $data['major'],
          'university_id'                 => $data['university_id'],
          'gp'                            => $data['gp'],
          'gpa'                           => $data['gpa'],
          'number_of_credits'             => $data['number_of_credits'],
          'semester'                      => $data['semester'],
          'gender'                        => $data['gender'],
          'religion'                      => $data['religion'],
          'blood'                         => $data['blood'],
          'ethnic'                        => htmlspecialchars($data['ethnic']),
          'address'                       => htmlspecialchars($data['address']),
          'domicile'                      => htmlspecialchars($data['domicile']),
          'email'                         => strtolower($data['email']),
          'instagram'                     => $data['instagram'],
          'facebook'                      => $data['facebook'],
          'twitter'                       => $data['twitter'],
          'linkedin'                      => $data['linkedin'],
          'no_hp'                         => $data['no_hp'],
          'father'                        => htmlspecialchars($data['father']),
          'father_occupation'             => $data['father_occupation'],
          'mother'                        => htmlspecialchars($data['mother']),
          'mother_occupation'             => $data['mother_occupation'],
          'interest_talent'               => $data['interest_talent'],
          'link_interest_talent'          => $data['link_interest_talent'],
          'life_skill'                    => $data['life_skill'],
          'self_potential'                => $data['self_potential'],
          'activity'                      => $data['activity'],
          'genbi_'                        => $data['genbi_'],
          'reason_of_genbi'               => $data['reason_of_genbi'],
          'suggestion_for_genbi'          => $data['suggestion_for_genbi'],
          'sktm_option'                   => $data['sktm_option'],
          'photo'                         => $photo,
          'ktp_file'                      => $ktp,
          'student_identity_file'         => $student_identity,
          'transcript_file'               => $transcript,
          'sktm_file'                     => $sktm,
          'academic_recomend_file'        => $academic_recomendation,
          'statement_letter_file'         => $statement_letter,
          'motivation_letter_file'        => $motivation_letter,
          'organization_achievement_file' => $organization_achievement,
        ];
      } else {
        $setValue = [
          'name'                          => htmlspecialchars($data['name']),
          'npm'                           => $data['npm'],
          'place_of_birth'                => $data['place_of_birth'],
          'birth_date'                    => $data['birth_date'],
          'faculty'                       => $data['faculty'],
          'major'                         => $data['major'],
          'university_id'                 => $data['university_id'],
          'gp'                            => $data['gp'],
          'gpa'                           => $data['gpa'],
          'number_of_credits'             => $data['number_of_credits'],
          'semester'                      => $data['semester'],
          'gender'                        => $data['gender'],
          'religion'                      => $data['religion'],
          'blood'                         => $data['blood'],
          'ethnic'                        => htmlspecialchars($data['ethnic']),
          'address'                       => htmlspecialchars($data['address']),
          'domicile'                      => htmlspecialchars($data['domicile']),
          'email'                         => strtolower($data['email']),
          'instagram'                     => $data['instagram'],
          'facebook'                      => $data['facebook'],
          'twitter'                       => $data['twitter'],
          'linkedin'                      => $data['linkedin'],
          'no_hp'                         => $data['no_hp'],
          'father'                        => htmlspecialchars($data['father']),
          'father_occupation'             => $data['father_occupation'],
          'mother'                        => htmlspecialchars($data['mother']),
          'mother_occupation'             => $data['mother_occupation'],
          'interest_talent'               => $data['interest_talent'],
          'link_interest_talent'          => $data['link_interest_talent'],
          'life_skill'                    => $data['life_skill'],
          'self_potential'                => $data['self_potential'],
          'activity'                      => $data['activity'],
          'genbi_'                        => $data['genbi_'],
          'reason_of_genbi'               => $data['reason_of_genbi'],
          'suggestion_for_genbi'          => $data['suggestion_for_genbi'],
          'motivation_to_apply'           => $data['motivation_to_apply'],
          'sktm_option'                   => $data['sktm_option'],
          'photo'                         => $photo,
          'ktp_file'                      => $ktp,
          'student_identity_file'         => $student_identity,
          'transcript_file'               => $transcript,
          'academic_recomend_file'        => $academic_recomendation,
          'statement_letter_file'         => $statement_letter,
          'motivation_letter_file'        => $motivation_letter,
          'organization_achievement_file' => $organization_achievement,
        ];
      }

      $save = $this->OP->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function detail($id)
  {
    $get = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $age = calculateAge($get->birth_date);
    $data = [
      'title'       => 'Detail Pendaftar | beasiswabilampung.com',
      'sub_title'   => 'Detail Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data detail pendaftar beasiswa bank indonesia',
      'pendaftar'   => $get,
      'age'         => $age,
      'redirect'    => $this->redirectUrl
    ];
    $page = '/backoffice/operator/pendaftar/detail';
    pageBackend($this->role, $page, $data);
  }

  public function update($id)
  {
    $pendaftar = $this->OP->getPendaftar(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Data Pendaftar | beasiswabilampung.com',
        'sub_title'   => 'Edit Data Pendaftar',
        'pendaftar'   => $pendaftar,
        'university'  => $this->University->getUniversityByUser(['username' => $this->session->userdata('user')])->result(),
        'blood'       => $this->OP->getBloodGroup(),
        'religion'    => $this->OP->getReligion(),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/operator/pendaftar/edit_new';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($pendaftar));
      $data = $this->input->post();
      $photo                    = $_FILES['photo'];

      if ($photo != '') {
        $new_name = $data['npm'] . '_' . $_FILES["photo"]['name'];
        $config['upload_path'] = './assets/backoffice/upload/photo/';
        $config['allowed_types'] = 'jpg|jpeg';
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('photo')) {
          $photo = "";
        } else {
          $photo = $this->upload->data('file_name');
        }
      }

      if ($photo == '') {
        $setValue = [
          'id'                            => $data['id'],
          'name'                          => htmlspecialchars($data['name']),
          'npm'                           => $data['npm'],
          'place_of_birth'                => $data['place_of_birth'],
          'birth_date'                    => $data['birth_date'],
          'faculty'                       => $data['faculty'],
          'major'                         => $data['major'],
          'university_id'                 => $data['university_id'],
          'gp'                            => $data['gp'],
          'gpa'                           => $data['gpa'],
          'number_of_credits'             => $data['number_of_credits'],
          'semester'                      => $data['semester'],
          'gender'                        => $data['gender'],
          'religion'                      => $data['religion'],
          'blood'                         => $data['blood'],
          'ethnic'                        => htmlspecialchars($data['ethnic']),
          'address'                       => htmlspecialchars($data['address']),
          'domicile'                      => htmlspecialchars($data['domicile']),
          'email'                         => strtolower($data['email']),
          'instagram'                     => $data['instagram'],
          'facebook'                      => $data['facebook'],
          'twitter'                       => $data['twitter'],
          'linkedin'                      => $data['linkedin'],
          'no_hp'                         => $data['no_hp'],
          'father'                        => htmlspecialchars($data['father']),
          'father_occupation'             => $data['father_occupation'],
          'mother'                        => htmlspecialchars($data['mother']),
          'mother_occupation'             => $data['mother_occupation'],
          'interest_talent'               => $data['interest_talent'],
          'link_interest_talent'          => $data['link_interest_talent'],
          'life_skill'                    => $data['life_skill'],
          'self_potential'                => $data['self_potential'],
          'activity'                      => $data['activity'],
          'genbi_'                        => $data['genbi_'],
          'reason_of_genbi'               => $data['reason_of_genbi'],
          'suggestion_for_genbi'          => $data['suggestion_for_genbi'],
          'sktm_option'                   => $data['sktm_option'],
          'photo'                         => $data['photo_old'],
        ];
      } else {
        $setValue = [
          'id'                            => $data['id'],
          'name'                          => htmlspecialchars($data['name']),
          'npm'                           => $data['npm'],
          'place_of_birth'                => $data['place_of_birth'],
          'birth_date'                    => $data['birth_date'],
          'faculty'                       => $data['faculty'],
          'major'                         => $data['major'],
          'university_id'                 => $data['university_id'],
          'gp'                            => $data['gp'],
          'gpa'                           => $data['gpa'],
          'number_of_credits'             => $data['number_of_credits'],
          'semester'                      => $data['semester'],
          'gender'                        => $data['gender'],
          'religion'                      => $data['religion'],
          'blood'                         => $data['blood'],
          'ethnic'                        => htmlspecialchars($data['ethnic']),
          'address'                       => htmlspecialchars($data['address']),
          'domicile'                      => htmlspecialchars($data['domicile']),
          'email'                         => strtolower($data['email']),
          'instagram'                     => $data['instagram'],
          'facebook'                      => $data['facebook'],
          'twitter'                       => $data['twitter'],
          'linkedin'                      => $data['linkedin'],
          'no_hp'                         => $data['no_hp'],
          'father'                        => htmlspecialchars($data['father']),
          'father_occupation'             => $data['father_occupation'],
          'mother'                        => htmlspecialchars($data['mother']),
          'mother_occupation'             => $data['mother_occupation'],
          'interest_talent'               => $data['interest_talent'],
          'link_interest_talent'          => $data['link_interest_talent'],
          'life_skill'                    => $data['life_skill'],
          'self_potential'                => $data['self_potential'],
          'activity'                      => $data['activity'],
          'genbi_'                        => $data['genbi_'],
          'reason_of_genbi'               => $data['reason_of_genbi'],
          'suggestion_for_genbi'          => $data['suggestion_for_genbi'],
          'sktm_option'                   => $data['sktm_option'],
          'photo'                         => $photo,
        ];
      }
      $save = $this->OP->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect(base_url('backoffice/operator/pendaftar_beasiswa/edit/' . $data['id']));
    }
  }

  public function update_berkas()
  {
    $data = $this->input->post();
    $ktp                      = $_FILES['ktp'];
    $student_identity         = $_FILES['student_identity'];
    $transcript               = $_FILES['transcript'];
    $sktm                     = $_FILES['sktm'];
    $academic_recomendation   = $_FILES['academic_recomendation'];
    $statement_letter         = $_FILES['statement_letter'];
    $motivation_letter        = $_FILES['motivation_letter'];
    $organization_achievement = $_FILES['organization_achievement'];

    if ($ktp != '') {
      $new_name = $data['npm'] . '_' . $_FILES["ktp"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/ktp/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('ktp')) {
        $ktp = "";
      } else {
        $ktp = $this->upload->data('file_name');
      }
    }

    if ($student_identity != '') {
      $new_name = $data['npm'] . '_' . $_FILES["student_identity"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/student_identity/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('student_identity')) {
        $student_identity = "";
      } else {
        $student_identity = $this->upload->data('file_name');
      }
    }

    if ($transcript != '') {
      $new_name = $data['npm'] . '_' . $_FILES["transcript"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/transcript/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('transcript')) {
        $transcript = "";
      } else {
        $transcript = $this->upload->data('file_name');
      }
    }

    if ($sktm != '') {
      $new_name = $data['npm'] . '_' . $_FILES["sktm"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/sktm/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('sktm')) {
        $sktm = "";
      } else {
        $sktm = $this->upload->data('file_name');
      }
    }

    if ($academic_recomendation != '') {
      $new_name = $data['npm'] . '_' . $_FILES["academic_recomendation"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/academic_recomendation/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('academic_recomendation')) {
        $academic_recomendation = "";
      } else {
        $academic_recomendation = $this->upload->data('file_name');
      }
    }

    if ($statement_letter != '') {
      $new_name = $data['npm'] . '_' . $_FILES["statement_letter"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/statement_letter/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('statement_letter')) {
        $statement_letter = "";
      } else {
        $statement_letter = $this->upload->data('file_name');
      }
    }

    if ($motivation_letter != '') {
      $new_name = $data['npm'] . '_' . $_FILES["motivation_letter"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/motivation_letter/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('motivation_letter')) {
        $motivation_letter = "";
      } else {
        $motivation_letter = $this->upload->data('file_name');
      }
    }

    if ($organization_achievement != '') {
      $new_name = $data['npm'] . '_' . $_FILES["organization_achievement"]['name'];
      $config['upload_path'] = './assets/backoffice/upload/organization_achievement/';
      $config['allowed_types'] = 'jpg|jpeg|pdf';
      $config['file_name'] = $new_name;
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('organization_achievement')) {
        $organization_achievement = "";
      } else {
        $organization_achievement = $this->upload->data('file_name');
      }
    }

    $setValue = [
      'id'                            => $data['id'],
      'sktm_option'                   => $data['sktm_option'],
      'ktp_file'                      => $ktp,
      'student_identity_file'         => $student_identity,
      'transcript_file'               => $transcript,
      'sktm_file'                     => @$sktm,
      'academic_recomend_file'        => $academic_recomendation,
      'statement_letter_file'         => $statement_letter,
      'motivation_letter_file'        => $motivation_letter,
      'organization_achievement_file' => $organization_achievement,
    ];

    $save = $this->OP->save($setValue);
    if ($save > 0) {
      $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
    } else {
      $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
    }
    redirect(base_url('backoffice/operator/pendaftar_beasiswa/edit/' . $data['id']));
  }

  public function delete($id)
  {
    $data      = $this->OP->getPendaftar(['id' => $id])->row();
    if ($data) {
      $delete       = $this->OP->delete(['id' => $id]);
      if ($delete > 0) {
        $this->OP->deleteRefInterview(['applicants_id' => $id]);
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'name',
      'Nama mahasiswa',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'npm',
      'Nomor pokok mahasiswa',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'place_of_birth',
      'Tempat lahir',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'birth_date',
      'Tanggal lahir',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'ethnic',
      'Suku bangsa',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'faculty',
      'Fakultas',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'major',
      'Jurusan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'number_of_credits',
      'Jumlah SKS',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'semester',
      'Semester',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'gpa',
      'IPK',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'gp',
      'IP Semester terakhir',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'address',
      'Alamat',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'domicile',
      'Alamat domisili',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'no_hp',
      'No handphone',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'father',
      'Nama ayah',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'father_occupation',
      'Pekerjaan ayah',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'mother',
      'Nama ibut',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'mother_occupation',
      'Pekerjaan ibu',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'interest_talent',
      'Minat dan bakat',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'life_skill',
      'Keterampilan diri',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'self_potential',
      'Potensi diri',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'activity',
      'Aktivitas kampus',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'suggestion_for_genbi',
      'Usulan untuk GenBI',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

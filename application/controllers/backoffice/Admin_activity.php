
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_activity extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_activity_model', 'Activity');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirect = 'backoffice/admin/master/kegiatan';
    $this->redirectUrl = 'backoffice/admin/kegiatan_mahasiswa';
  }

  public function index()
  {
    $data = [
      'title'       => 'Kegiatan | beasiswabilampung.com',
      'sub_title'   => 'Kegiatan',
      'desc'        => 'Di bawah ini adalah data Kegiatan website beasiswa bank indonesia',
      'activities'  => $this->Activity->getActivity()->result()
    ];
    $page = '/backoffice/admin/activity/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation('insert');
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Kegiatan | beasiswabilampung.com',
        'sub_title' => 'Tambah Kegiatan',
        'redirect'  => $this->redirect
      ];
      $page = '/backoffice/admin/activity/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $setValue = [
        'title'         => $data['title'],
        'location'      => $data['location'],
        'activity_link' => $data['activity_link'],
        'activity_date' => $data['activity_date'],
        'activity_time' => $data['activity_time'],
      ];

      $save = $this->Activity->save($setValue);
      if ($save > 0) {
        // send to student
        // $studentMails = $this->Setting->getStudentMail()->result();
        // foreach ($studentMails as $row) {
        //   $this->sendMail($row->email, $data['title'], $data['activity_date']);
        // }

        // send to admin or univ
        $adminUnivMail = $this->Setting->getAdminUnivMail()->result();
        foreach ($adminUnivMail as $row) {
          $this->sendMail($row->email, $data['title'], $data['activity_date']);
        }
        $this->session->set_flashdata('success', '<b>Tambah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirect);
    }
  }

  public function update($id)
  {
    $getId      = $this->Activity->getActivity(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Kegiatan | beasiswabilampung.com',
        'sub_title'   => 'Edit Kegiatan',
        'activity'        => $getId,
        'redirect'    => $this->redirect
      ];
      $page = '/backoffice/admin/activity/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();
      $setValue = [
        'id'            => $data['id'],
        'title'         => $data['title'],
        'location'      => $data['location'],
        'activity_link' => $data['activity_link'],
        'activity_date' => $data['activity_date'],
        'activity_time' => $data['activity_time'],
      ];
      $save = $this->Activity->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirect);
    }
  }

  public function delete($id)
  {
    $data      = $this->Activity->getActivity(['id' => $id])->row();
    if ($data) {
      $delete    = $this->Activity->delete(['id' => $id]);
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirect);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Nama kegiatan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'location',
      'Lokasi kegoatan',
      'trim|required',
      [
        'required'    => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'activity_date',
      'Tanggal kegiatan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'activity_time',
      'Waktu kegiatan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }

  public function studentActivity()
  {
    $university      = $this->input->get('university');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $activities       = $this->Activity->getActivityWithUniversity(['YEAR(a.created_at)' => $tahun, 'b.university_id' => $university, 'b.email'])->result();
    } else {
      $activities       = $this->Activity->getActivityWithUniversity()->result();
    }
    $data = [
      'title'       => 'Kegiatan Mahasiswa | beasiswabilampung.com',
      'sub_title'   => 'Kegiatan Mahasiswa',
      'desc'        => 'Di bawah ini adalah data kegiatan mahasiswa beasiswa bank indonesia',
      'activities'   => $activities,
      'university'  => $this->University->getUniversity()->result(),
      'redirect'    =>  $this->redirectUrl,
    ];
    $page = '/backoffice/admin/activity/student_activity';
    pageBackend($this->role, $page, $data);
  }

  public function studentActivityDetail($applicant_id)
  {
    $activity = $this->Activity->getActivityWithUniversityDetail(['a.applicants_id' => $applicant_id])->row();
    $activities = $this->Activity->getActivityWithUniversityDetail(['a.applicants_id' => $applicant_id])->result();
    $data = [
      'title'       => 'Detail Kegiatan Mahasiswa | beasiswabilampung.com',
      'sub_title'   => 'Detail Kegiatan Mahasiswa',
      'desc'        => 'Di bawah ini adalah detail data kegiatan mahasiswa beasiswa bank indonesia',
      'activities'  => $activities,
      'activity'    => $activity,
      'redirect'    =>  $this->redirectUrl,
    ];
    $page = '/backoffice/admin/activity/student_activity_detail';
    pageBackend($this->role, $page, $data);
  }

  private function sendMail($email, $acara, $tanggal)
  {
    $config = $this->Setting->emailConfig();
    $this->load->library('PHPMailer_load'); //Load Library PHPMailer
    $mail = $this->phpmailer_load->load(); // Mendefinisikan Variabel Mail
    $mail->isSMTP();  // Mengirim menggunakan protokol SMTP
    $mail->Host = $config->host; // Host dari server SMTP
    $mail->SMTPAuth = true; // Autentikasi SMTP
    $mail->Username = $config->email;
    $mail->Password = $config->password;
    $mail->SMTPSecure = $config->smtp_secure;
    $mail->Port = (int)$config->port;
    $mail->setFrom('noreply@beasiswabilampung.com', 'noreply'); // Sumber email
    $mail->addAddress($email); // Masukkan alamat email dari variabel $email
    $mail->Subject = "Kegiatan Baru Telah Ditambahkan"; // Subjek Email
    $body = emailBody($acara, $tanggal);
    $mail->msgHtml($body); // Isi email dengan format HTML
    if (!$mail->send()) {
    } else {
      //echo "Message sent!";
    } // Kirim email dengan cek kondisi
  }
}

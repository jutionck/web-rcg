
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_profile extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Student_profile_model', 'Profile');
    $this->role = 'student';
    cek_login('Student');
    $this->redirectUrl = 'backoffice/student/biodata';
  }

  public function index()
  {
    $profile = $this->Profile->GetProfile($this->session->userdata('username'))->row();
    
    $this->_validation();
    if ($this->form_validation->run() == false) {
      $data = [
        'title'       => 'Biodata | beasiswabilampung.com',
        'sub_title'   => 'Biodata',
        'profile'     => $profile,
      ];
      $page = '/backoffice/student/profile/index';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($profile));
      $data = $this->input->post();
      $photo       = $_FILES['photo'];
      if ($photo != '') {
        $config['upload_path'] = './assets/backoffice/upload/photo/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('photo')) {
          $photo = "";
        } else {
          $photo = $this->upload->data('file_name');
        }
      }
      if ($photo == '') {
        $setValue = [
          'id'          => $data['profile_id'],
          'name'        => $data['name'],
          'description' => $data['description'],
          'major'       => $data['major'],
          'npm'         => $data['npm'],
          'gpa'         => $data['gpa'],
          'no_hp'       => $data['no_hp'],
          'photo'       => $data['photo_old'],
        ];
      } else {
        $setValue = [
          'id'          => $data['profile_id'],
          'name'        => $data['name'],
          'description' => $data['description'],
          'major'       => $data['major'],
          'npm'         => $data['npm'],
          'gpa'         => $data['gpa'],
          'no_hp'       => $data['no_hp'],
          'photo'       => $photo,
        ];
      }
      $save = $this->Profile->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'name',
      'Nama lengkap',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'npm',
      'NPM',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

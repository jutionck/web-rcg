
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_berkas extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/berkas';
  }

  public function index()
  {
    $data = [
      'title'     => 'Berkas Final | beasiswabilampung.com',
      'sub_title' => 'Berkas Final',
      'desc'      => 'Di bawah ini adalah berkas final wawancara beasiswa bank indonesia',
      'berkas'    => $this->Setting->getBerkas()->result()
    ];
    $page = '/backoffice/admin/berkas/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Upload Berkas Final | beasiswabilampung.com',
        'sub_title' => 'Upload Berkas Final',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/berkas/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data       = $this->input->post();
      $file       = $_FILES['file'];
      if ($file != '') {
        $config['upload_path'] = './assets/backoffice/upload/interview_file';
        $config['allowed_types'] = 'pdf|xls|xlsx';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('file')) {
          $file = "";
        } else {
          $file = $this->upload->data('file_name');
        }
      }

      $setValue = [
        'title'       => $data['title'],
        'file'        => $file,
      ];

      $save = $this->Setting->saveBerkas($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->Setting->getBerkas(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Berkas Final | beasiswabilampung.com',
        'sub_title' => 'Edit Berkas Final',
        'berkas'    => $getId,
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/berkas/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();
      $file       = $_FILES['file'];

      if ($file != '') {
        $config['upload_path'] = './assets/backoffice/upload/interview_file';
        $config['allowed_types'] = 'pdf|xls|xlsx';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('file')) {
          $file = "";
        } else {
          $file = $this->upload->data('file_name');
        }
      }

      if ($file == '') {
        $setValue = [
          'id'    => $data['id'],
          'title' => $data['title'],
          'file'  => $data['file_old'],
        ];
      } else {
        $setValue = [
          'id'    => $data['id'],
          'title' => $data['title'],
          'file'  => $file,
        ];
      }

      $save = $this->Setting->saveBerkas($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->Setting->getBerkas(['id' => $id])->row();
    if ($data) {
      $delete    = $this->Setting->deleteBerkas(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Judul',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

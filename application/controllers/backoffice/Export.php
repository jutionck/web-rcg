<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Export extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Operator_pendaftar_beasiswa_model', 'OP');
    $this->load->model('backoffice/Setting_model', 'SM');
    $this->load->model('backoffice/Admin_activity_model', 'Activity');
  }

  public function form_pendaftar($id)
  {
    $mpdf = new \Mpdf\Mpdf([
      'mode' => 'utf-8',
      'format' => 'A4-P',
      'setAutoBottomMargin' => 'stretch',
      'default_font_size' => 7,
      'default_font' => 'dejavusans'
    ]);
    $pendaftar = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $data = [
      'pendaftar' => $pendaftar
    ];
    $view = $this->load->view('backoffice/exports/form_a1', $data, TRUE);
    $mpdf->SetProtection(array('print'));
    $mpdf->SetTitle("Form A.1");
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($view);
    $mpdf->Output("FORM A.1_" . $pendaftar->name . '.pdf', 'I');
  }

  public function exportFormDataMahasiswa($university_id)
  {
    $tahun          = $this->input->get('tahun');
    $getUniversity  = $this->SM->getUniversity($university_id)->row();
    $data   =   [
      'title'    => 'Rekap_Data_Mahasiswa_Beasiswa_BI_' . $getUniversity->name . '_' . date('m_Y'),
      'allData'  => $this->SM->getPendaftarByYear(['YEAR(a.created_at)' => $tahun, 'b.id' => $university_id, 'a.approval' => 'Terima'])->result(),
      'row'     => $this->SM->getPendaftarByYear(['YEAR(a.created_at)' => $tahun, 'b.id' => $university_id, 'a.approval' => 'Terima'])->row(),
      'university' => $getUniversity
    ];

    $this->load->view('backoffice/exports/rekap_data_mahasiswa', $data);
  }

  public function exportFormDataWawancara($university_id)
  {
    $tahun          = $this->input->get('tahun');
    $getUniversity  = $this->SM->getUniversity($university_id)->row();
    $data   =   [
      'title'    => 'Rekap_Data_Wawancara_Beasiswa_BI_' . $getUniversity->name . '_' . date('m_Y'),
      'allData'  => $this->SM->getPendaftarWithInterview(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id, 'b.approval' => 'Terima'])->result(),
      'row'     => $this->SM->getPendaftarWithInterview(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id, 'b.approval' => 'Terima'])->row(),
      'university' => $getUniversity
    ];

    $this->load->view('backoffice/exports/rekap_data_wawancara', $data);
  }

  public function exportStudentActivity($university_id)
  {
    $tahun          = $this->input->get('tahun');
    $getUniversity  = $this->SM->getUniversity($university_id)->row();
    $data   =   [
      'title'    => 'Rekap_Data_Kegiatan_Mahasiswa_Beasiswa_BI_' . $getUniversity->name . '_' . date('m_Y'),
      'allData'  => $this->Activity->getActivityWithUniversityDetail(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id])->result(),
      'row'     => $this->Activity->getActivityWithUniversityDetail(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id])->row(),
      'university' => $getUniversity
    ];

    $this->load->view('backoffice/exports/rekap_data_kegiatan', $data);
  }

  public function exportFormDataWawancaraFinal($university_id)
  {
    $tahun          = $this->input->get('tahun');
    $getUniversity  = $this->SM->getUniversity($university_id)->row();
    $data   =   [
      'title'    => 'Rekap_Data_Wawancara_Beasiswa_BI_' . $getUniversity->name . '_' . date('m_Y'),
      'allData'  => $this->SM->getPendaftarWithInterviewFinal(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id, 'b.approval' => 'Terima', 'a.recomendation' => 'Lulus'])->result(),
      'row'     => $this->SM->getPendaftarWithInterviewFinal(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id, 'b.approval' => 'Terima', 'a.recomendation' => 'Lulus'])->row(),
      'university' => $getUniversity
    ];

    $this->load->view('backoffice/exports/rekap_data_wawancara_akhir', $data);
  }

  public function exportEvaluationReport($university_id)
  {
    $tahun          = $this->input->get('tahun');
    $getUniversity  = $this->SM->getUniversity($university_id)->row();
    $data   =   [
      'title'    => 'Rekap_Evaluasi_Beasiswa_BI_' . $getUniversity->name . '_' . date('m_Y'),
      'allData'  => $this->SM->getEvaluationReport($tahun, $university_id)->result(),
      'row'     => $this->SM->getEvaluationReport()->row(),
      'university' => $getUniversity
    ];

    $this->load->view('backoffice/exports/rekap_hasil_evaluasi', $data);
  }

  public function exportIndividualInterview($applicantId)
  {
    $student = $this->SM->getPendaftarWithInterview(['YEAR(a.created_at)' => $tahun, 'c.id' => $university_id, 'b.approval' => 'Terima'], $applicantId)->row();
    $data   =   [
      'title'       => 'Tabel_Nilai_Wawancara_Beasiswa_BI_' . $student->name . '_' . date('m_Y'),
      'row'         => $student,
    ];

    $this->load->view('backoffice/exports/tabel_nilai_wawancara', $data);
  }
}

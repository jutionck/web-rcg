
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operator_dashboard extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Dashboard_model', 'Dashboard');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'operator';
    cek_login('Operator');
    $this->redirectUrl = 'backoffice/operator/setting';
  }

  public function index()
  {
    $data = [
      'title'       => 'Dashboard | beasiswabilampung.com',
      'sub_title'   => 'Dashboard',
      'information' => $this->Dashboard->getInformation($this->session->userdata('user'))->row(),
      'periode'     => $this->Setting->getPeriod('list')->result(),
    ];

    $page = '/backoffice/operator/dashboard';
    pageBackend($this->role, $page, $data);
  }
}

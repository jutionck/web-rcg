<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Send_mail extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Setting_model', 'ST');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/kirim_email';
  }

  public function index()
  {
    $this->_validation();
    if ($this->form_validation->run() == false) {
      $data = [
        'title'     => 'Kirim Email | beasiswabilampung.com',
        'sub_title' => 'Kirim Email',
        'desc'      => 'Di bawah ini adalah form untuk test kirim email website beasiswa bank indonesia',
      ];
      $page = '/backoffice/admin/send_mail/index';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $email = $data["email"];
      $acara = $data["acara"];
      $tanggal = indonesianDate(date('d M Y'), 'dd MMMM YY');
      $save = $this->sendMail($email, $acara, $tanggal);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Kirim email berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  private function sendMail($email, $acara, $tanggal)
  {
    $this->load->library('PHPMailer_load'); //Load Library PHPMailer
    $mail = $this->phpmailer_load->load(); // Mendefinisikan Variabel Mail
    $mail->isSMTP();  // Mengirim menggunakan protokol SMTP
    $mail->Host = 'smtp.gmail.com'; // Host dari server SMTP
    $mail->SMTPAuth = true; // Autentikasi SMTP
    $mail->Username = 'beasiswabilampung@gmail.com';
    $mail->Password = 'zxzqgutongorzdfb';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->setFrom('noreply@beasiswabilampung.com', 'Kegiatan Baru'); // Sumber email
    $mail->addAddress($email); // Masukkan alamat email dari variabel $email
    $mail->Subject = "Kegiatan Baru $acara"; // Subjek Email
    $mail->msgHtml("
            <h3>Kegiatan Baru $acara</h3><hr>

                Kegiatan baru yang harus diikuti telah ditambahkan dengan detail : <br><br>
                Nama Kegiatan : $acara<br>
                Tanggal : $tanggal<br><br>

                Silahkan cek detail dan laporkan kehadiran pada aplikasi<br>
                <a href='https://beasiswabilampung/backoffice' target='_blank'>https://beasiswabilampung/backoffice</a>
            "); // Isi email dengan format HTML


    if (!$mail->send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
      //echo "Message sent!";
    } // Kirim email dengan cek kondisi
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'email',
      'Email',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

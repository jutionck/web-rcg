
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_dashboard extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Dashboard_model', 'Dashboard');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->load->model('backoffice/Student_activity_model', 'Activity');
    $this->role = 'student';
    cek_login('Student');
    $this->redirectUrl = 'backoffice/student/setting';
  }

  public function index()
  {
    $getBy = $this->session->userdata('username');
    $activities = $this->Activity->getActivity("", $getBy->university_id, $getBy->username)->result();
    $activity_details = $this->Activity->getActivityDetail("", $getBy->username)->result();
    $data = [
      'title'       => 'Dashboard | beasiswabilampung.com',
      'sub_title'   => 'Dashboard',
      'information' => $this->Dashboard->getInformation($this->session->userdata('user'))->row(),
      'periode'     => $this->Setting->getPeriod('list')->result(),
      'activities'        => $activities,
      'activity_details'  => $activity_details,
    ];

    $page = '/backoffice/student/dashboard';
    pageBackend($this->role, $page, $data);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_activity extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Student_activity_model', 'Activity');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'student';
    cek_login('Student');
    $this->redirectUrl = 'backoffice/student/kegiatan';
  }

  public function index()
  {
    $getBy = $this->session->userdata('username');
    $activities = $this->Activity->getActivity("", $getBy->university_id, $getBy->username)->result();
    $activity_details = $this->Activity->getActivityDetail("", $getBy->username)->result();
    $data = [
      'title'             => 'Kegiatan | beasiswabilampung.com',
      'sub_title'         => 'Kegiatan Yang Wajib Diikuti:',
      'activities'        => $activities,
      'activity_details'  => $activity_details,
    ];
    $page = '/backoffice/student/activity/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation('A01');
    $applicantId = $this->Activity->getApplicantId($this->session->userdata('username'))->row();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'         => 'Kegiatan | beasiswabilampung.com',
        'sub_title'     => 'Tambah Kegiatan',
        'applicant_id'  => $applicantId,
        'redirect'      => $this->redirectUrl
      ];
      $page = '/backoffice/student/activity/other_form';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $activity_file       = $_FILES['activity_file'];
      if ($activity_file != '') {
        $config['upload_path'] = './assets/backoffice/upload/activity/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('activity_file')) {
          $activity_file = "";
        } else {
          $activity_file = $this->upload->data('file_name');
        }
      }
      $setValue = [
        'applicants_id'   => $data['applicants_id'],
        'activity_name'   => $data['activity_name'],
        'activity_date'   => $data['activity_date'],
        'attendance'      => $data['attendance'],
        'activity_type'   => $data['activity_type'],
        'activity_resume' => $data['activity_resume'],
        'activity_file'   => $activity_file,
      ];
      $save = $this->Activity->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Tambah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function present($id)
  {
    $this->_validation('A02');
    $getBy = $this->session->userdata('username');
    $applicantId = $this->Activity->getApplicantId($getBy)->row();
    $activity = $this->Activity->getActivity($id)->row();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'         => 'Kegiatan | beasiswabilampung.com',
        'sub_title'     => 'Menghadiri Kegiatan',
        'applicant_id'  => $applicantId,
        'activity'      => $activity,
        'redirect'      => $this->redirectUrl
      ];
      $page = '/backoffice/student/activity/present_form';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $activity_file       = $_FILES['activity_file'];
      if ($activity_file != '') {
        $config['upload_path'] = './assets/backoffice/upload/activity/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('activity_file')) {
          $activity_file = "";
        } else {
          $activity_file = $this->upload->data('file_name');
        }
      }
      $setValue = [
        'applicants_id'   => $data['applicants_id'],
        'activity_name'   => $data['activity_name'],
        'activity_date'   => $data['activity_date'],
        'attendance'      => $data['attendance'],
        'activity_type'   => $data['activity_type'],
        'activity_resume' => $data['activity_resume'],
        'activity_file'   => $activity_file,
      ];
      $save = $this->Activity->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Tambah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function notPresent($id)
  {
    $this->_validation();
    $getBy = $this->session->userdata('username');
    $applicantId = $this->Activity->getApplicantId($getBy)->row();
    $activity = $this->Activity->getActivity($id)->row();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'         => 'Kegiatan | beasiswabilampung.com',
        'sub_title'     => 'Tidak Menghadiri Kegiatan',
        'applicant_id'  => $applicantId,
        'activity'      => $activity,
        'redirect'      => $this->redirectUrl
      ];
      $page = '/backoffice/student/activity/not_present_form';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $setValue = [
        'applicants_id'   => $data['applicants_id'],
        'activity_name'   => $data['activity_name'],
        'activity_date'   => $data['activity_date'],
        'attendance'      => 'Tidak Hadir',
        'activity_type'   => $data['activity_type'],
        'activity_reason' => $data['activity_reason'],
      ];
      $save = $this->Activity->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Tambah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  private function _validation($type = null)
  {
    if ($type == "A01") {
      $this->form_validation->set_rules(
        'attendance',
        'Kehadiran',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_name',
        'Nama Kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_date',
        'Tanggal Pelaksanaan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_type',
        'Jenis Kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_resume',
        'Deskripsi kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
    } else if ($type == "A02") {
      $this->form_validation->set_rules(
        'attendance',
        'Kehadiran',
        'trim|required',
        [
          'required' => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_type',
        'Jenis Kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_resume',
        'Resume Kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
    } else {
      $this->form_validation->set_rules(
        'activity_type',
        'Jenis Kegiatan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );

      $this->form_validation->set_rules(
        'activity_reason',
        'Alasan',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
    }
  }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = $this->session->userdata('role');
    cek_login($this->session->userdata('role'));
    $this->redirectUrl = 'backoffice/pengaturan_akun';
    $this->redirectUrlPeriode = 'backoffice/periode_pendaftaran';
  }

  public function index()
  {
    $setting = $this->Setting->getInformation($this->session->userdata('user'), $this->session->userdata('role'))->row();
    $this->_validation();
    if ($this->form_validation->run() == false) {
      $data = [
        'title'       => 'Pengaturan Akun | beasiswabilampung.com',
        'sub_title'   => 'Pengaturan Akun',
        'user'        => $setting
      ];

      $page = '/backoffice/setting';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($setting));
      $data = $this->input->post();
      if ($data['password'] != '') {
        $setValue = [
          'id'              => $data['id'],
          'username'        => $data['username'],
          'password'        => password_hash($data['password'], PASSWORD_DEFAULT),
        ];
      } else {
        $setValue = [
          'id'              => $data['id'],
          'username'        => $data['username']
        ];
      }

      $save = $this->Setting->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Password berhasil dirubah</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function periode()
  {
    $periode = $this->Setting->getPeriod('list')->result();
    $data = [
      'title'     => 'Periode Pendaftaran | beasiswabilampung.com',
      'sub_title' => 'Periode Pendaftaran',
      'desc'      => 'Di bawah ini adalah data periode pendaftaran website beasiswa bank indonesia',
      'periode'     => $periode
    ];
    $page = '/backoffice/admin/periode/index';
    pageBackend($this->role, $page, $data);
  }

  public function addPeriode($id = null, $tag = null)
  {
    $periode = $this->Setting->getPeriod('list', $id, $tag)->row();
    $this->_validation('periode');
    if ($this->form_validation->run() == false) {
      $data = [
        'title'     => 'Tambah Periode Pendaftaran | beasiswabilampung.com',
        'sub_title' => 'Periode Pendaftaran',
        'desc'      => 'Di bawah ini adalah form tambah periode pendaftaran website beasiswa bank indonesia',
        'periode'   => $periode,
        'redirect'  => $this->redirectUrlPeriode
      ];
      $page = '/backoffice/admin/periode/form';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($periode));

      $data = $this->input->post();
      $setValue = [
        'id'           => $data['id'],
        'title'        => $data['title'],
        'start_time'   => $data['start_time'],
        'finish_time'  => $data['finish_time'],
      ];
      $save = $this->Setting->savePeriode($setValue);
      $this->Setting->savePeriodeHistory($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrlPeriode);
    }
  }

  private function _validation($type = null)
  {
    if ($type == 'periode') {
      $this->form_validation->set_rules(
        'title',
        'Judul',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
      $this->form_validation->set_rules(
        'start_time',
        'Tanggal awal',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
      $this->form_validation->set_rules(
        'finish_time',
        'Tanggal akhir',
        'trim|required',
        [
          'required'    => '%s wajib di isi',
        ]
      );
    } else {
      $this->form_validation->set_rules(
        'password',
        'Password',
        'trim|required|min_length[6]',
        [
          'required'    => '%s wajib di isi',
          'min_length'  => '%s wajib minimal 6 karakter'
        ]
      );

      $this->form_validation->set_rules(
        'passconf',
        'Password Confirmation',
        'trim|required|matches[password]',
        [
          'required' => '%s wajib di isi',
        ]
      );
    }
  }
}

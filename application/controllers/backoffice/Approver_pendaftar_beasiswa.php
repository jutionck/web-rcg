
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approver_pendaftar_beasiswa extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Approver_pendaftar_beasiswa_model', 'OP');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'approver';
    cek_login('Approver');
    $this->redirectUrl = 'backoffice/approver/pendaftar_beasiswa';
  }

  public function index()
  {
    $getUniversityId = $this->session->userdata('username');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['university_id' => $getUniversityId->university_id], $tahun)->result();
    } else {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['university_id' => $getUniversityId->university_id])->result();
    }
    $data = [
      'title'       => 'Pendaftar Beasiswa | beasiswabilampung.com',
      'sub_title'   => 'Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data pendaftar beasiswa bank indonesia',
      'pendaftar'   => $pendaftar,
      'redirect'    => $this->redirectUrl,
    ];
    $page = '/backoffice/approver/pendaftar/index';
    pageBackend($this->role, $page, $data);
  }

  public function detail($id)
  {
    $get = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $data = [
      'title'       => 'Detail Pendaftar | beasiswabilampung.com',
      'sub_title'   => 'Detail Pendaftar Beasiswa',
      'desc'        => 'Di bawah ini adalah data detail pendaftar beasiswa bank indonesia',
      'pendaftar'   => $get,
      'age'         => calculateAge($get->birth_date),
      'periode'     => $this->Setting->getPeriod('', '', 'registration')->row(),
      'redirect'    => $this->redirectUrl
    ];
    $page = '/backoffice/approver/pendaftar/detail';
    pageBackend($this->role, $page, $data);
  }

  public function verifikasi($stringUrl, $status)
  {
    $explode    = explode(":", $stringUrl);
    $id         = $explode[0];
    $uri        = $explode[1];
    $data      = $this->OP->getPendaftar(['id' => $id])->row();
    if ($data) {
      if ($status === '1') {
        $groupStatus    = 'Terima';
      } else {
        $groupStatus    = 'Tolak';
      }
      $dataUpdate = [
        'approval'    => $groupStatus,
        'updated_at'  => date('Y-m-d H:i:s')
      ];
      $where      = [
        'id'    => $id,
      ];
      $approval       = $this->OP->approval($dataUpdate, $where);
      if ($approval > 0) {
        $this->session->set_flashdata('success', 'Data berhasil disimpan');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_university extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/perguruan_tinggi';
  }

  public function index()
  {
    $data = [
      'title'     => 'Perguruan Tingggi | beasiswabilampung.com',
      'sub_title' => 'Perguruan Tingggi',
      'desc'      => 'Di bawah ini adalah data Perguruan Tingggi website beasiswa bank indonesia',
      'university'  => $this->University->getUniversity()->result()
    ];
    $page = '/backoffice/admin/university/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'Perguruan Tinggi | beasiswabilampung.com',
        'sub_title' => 'Tambah Perguruan Tinggi',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/university/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $university_logo       = $_FILES['university_logo'];

      if ($university_logo != '') {
        $config['upload_path'] = './assets/official/img/university/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('university_logo')) {
          $university_logo = "";
        } else {
          $university_logo = $this->upload->data('file_name');
        }
      }

      $setValue = [
        'name'            => $data['name'],
        'university_logo' => $university_logo,
        'address'         => $data['address'],
      ];

      $save = $this->University->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->University->getUniversity(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Perguruan Tinggi | beasiswabilampung.com',
        'sub_title'   => 'Edit Perguruan Tinggi',
        'university'  => $getId,
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/admin/university/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();
      $university_logo       = $_FILES['university_logo'];

      if ($university_logo != '') {
        $config['upload_path'] = './assets/official/img/university/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('university_logo')) {
          $university_logo = "";
        } else {
          $university_logo = $this->upload->data('file_name');
        }
      }

      if ($university_logo == '') {
        $setValue = [
          'id'              => $data['id'],
          'name'            => $data['name'],
          'university_logo' => $data['university_logo_old'],
          'address'         => $data['address'],
        ];
      } else {
        $setValue = [
          'id'              => $data['id'],
          'name'            => $data['name'],
          'university_logo' => $university_logo,
          'address'         => $data['address'],
        ];
      }
      $save = $this->University->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->University->getUniversity(['id' => $id])->row();
    if ($data) {
      $delete    = $this->University->delete(['id' => $id]);
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'name',
      'Nama kampus',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

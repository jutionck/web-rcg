
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_interview extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_interview_model', 'OP');
    $this->load->model('backoffice/Admin_university_model', 'University');
    $this->load->model('backoffice/Admin_users_model', 'User');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/wawancara/penilaian/';
    $this->redirectUrlApproval = 'backoffice/admin/wawancara';
  }

  public function index()
  {
    $university      = $this->input->get('university');
    $tahun           = $this->input->get('tahun');
    if ($tahun) {
      $pendaftar       = $this->OP->getPendaftarWithUniversity(['YEAR(a.created_at)' => $tahun, 'a.university_id' => $university, 'a.approval' => 'Terima'])->result();
    } else {
      $pendaftar       = $this->OP->getPendaftarWithUniversity()->result();
    }

    $data = [
      'title'       => 'Pendaftar Wawancara | beasiswabilampung.com',
      'sub_title'   => 'Pendaftar Wawancara',
      'desc'        => 'Di bawah ini adalah data pendaftar wawancara beasiswa bank indonesia',
      'pendaftar'   => $pendaftar,
      'university'  => $this->University->getUniversity()->result()
    ];
    $page = '/backoffice/admin/interview/index';
    pageBackend($this->role, $page, $data);
  }

  public function penilaian($id)
  {
    $get        = $this->OP->getPendaftarFull(['a.id' => $id])->row();
    $interview  = $this->OP->getDataInterview($id)->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'       => 'Detail Pendaftar Wawancara | beasiswabilampung.com',
        'sub_title'   => 'Detail Pendaftar Wawancara',
        'desc'        => 'Di bawah ini adalah data detail pendaftar wawancara beasiswa bank indonesia',
        'pendaftar'   => $get,
        'interview'   => $interview,
        'score'       => $this->OP->getScoreValue(),
        'age'         => calculateAge($get->birth_date),
        'redirect'    => $this->redirectUrl
      ];
      $page = '/backoffice/admin/interview/penilaian';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();
      $values = [
        $data['gpa_score'],
        $data['administration_score'],
        $data['bank_central_score'],
        $data['genbi_score'],
        $data['organization_score'],
        $data['scientific_work_score'],
        $data['motivation_score'],
        $data['sktm_score'],
      ];
      $resultScore = array_sum($values) / count($values);

      if (@$interview->id) {
        $setValue = [
          'id'                    => $data['id'],
          'applicants_id'         => $data['applicants_id'],
          'gpa_score'             => $data['gpa_score'],
          'gpa_note'              => $data['gpa_note'],
          'administration_score'  => $data['administration_score'],
          'administration_note'   => $data['administration_note'],
          'bank_central_score'    => $data['bank_central_score'],
          'bank_central_note'     => $data['bank_central_note'],
          'genbi_score'           => $data['genbi_score'],
          'genbi_note'            => $data['genbi_note'],
          'organization_score'    => $data['organization_score'],
          'organization_note'     => $data['organization_note'],
          'scientific_work_score' => $data['scientific_work_score'],
          'scientific_work_note'  => $data['scientific_work_note'],
          'motivation_score'      => $data['motivation_score'],
          'motivation_note'       => $data['motivation_note'],
          'sktm_score'            => $data['sktm_score'],
          'sktm_note'             => $data['sktm_note'],
          'result_score'          => number_format($resultScore, 2),
          'note'                  => $data['note'],
        ];
      } else {
        $setValue = [
          'applicants_id'         => $data['applicants_id'],
          'gpa_score'             => $data['gpa_score'],
          'gpa_note'              => $data['gpa_note'],
          'administration_score'  => $data['administration_score'],
          'administration_note'   => $data['administration_note'],
          'bank_central_score'    => $data['bank_central_score'],
          'bank_central_note'     => $data['bank_central_note'],
          'genbi_score'           => $data['genbi_score'],
          'genbi_note'            => $data['genbi_note'],
          'organization_score'    => $data['organization_score'],
          'organization_note'     => $data['organization_note'],
          'scientific_work_score' => $data['scientific_work_score'],
          'scientific_work_note'  => $data['scientific_work_note'],
          'motivation_score'      => $data['motivation_score'],
          'motivation_note'       => $data['motivation_note'],
          'sktm_score'            => $data['sktm_score'],
          'sktm_note'             => $data['sktm_note'],
          'result_score'          => number_format($resultScore, 2),
          'note'                  => $data['note'],
        ];
      }
      $save = $this->OP->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl . $id);
    }
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'gpa_note',
      'Keterangan',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }

  public function verifikasi($stringUrl, $status)
  {
    $explode    = explode(":", $stringUrl);
    $id         = $explode[0];
    $uri        = $explode[1];
    $data      = $this->OP->getPendaftar(['id' => $id])->row();
    if ($data) {
      if ($status === '1') {
        $groupStatus    = 'Lulus';
        $setValue = [
          'username'      => strtolower($data->email),
          'password'      => password_hash("beasiswa@bi", PASSWORD_DEFAULT),
          'role_id'       => "8308bdde-dce8-11eb-9096-0cc47abcfaa9", //Student role
          'university_id' => $data->university_id,
        ];
        $save = $this->User->save($setValue);
        if ($save > 0) {
          $this->session->set_flashdata('success', '<b>Tambah user berhasil</b>');
        } else {
          $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
          return;
        }
      } else {
        $groupStatus    = 'Tidak Lulus';
      }
      $dataUpdate = [
        'recomendation'    => $groupStatus,
        'updated_at'  => date('Y-m-d H:i:s')
      ];
      $where      = [
        'applicants_id'    => $id,
      ];
      $approval       = $this->OP->approval($dataUpdate, $where);
      if ($approval > 0) {
        $this->session->set_flashdata('success', 'Data berhasil disimpan');
      } else {
        $this->session->set_flashdata('error', 'Server data sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrlApproval);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_abouts extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_master_model', 'AMM');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/tentang';
  }

  public function index()
  {
    $about = $this->AMM->getAbouts()->row();
    $this->_validation();
    if ($this->form_validation->run() == false) {
      $data = [
        'title'     => 'Tentang | beasiswabilampung.com',
        'sub_title' => 'Tentang',
        'desc'      => 'Di bawah ini adalah data tentang website beasiswa bank indonesia',
        'about'     => $about
      ];
      $page = '/backoffice/admin/about/index';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($about));
      $data = $this->input->post();
      $desc_image       = $_FILES['desc_image'];
      $sub_title_image  = $_FILES['sub_title_image'];

      if ($desc_image != '') {
        $config['upload_path'] = './assets/official/img/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('desc_image')) {
          $desc_image = "";
        } else {
          $desc_image = $this->upload->data('file_name');
        }
      }

      if ($sub_title_image != '') {
        $config['upload_path'] = './assets/official/img/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('sub_title_image')) {
          $sub_title_image = "";
        } else {
          $sub_title_image = $this->upload->data('file_name');
        }
      }

      if ($sub_title_image == '') {
        $setValue = [
          'id'              => $data['id'],
          'title'           => $data['title'],
          'sub_title'       => $data['sub_title'],
          'description'     => $data['description'],
          'desc_image'      => $data['desc_image_old'],
          'sub_title_image' => $data['sub_title_image_old'],
        ];
      } else {
        $setValue = [
          'id'              => $data['id'],
          'title'           => $data['title'],
          'sub_title'       => $data['sub_title'],
          'description'     => $data['description'],
          'desc_image'      => $desc_image,
          'sub_title_image' => $sub_title_image
        ];
      }
      $save = $this->AMM->save($setValue, 'about');
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Judul website',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

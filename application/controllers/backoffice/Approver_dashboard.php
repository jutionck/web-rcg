
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approver_dashboard extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Dashboard_model', 'Dashboard');
    $this->load->model('backoffice/Setting_model', 'Setting');
    $this->role = 'approver';
    cek_login('Approver');
    $this->redirectUrl = 'backoffice/approver/setting';
  }

  public function index()
  {
    $data = [
      'title'       => 'Dashboard | beasiswabilampung.com',
      'sub_title'   => 'Dashboard',
      'information' => $this->Dashboard->getInformation($this->session->userdata('user'))->row(),
      'periode'     => $this->Setting->getPeriod('list')->result(),
    ];

    $page = '/backoffice/approver/dashboard';
    pageBackend($this->role, $page, $data);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_faqs extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('backoffice/Admin_faqs_model', 'Faqs');
    $this->role = 'admin';
    cek_login('Admin');
    $this->redirectUrl = 'backoffice/admin/master/faqs';
  }

  public function index()
  {
    $data = [
      'title'     => 'FAQs | beasiswabilampung.com',
      'sub_title' => 'FAQs',
      'desc'      => 'Di bawah ini adalah data faqs website beasiswa bank indonesia',
      'faqs'      => $this->Faqs->getFaqs()->result()
    ];
    $page = '/backoffice/admin/faqs/index';
    pageBackend($this->role, $page, $data);
  }

  public function add()
  {
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'FAQs | beasiswabilampung.com',
        'sub_title' => 'Tambah FAQs',
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/faqs/add';
      pageBackend($this->role, $page, $data);
    } else {
      $data = $this->input->post();

      $setValue = [
        'title'       => $data['title'],
        'description' => $data['description'],
        'position'    => $data['position'],
      ];

      $save = $this->Faqs->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function update($id)
  {
    $getId      = $this->Faqs->getFaqs(['id' => $id])->row();
    $this->_validation();
    if ($this->form_validation->run() === false) {
      $data = [
        'title'     => 'FAQs | beasiswabilampung.com',
        'sub_title' => 'Edit FAQs',
        'faq'       => $getId,
        'redirect'  => $this->redirectUrl
      ];
      $page = '/backoffice/admin/faqs/edit';
      pageBackend($this->role, $page, $data);
    } else {
      $this->output->set_content_type('application/json')->set_output(json_encode($getId));
      $data = $this->input->post();

      $setValue = [
        'id'          => $data['id'],
        'title'       => $data['title'],
        'description' => $data['description'],
        'position'    => $data['position'],
      ];
      $save = $this->Faqs->save($setValue);
      if ($save > 0) {
        $this->session->set_flashdata('success', '<b>Ubah data berhasil</b>');
      } else {
        $this->session->set_flashdata('error', '<b>Server sedang sibuk, silahkan coba lagi</b>');
      }
      redirect($this->redirectUrl);
    }
  }

  public function delete($id)
  {
    $data      = $this->Faqs->getFaqs(['id' => $id])->row();
    if ($data) {
      $delete    = $this->Faqs->delete(['id' => $id], 'benefit');
      if ($delete > 0) {
        $this->session->set_flashdata('success', 'Data berhasil di hapus');
      } else {
        $this->session->set_flashdata('error', 'Server data jurusan sedang sibuk, silahkan coba lagi');
      }
    } else {
      $this->session->set_flashdata('error', 'Data yang anda masukan tidak ada');
    }
    redirect($this->redirectUrl);
  }

  private function _validation()
  {
    $this->form_validation->set_rules(
      'title',
      'Judul',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'description',
      'Deskripsi',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );

    $this->form_validation->set_rules(
      'position',
      'Posisi faqs',
      'trim|required',
      [
        'required' => '%s wajib di isi',
      ]
    );
  }
}

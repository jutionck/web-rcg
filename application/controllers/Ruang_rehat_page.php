
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ruang_rehat_page extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data = [
      'title'               => 'RCG Group Indonesia',
    ];

    $page = '/official/member/ruang_rehat';
    pageOfficial($page, $data);
  }

  public function detail()
  {
    $data = [
      'title'               => 'RCG Group Indonesia',
    ];

    $page = '/official/member/ruang_rehat_detail';
    pageOfficial($page, $data);
  }
}

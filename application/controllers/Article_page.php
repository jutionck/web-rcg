
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article_page extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('official/Landing_page_model', 'LP');
  }

  public function index()
  {
    $this->load->library('pagination');
    $offset = $this->uri->segment(1, 0);
    $totalPerPage = 5;
    $result = $this->LP->GetArticlesWithFilter('', $offset, $totalPerPage, 'page');
    $config['uri_segment'] = 1;
    $config['base_url'] = site_url('artikel');
    $config['per_page'] = $totalPerPage;
    $config['total_rows'] = $result['total_rows'];
    $this->pagination->initialize($config);
    $data = [
      'title'     => 'Artikel | beasiswabilampung.com',
      'about'               => $this->LP->GetAbout()->row(),
      'benefits'            => $this->LP->GetBenefit()->result(),
      'contact'             => $this->LP->GetContact()->row(),
      'faqs_left'           => $this->LP->GetFaqs(0)->result(),
      'faqs_right'          => $this->LP->GetFaqs(1)->result(),
      'scholarship_stages'  => $this->LP->GetScholarshipStages()->result(),
      'university'          => $this->LP->GetUniversity()->result(),
      'articles'            => $result['data'],
      'pagination'          => $this->pagination->create_links(),
    ];

    $page = '/official/article/index';
    pageOfficial($page, $data);
  }

  public function detail($slug)
  {
    $article = $this->LP->GetArticleWithSlug($slug)->row();
    $data = [
      'title'   => 'Baca Artikel | beasiswabilampung.com',
      'about'               => $this->LP->GetAbout()->row(),
      'benefits'            => $this->LP->GetBenefit()->result(),
      'contact'             => $this->LP->GetContact()->row(),
      'faqs_left'           => $this->LP->GetFaqs(0)->result(),
      'faqs_right'          => $this->LP->GetFaqs(1)->result(),
      'scholarship_stages'  => $this->LP->GetScholarshipStages()->result(),
      'university'          => $this->LP->GetUniversity()->result(),
      'article'             => $article,
      'articles'            => $this->LP->GetArticles(5)->result(),
    ];

    // update hits
    if ($article) {
      $this->LP->SetArticleHits($article->id);
    }

    $page = '/official/article/detail';
    pageOfficial($page, $data);
  }

  public function search()
  {
    $filter = $this->input->get('judul');
    $result = $this->LP->GetArticlesWithFilter($filter, 0, 5);
    $recent = $this->LP->GetArticlesWithFilter('', 0, 5);
    $data = [
      'title'     => 'Artikel | beasiswabilampung.com',
      'about'               => $this->LP->GetAbout()->row(),
      'benefits'            => $this->LP->GetBenefit()->result(),
      'contact'             => $this->LP->GetContact()->row(),
      'faqs_left'           => $this->LP->GetFaqs(0)->result(),
      'faqs_right'          => $this->LP->GetFaqs(1)->result(),
      'scholarship_stages'  => $this->LP->GetScholarshipStages()->result(),
      'university'          => $this->LP->GetUniversity()->result(),
      'articles'            => $result['data'],
      'recents'             => $recent['data']->result(),
    ];

    $page = '/official/article/search';
    pageOfficial($page, $data);
  }
}


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Roadmap_page extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    // $this->load->model('official/Home_page_model', 'HP');
  }

  public function index()
  {
    $data = [
      'title'               => 'RCG Group Indonesia',
      // 'about'               => $this->LP->GetAbout()->row(),
      // 'benefits'            => $this->LP->GetBenefit()->result(),
      // 'contact'             => $this->LP->GetContact()->row(),
      // 'faqs_left'           => $this->LP->GetFaqs(0)->result(),
      // 'faqs_right'          => $this->LP->GetFaqs(1)->result(),
      // 'scholarship_stages'  => $this->LP->GetScholarshipStages()->result(),
      // 'university'          => $this->LP->GetUniversity()->result(),
      // 'articles'            => $this->LP->GetArticles(3),
    ];

    $page = '/official/roadmap';
    pageOfficial($page, $data);
  }
}

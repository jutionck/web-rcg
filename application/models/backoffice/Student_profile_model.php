<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_profile_model extends CI_Model
{

  public function GetProfile($where = null)
  {
    $this->db->select('a.*, b.name as university');
    $this->_join();
    return $this->db->get_where('tr_scholarship_applicants a', ['a.email' => $where->username]);
  }

  private function _join()
  {
    $this->db->join('m_university b', 'b.id=a.university_id');
  }

  public function save($data)
  {
    $this->db->set('updated_at', date('Y-m-d H:i:s'));
    $this->db->update('tr_scholarship_applicants', $data, ['id' => $data['id']]);
    return $this->db->affected_rows();
  }
}

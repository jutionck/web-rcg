<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_activity_model extends CI_Model
{

  public function getActivity($id = null, $where = null, $year = null)
  {
    if ($id) {
      return $this->db->get_where('m_activity', $id);
    } else {
      if ($this->session->userdata('role') == 'Approver') {
        if ($year) {
          $this->db->where('YEAR(m_activity.created_at)', $year);
        } else {
          $this->db->where('YEAR(m_activity.created_at)', date('Y'));
        }
        $this->db->where('university_id', null);
        $this->db->or_where('university_id', $where);
      }
      $this->db->select('m_activity.*, m_university.name as university');
      $this->db->join('m_university', 'm_university.id=m_activity.university_id', 'LEFT');
      return $this->db->get('m_activity');
    }
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_activity', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_activity', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_activity', $id);
    return $this->db->affected_rows();
  }

  public function getActivityWithUniversity($where = null)
  {
    if ($where != null) {
      $this->_join();
      $this->db->group_by('a.applicants_id');
      return $this->db->get_where('tr_student_activities a', $where);
    } else {
      $this->_join();
      $this->db->order_by('a.created_at', 'desc');
      $this->db->group_by('a.applicants_id');
      return $this->db->get_where('tr_student_activities a', ['YEAR(a.activity_date)' => date('Y')]);
    }
  }

  public function getActivityWithUniversityDetail($where = null)
  {
    $this->_join();
    $this->db->order_by('a.created_at', 'desc');
    return $this->db->get_where('tr_student_activities a', $where);
  }

  private function _join()
  {
    $this->db->select('a.*, b.name, b.university_id, c.name as university');
    $this->db->join('tr_scholarship_applicants b', 'b.id=a.applicants_id');
    $this->db->join('m_university c', 'c.id=b.university_id');
  }
}

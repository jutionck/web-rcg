<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_interview_model extends CI_Model
{
  public function getPendaftar($id = null)
  {
    if ($id) {
      return $this->db->get_where('tr_scholarship_applicants', $id);
    } else {
      return $this->db->get('tr_scholarship_applicants');
    }
  }

  public function getPendaftarWithUniversity($where = null)
  {
    if ($where) {
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, b.name as university, c.status as interview_status, c.recomendation, a.no_hp');
      $this->db->join('m_university b', 'b.id=a.university_id');
      $this->db->join('tr_interview_result c', 'c.applicants_id=a.id', 'LEFT');
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, b.name as university, c.recomendation, a.no_hp',);
      $this->db->join('m_university b', 'b.id=a.university_id');
      $this->db->join('tr_interview_result c', 'c.applicants_id=a.id', 'LEFT');
      return $this->db->get_where('tr_scholarship_applicants a', ['a.approval' => 'Terima', 'YEAR(a.created_at)' => date('Y')]);
    }
  }

  public function getPendaftarFull($where = null)
  {
    $this->db->select('a.*, b.name as university, c.username');
    $this->_join();
    if ($where) {
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      return $this->db->get('tr_scholarship_applicants a');
    }
  }

  private function _join()
  {
    $this->db->join('m_university b', 'b.id=a.university_id');
    $this->db->join('m_users c', 'c.university_id=b.id');
  }

  public function getScoreValue()
  {
    return $this->db->get('ref_score')->result();
  }

  public function getDataInterview($id)
  {
    return $this->db->get_where('tr_interview_result', ['applicants_id' => $id]);
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('tr_interview_result', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('tr_interview_result', $data);
    }
    return $this->db->affected_rows();
  }

  public function approval($data, $where)
  {
    $this->db->update('tr_interview_result', $data, $where);
    return $this->db->affected_rows();
  }

  // new feature
  public function getPendaftarWithUniversityFinal($where = null, $university_id = null)
  {
    if ($where) {
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, a.no_hp, b.name as university, c.recomendation, d.evaluation_result');
      $this->db->join('m_university b', 'b.id=a.university_id');
      $this->db->join('tr_interview_result c', 'c.applicants_id=a.id');
      $this->db->join('tr_evaluation d', 'd.applicants_id=a.id', 'LEFT');
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      if ($this->session->userdata('role') == "Approver") {
        $this->db->where('b.id', $university_id);
      }
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, a.no_hp, b.name as university, c.recomendation, d.evaluation_result');
      $this->db->join('m_university b', 'b.id=a.university_id');
      $this->db->join('tr_interview_result c', 'c.applicants_id=a.id');
      $this->db->join('tr_evaluation d', 'd.applicants_id=a.id', 'LEFT');
      return $this->db->get_where('tr_scholarship_applicants a', ['c.recomendation' => 'Lulus', 'YEAR(a.created_at)' => date('Y')]);
    }
  }

  public function getDataEvaluation($id)
  {
    return $this->db->get_where('tr_evaluation', ['applicants_id' => $id]);
  }

  public function getOptionValue()
  {
    return $this->db->get('ref_option')->result();
  }

  public function saveEvaluation($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('tr_evaluation', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('tr_evaluation', $data);
    }
    return $this->db->affected_rows();
  }

  // Evaluation report
  public function getEvaluationReport($year = null, $university = null, $approver = null)
  {
    $now = date('Y');
    $query = "SELECT te.id,te.applicants_id, tsa.name,tsa.university_id, mu.name as university,YEAR(te.created_at) as year,te.gpa_evaluation,te.paid_leave,te.receive_another_scholarship,te.description,te.transcript_file,te.evaluation_result,te.created_at FROM tr_evaluation te join tr_scholarship_applicants tsa on tsa.id = te.applicants_id join m_university mu on mu.id = tsa.university_id WHERE YEAR(te.created_at) = $now";
    if ($year && $university) {
      $query = "SELECT te.id,te.applicants_id, tsa.name,tsa.university_id, mu.name as university,YEAR(te.created_at) as year,te.gpa_evaluation,te.paid_leave,te.receive_another_scholarship,te.description,te.transcript_file,te.evaluation_result,te.created_at FROM tr_evaluation te join tr_scholarship_applicants tsa on tsa.id = te.applicants_id join m_university mu on mu.id = tsa.university_id WHERE YEAR(te.created_at) = $year and mu.id = '$university'";
    }

    if ($approver) {
      $query = "SELECT te.id,te.applicants_id, tsa.name,tsa.university_id, mu.name as university,YEAR(te.created_at) as year,te.gpa_evaluation,te.paid_leave,te.receive_another_scholarship,te.description,te.transcript_file,te.evaluation_result,te.created_at FROM tr_evaluation te join tr_scholarship_applicants tsa on tsa.id = te.applicants_id join m_university mu on mu.id = tsa.university_id WHERE mu.id = '$university' and YEAR(te.created_at) = $now";
    }

    return $this->db->query($query);
  }
}

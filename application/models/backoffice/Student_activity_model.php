<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_activity_model extends CI_Model
{

  public function getActivity($id = null, $where = null, $user = null)
  {
    if ($id) {
      return $this->db->get_where('m_activity', ['id' => $id]);
    } else {
      $query = "SELECT * FROM m_activity WHERE title NOT IN (SELECT activity_name FROM tr_student_activities JOIN tr_scholarship_applicants on tr_scholarship_applicants.id=tr_student_activities.applicants_id WHERE tr_scholarship_applicants.email = '" . $user . "' AND (m_activity.university_id='" . $where . "' OR m_activity.university_id IS NULL))";
      return $this->db->query($query);
    }
  }

  public function getApplicantId($where)
  {
    $this->db->select('id, email');
    return $this->db->get_where('tr_scholarship_applicants', ['email' => $where->username]);
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_activity', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('tr_student_activities', $data);
    }
    return $this->db->affected_rows();
  }

  public function getActivityDetail($id = null, $where = null)
  {
    if ($id) {
      $this->_join();
      $this->db->where('b.email', $where->email);
      return $this->db->get_where('tr_student_activities', $id);
    } else {
      $this->_join();
      return $this->db->get_where('tr_student_activities a', ['b.email' => $where]);
    }
  }

  private function _join()
  {
    $this->db->select('a.*, b.email');
    $this->db->join('tr_scholarship_applicants b', 'b.id=a.applicants_id');
  }
}

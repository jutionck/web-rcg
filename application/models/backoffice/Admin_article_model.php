<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_article_model extends CI_Model
{

  public function getArticles($id = null)
  {
    $this->db->select('id,title,slug,description,author,image,is_status,created_at');
    if ($id) {
      $this->db->where($id);
    }
    return $this->db->get('m_news');
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->update('m_news', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_news', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_news', $id);
    return $this->db->affected_rows();
  }
}

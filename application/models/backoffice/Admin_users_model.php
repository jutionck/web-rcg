<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_users_model extends CI_Model
{

  public function getUsers($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_users', $id);
    } else {
      return $this->db->get('m_users');
    }
  }

  public function getUserWithRoleUniveristy($role = null)
  {
    $this->db->select('a.id, a.username, a.email, a.last_login, b.name as role_name, c.name as university');
    $this->db->join('ref_role b', 'b.id=a.role_id');

    if ($role == 'Student') {
      $this->db->where('b.name',  $role);
    } else if ($role == 'Approver') {
      $this->db->where('b.name',  $role);
    } else if ($role == 'Operator') {
      $this->db->where('b.name',  $role);
    } 
    
    $this->db->join('m_university c', 'c.id=a.university_id');
    $this->db->order_by('c.name', 'ASC');
    return $this->db->get('m_users a');
  }

  public function getUserWithRoleInterviewer()
  {
    $this->db->select('a.id, a.username, a.email, a.last_login, b.name as role_name');
    $this->db->join('ref_role b', 'b.id=a.role_id');
    $this->db->where('b.name',  'Interviewer');
    return $this->db->get('m_users a');
  }

  public function getRoles()
  {
    return $this->db->get('ref_role');
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_users', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_users', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_users', $id);
    return $this->db->affected_rows();
  }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_pendaftar_beasiswa_model extends CI_Model
{
  public function getPendaftar($id = null)
  {
    if ($id) {
      return $this->db->get_where('tr_scholarship_applicants', $id);
    } else {
      return $this->db->get('tr_scholarship_applicants');
    }
  }

  public function getPendaftarWithUniversity($where = null)
  {
    if ($where != null) {
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, b.name as university, a.no_hp, a.created_at');
      $this->db->join('m_university b', 'b.id=a.university_id');
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, b.name as university, a.no_hp, a.created_at');
      $this->db->join('m_university b', 'b.id=a.university_id');
      $this->db->order_by('a.created_at', 'desc');
      return $this->db->get_where('tr_scholarship_applicants a', ['YEAR(a.created_at)' => date('Y')]);
    }
  }

  public function getPendaftarFull($where = null)
  {
    $this->db->select('a.*, b.name as university, c.username');
    $this->_join();
    if ($where) {
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      return $this->db->get('tr_scholarship_applicants a');
    }
  }

  private function _join()
  {
    $this->db->join('m_university b', 'b.id=a.university_id');
    $this->db->join('m_users c', 'c.university_id=b.id');
  }

  public function approval($data, $where)
  {
    $this->db->update('tr_scholarship_applicants', $data, $where);
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('tr_scholarship_applicants', $id);
    return $this->db->affected_rows();
  }

  public function deleteRefInterview($id)
  {
    $this->db->delete('tr_interview_result', $id);
    return $this->db->affected_rows();
  }
}

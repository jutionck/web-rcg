<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authentication_model extends CI_Model
{
  public function userCheck($username)
  {
    $this->db->select('a.*,b.id role_id,b.name');
    $this->db->join('ref_role as b', 'a.role_id=b.id');
    $query = $this->db->get_where('m_users as a', ['a.username' => $username, 'a.is_active' => 1]);
    return $query;
  }

  function cek_login()
  {
    if (empty($this->session->userdata('is_login'))) {
      redirect('/backoffice');
    }
  }

  public function logout($date, $username)
  {
    $this->db->where('username', $username);
    $this->db->update('m_users', $date);
  }
}

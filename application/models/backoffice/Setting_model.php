<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_model extends CI_Model
{
  public function getInformation($where, $type = null)
  {
    if ($type == 'Admin') {
      return $this->db->get_where('m_users', ['username' => $where]);
    } else {
      $this->db->select('m_users.*, m_university.name as university ');
      $this->db->join('m_university', 'm_university.id = m_users.university_id');
      return $this->db->get_where('m_users', ['username' => $where]);
    }
  }

  public function save($data)
  {
    $this->db->set('updated_at', date('Y-m-d H:i:s'));
    $this->db->update('m_users', $data, ['id' => $data['id']]);
    return $this->db->affected_rows();
  }

  public function savePeriode($data)
  {
    $this->db->set('updated_at', date('Y-m-d H:i:s'));
    $this->db->update('m_periode', $data, ['id' => $data['id']]);
    return $this->db->affected_rows();
  }

  public function savePeriodeHistory($data)
  {
    $this->db->set('id', 'UUID()', FALSE);
    $registrationTag = str_contains($data['title'], 'Pendaftaran');
    $evaluationTag = str_contains($data['title'], 'Evaluasi');
    if ($registrationTag) {
      $tag = 'registration';
    } else {
      $tag = 'evaluation';
    }
    $newData = [
      'title'       => $data['title'],
      'start_time'  => $data['start_time'],
      'finish_time' => $data['finish_time'],
      'tag'         => $tag,
    ];
    $this->db->insert('m_periode_history', $newData);
    return $this->db->affected_rows();
  }

  public function getPendaftarByYear($where = null)
  {
    $this->db->select('a.*, b.name as university, c.username');
    $this->_join();
    $this->db->group_by('a.id');
    return $this->db->get_where('tr_scholarship_applicants a', $where);
  }


  private function _join()
  {
    $this->db->join('m_university b', 'b.id=a.university_id');
    $this->db->join('m_users c', 'c.university_id=b.id');
  }

  public function getUniversity($where)
  {
    return $this->db->get_where('m_university', ['id' => $where]);
  }

  function getPendaftarWithInterview($where = null, $applicantId = null)
  {
    $this->db->select('
    a.id as interview_id, 
    a.gpa_score, 
    a.gpa_note, 
    a.administration_score, 
    a.administration_note, 
    a.bank_central_score, 
    a.bank_central_note, 
    a.genbi_score, 
    a.genbi_note, 
    a.organization_score, 
    a.organization_note, 
    a.scientific_work_score, 
    a.scientific_work_note, 
    a.motivation_score, 
    a.motivation_note, 
    a.result_score, 
    a.recomendation,
    a.note,
    a.created_at as interview_date, 
    b.name,
    b.npm,
    b.faculty,
    b.major,
    b.birth_date,
    b.gpa, 
    b.semester, 
    b.number_of_credits as sks,
    b.sktm_option as sktm,
    c.id as university_id,
    c.name as university');
    $this->db->join('tr_scholarship_applicants b', 'b.id = a.applicants_id', 'right');
    $this->db->join('m_university c', 'c.id = b.university_id', 'right');
    if ($applicantId) {
      $this->db->where('b.id', $applicantId);
    }
    $this->db->order_by('a.result_score', 'desc');
    return $this->db->get_where('tr_interview_result a', $where);
  }

  function getPendaftarWithInterviewFinal($where = null)
  {
    $this->db->select('
    a.id as interview_id, 
    a.gpa_score, 
    a.gpa_note, 
    a.administration_score, 
    a.administration_note, 
    a.bank_central_score, 
    a.bank_central_note, 
    a.genbi_score, 
    a.genbi_note, 
    a.organization_score, 
    a.organization_note, 
    a.scientific_work_score, 
    a.scientific_work_note, 
    a.motivation_score, 
    a.motivation_note, 
    a.result_score, 
    a.recomendation,
    a.note,
    a.created_at as interview_date, 
    b.name, 
    b.birth_date,
    b.gpa, 
    b.semester, 
    b.number_of_credits as sks,
    b.sktm_option as sktm,
    c.id as university_id,
    c.name as university');
    $this->db->join('tr_scholarship_applicants b', 'b.id = a.applicants_id', 'right');
    $this->db->join('m_university c', 'c.id = b.university_id', 'right');
    $this->db->order_by('a.result_score', 'desc');
    return $this->db->get_where('tr_interview_result a', $where);
  }

  public function getPeriod($type = null, $id = null, $tag = null)
  {
    if ($type == 'list') {
      if ($tag == 'registration') {
        return $this->db->get_where('m_periode', ['id' => $id, 'tag' => $tag]);
      } else if ($tag == 'evaluation') {
        return $this->db->get_where('m_periode', ['id' => $id, 'tag' => $tag]);
      } else {
        return $this->db->get('m_periode');
      }
    } else {
      $today  = date('Y-m-d');
      $this->db->where('start_time <=', $today);
      $this->db->where('finish_time >=', $today);
      return $this->db->get_where('m_periode', ['tag' => $tag]);
    }
  }

  public function getBerkas($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_interview_file', $id);
    } else {
      return $this->db->get('m_interview_file');
    }
  }

  public function saveBerkas($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_interview_file', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_interview_file', $data);
    }
    return $this->db->affected_rows();
  }

  public function deleteBerkas($id)
  {
    $this->db->delete('m_interview_file', $id);
    return $this->db->affected_rows();
  }

  public function emailConfig()
  {
    return $this->db->get('m_config')->row();
  }

  // sementara tidak digunakan
  public function getStudentMail($university = null)
  {
    if ($university) {
      $this->db->where($university);
    }
    $this->db->select('a.id, a.email, b.recomendation, a.university_id');
    $this->db->join('tr_interview_result b', 'b.applicants_id=a.id');
    $this->db->where('b.recomendation', 'Lulus');
    return $this->db->get('tr_scholarship_applicants a');
  }

  public function getAdminUnivMail($role = null)
  {
    $this->db->select('id,email');
    if ($role === 'univ') {
      $this->db->where('role_id', '775b0ff8-b7a8-11eb-a91e-0cc47abcfaa6');
    }
    return $this->db->get('m_users');
  }

  public function getEvaluationReport($year = null, $university = null)
  {
    $query = "SELECT te.id,te.applicants_id, tsa.name,tsa.university_id, mu.name as university,YEAR(te.created_at) as year,te.gpa_evaluation,te.paid_leave,te.receive_another_scholarship,te.description,tsa.transcript_file,te.evaluation_result,te.created_at FROM tr_evaluation te join tr_scholarship_applicants tsa on tsa.id = te.applicants_id join m_university mu on mu.id = tsa.university_id";
    if ($year && $university) {
      $query = "SELECT te.id,te.applicants_id, tsa.name,tsa.university_id, mu.name as university,YEAR(te.created_at) as year,te.gpa_evaluation,te.paid_leave,te.receive_another_scholarship,te.description,tsa.transcript_file,te.evaluation_result,te.created_at FROM tr_evaluation te join tr_scholarship_applicants tsa on tsa.id = te.applicants_id join m_university mu on mu.id = tsa.university_id WHERE YEAR(te.created_at) = $year and mu.id = '$university'";
    }

    return $this->db->query($query);
  }
}

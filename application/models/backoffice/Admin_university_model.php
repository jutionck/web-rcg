<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_university_model extends CI_Model
{

  public function getUniversity($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_university', $id);
    } else {
      return $this->db->get('m_university');
    }
  }

  public function getUniversityByUser($where)
  {
    $this->db->select('a.id, a.name, b.username');
    $this->db->join('m_users b', 'b.university_id=a.id');
    return $this->db->get_where('m_university a', $where);
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_university', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_university', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_university', $id);
    return $this->db->affected_rows();
  }
}

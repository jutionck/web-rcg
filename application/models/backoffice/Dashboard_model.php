<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
  public function getInformation($where)
  {
    $this->db->select('m_users.*, m_university.name as university ');
    $this->db->join('m_university', 'm_university.id = m_users.university_id');
    return $this->db->get_where('m_users', ['username' => $where]);
  }
}

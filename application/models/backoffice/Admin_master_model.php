<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_master_model extends CI_Model
{

  public function getAbouts()
  {
    return $this->db->get('m_abouts');
  }

  public function save($data, $type = null)
  {
    if ($type == 'about') {
      $this->db->set('updated_at', date('Y-m-d H:i:s'));
      $this->db->update('m_abouts', $data, ['id' => $data['id']]);
    } else if ($type == 'benefit' && $data['id']) {
      $this->db->update('m_benefit', $data, ['id' => $data['id']]);
    } else if ($type == 'benefit') {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_benefit', $data);
    }
    return $this->db->affected_rows();
  }

  public function getBenefits($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_benefit', $id);
    } else {
      return $this->db->get('m_benefit');
    }
  }

  public function delete($id, $type = null)
  {
    if ($type == 'benefit') {
      $this->db->delete('m_benefit', $id);
    }
    return $this->db->affected_rows();
  }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approver_activity_model extends CI_Model
{
  public function activities($universityId = null, $year = null)
  {
    $this->_join();
    if ($year) {
      return $this->db->get_where('tr_student_activities a', ['c.id' => $universityId, 'YEAR(a.created_at)' => $year]);
    } else {
      return $this->db->get_where('tr_student_activities a', ['c.id' => $universityId, 'YEAR(a.created_at)' => date('Y')]);
    }
  }

  public function activityDetail($where = null)
  {
    $this->_join();
    $this->db->order_by('a.created_at', 'desc');
    return $this->db->get_where('tr_student_activities a', $where);
  }

  private function _join()
  {
    $this->db->select('a.id, a.applicants_id, b.name as name, a.activity_name, a.activity_date, a.attendance, a.activity_type, a.activity_reason, a.activity_resume, a.activity_file, a.created_at, c.id as university_id, c.name as university');
    $this->db->join('tr_scholarship_applicants b', 'b.id=a.applicants_id');
    $this->db->join('m_university c', 'c.id=b.university_id');
  }
}

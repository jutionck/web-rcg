<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_stage_model extends CI_Model
{

  public function getStages($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_scholarship_stages', $id);
    } else {
      return $this->db->get('m_scholarship_stages');
    }
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->update('m_scholarship_stages', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_scholarship_stages', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_scholarship_stages', $id);
    return $this->db->affected_rows();
  }
}

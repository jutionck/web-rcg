<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_faqs_model extends CI_Model
{

  public function getFaqs($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_faqs', $id);
    } else {
      return $this->db->get('m_faqs');
    }
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->update('m_faqs', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_faqs', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('m_faqs', $id);
    return $this->db->affected_rows();
  }
}

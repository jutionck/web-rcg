<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_contacts_model extends CI_Model
{

  public function getContacts($id = null)
  {
    if ($id) {
      return $this->db->get_where('m_contacts', $id);
    } else {
      return $this->db->get('m_contacts');
    }
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->update('m_contacts', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('m_contacts', $data);
    }
    return $this->db->affected_rows();
  }
}

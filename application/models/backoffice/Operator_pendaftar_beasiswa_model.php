<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operator_pendaftar_beasiswa_model extends CI_Model
{
  public function getPendaftar($id = null)
  {
    if ($id) {
      return $this->db->get_where('tr_scholarship_applicants', $id);
    } else {
      return $this->db->get('tr_scholarship_applicants');
    }
  }

  public function getPendaftarWithUniversity($where, $year = null)
  {
    $this->db->select('a.id, a.name, a.npm, a.faculty, a.major, a.gp, a.gpa, a.approval, b.name as university');
    if ($year) {
      $this->db->where('YEAR(a.created_at)', $year);
    } else {
      $this->db->where('YEAR(a.created_at)', date('Y'));
    }
    $this->db->join('m_university b', 'b.id=a.university_id');
    $this->db->order_by('a.created_at', 'desc');
    return $this->db->get_where('tr_scholarship_applicants a', $where);
  }

  public function getPendaftarFull($where = null)
  {
    $this->db->select('a.*, b.name as university, c.username');
    $this->_join();
    if ($where) {
      return $this->db->get_where('tr_scholarship_applicants a', $where);
    } else {
      return $this->db->get('tr_scholarship_applicants a');
    }
  }

  private function _join()
  {
    $this->db->join('m_university b', 'b.id=a.university_id');
    $this->db->join('m_users c', 'c.university_id=b.id');
  }

  public function getReligion()
  {
    return $this->db->get('m_religion')->result();
  }

  public function getBloodGroup()
  {
    return $this->db->get('m_blood_group')->result();
  }

  public function save($data)
  {
    if ($data['id']) {
      $this->db->update('tr_scholarship_applicants', $data, ['id' => $data['id']]);
    } else {
      $this->db->set('id', 'UUID()', FALSE);
      $this->db->insert('tr_scholarship_applicants', $data);
    }
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->delete('tr_scholarship_applicants', $id);
    return $this->db->affected_rows();
  }

  public function deleteRefInterview($id)
  {
    $this->db->delete('tr_interview_result', $id);
    return $this->db->affected_rows();
  }
}

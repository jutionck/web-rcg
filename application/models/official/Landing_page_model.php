<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing_page_model extends CI_Model
{

  public function GetAbout()
  {
    return $this->db->get('m_abouts');
  }

  public function GetBenefit()
  {
    return $this->db->get('m_benefit');
  }

  public function GetContact()
  {
    return $this->db->get('m_contacts');
  }

  public function GetFaqs($position = null)
  {
    return $this->db->get_where('m_faqs', ['position' => $position]);
  }

  public function GetScholarshipStages()
  {
    return $this->db->get('m_scholarship_stages');
  }

  public function GetUniversity()
  {
    return $this->db->get('m_university');
  }

  public function GetArticles($limit = null)
  {
    $this->db->select('id,title,slug,description,author,image,is_status,created_at');
    $this->db->limit($limit);
    $this->db->order_by('created_at', 'DESC');
    return $this->db->get('m_news');
  }

  public function GetArticleWithSlug($slug)
  {
    $this->db->select('id,title,slug,description,author,image,is_status,created_at,hits');
    $this->db->where('slug', $slug);
    return $this->db->get('m_news');
  }

  public function GetArticlesWithFilter($filter, $offset, $total, $pagination = null)
  {
    if ($filter) {
      $this->db->like('title', $filter);
    }

    if ($pagination == 'page') {
      $result['total_rows'] = $this->db->count_all_results('m_news');
    }
    $this->db->order_by('created_at', 'DESC');
    $query = $this->db->get('m_news', $total, $offset);
    $result['data'] = $query;
    return $result;
  }

  public function SetArticleHits($data)
  {
    $this->db->set('hits', 'hits+1', FALSE);
    $this->db->where('id', $data);
    $this->db->update('m_news');
  }
}

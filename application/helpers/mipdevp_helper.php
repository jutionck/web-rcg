<?php

function pageOfficial($page  = "", $data = "")
{
  $ci = get_instance();
  $ci->load->view('official/header', $data);
  $ci->load->view('/' . $page, $data);
  $ci->load->view('official/footer', $data);
}

function pageAuth($page  = "", $data = "")
{
  $ci = get_instance();
  $ci->load->view('backoffice/auth/header', $data);
  $ci->load->view('/' . $page, $data);
  $ci->load->view('backoffice/auth/footer', $data);
}

function pageAuthV2($page  = "", $data = "")
{
  $ci = get_instance();
  $ci->load->view('backoffice/auth/v2/header', $data);
  $ci->load->view('/' . $page, $data);
  $ci->load->view('backoffice/auth/v2/footer', $data);
}

function pageBackend($role = '', $page = '', $data = '')
{
  $ci = get_instance();
  $ci->load->view('backoffice/template/header', $data);
  $ci->load->view('backoffice/template/topbar', $data);
  $ci->load->view('backoffice/template/' . strtolower($role . '_sidebar'), $data);
  $ci->load->view('' . $page, $data);
  $ci->load->view('backoffice/template/footer');
}

function keyencrypt()
{
  return '123Daftar@@';
}

function encodeEncrypt($id)
{
  $ci = get_instance();
  return $ci->encryption->encrypt($id);
}

function decodeEncrypt($id)
{
  $ci = get_instance();
  return $ci->encryption->decrypt($id);
}

function sessionAdmin($role)
{
  $ci = get_instance();
  return $ci->session->userdata($role);
}

function cek_login($role = null)
{
  $ci = get_instance();
  if (!$ci->session->userdata('username')) {
    redirect('backoffice');
  }

  if ($role) {
    $sessionActive = $ci->session->userdata('username');
    if ($sessionActive->name != $role) {
      redirect('backoffice');
    }
  }
}

function calculateAge($birthDate)
{
  $birthDate = new DateTime($birthDate);
  $today = new DateTime("today");
  if ($birthDate > $today) {
    exit("0 tahun 0 bulan 0 hari");
  }
  $y = $today->diff($birthDate)->y;
  $m = $today->diff($birthDate)->m;
  $d = $today->diff($birthDate)->d;
  return $y . " tahun " . $m . " bulan " . $d . " hari";
}

function indonesianDate($date, $format)
{
  // 'dddd, D MMMM Y, HH:mm' => Selasa, 8 Februari 2022, 14:00
  $date = \Carbon\Carbon::parse($date, 'UTC')->locale('id');
  return $date->isoFormat($format);
}

function getPendaftarWithInterview($where = null)
{
  $ci = get_instance();
  $ci->db->select('
    a.id as interview_id, 
    a.gpa_score, 
    a.administration_score, 
    a.bank_central_score, 
    a.genbi_score, 
    a.organization_score, 
    a.scientific_work_score, 
    a.motivation_score, 
    a.result_score, 
    a.created_at as interview_date, 
    b.name, 
    b.gpa, 
    b.semester, 
    b.number_of_credits as sks,
    b.sktm_option as sktm, 
    c.name as university');
  $ci->db->join('tr_scholarship_applicants b', 'b.id = a.applicants_id');
  $ci->db->join('m_university c', 'c.id = b.university_id');
  if ($where) {
    return $ci->db->get_where('tr_interview_result a', ['a.applicants_id' => $where]);
  } else {
    return $ci->db->get('tr_interview_result a');
  }
}

function getTextPeriod($date)
{
  $today = date('Y-m-d');
  if ($today <= $date) {
    return '<span class="badge bg-success">Open</span>';
  } else {
    return '<span class="badge bg-danger">Closed</span>';
  }
}

function strContains($text, $needle)
{
  return str_contains($text, $needle);
}

function emailBody($acara, $tanggal)
{
  $body = "Kegiatan baru yang harus diikuti telah ditambahkan dengan detail : <br><br>
  <strong>Nama Kegiatan</strong> : $acara<br>
  <strong>Tanggal</strong> : " . indonesianDate($tanggal, 'dddd, D MMMM Y') . "<br><br>

  Silahkan cek detail dan laporkan kehadiran pada aplikasi<br>
  <a href='https://beasiswabilampung/backoffice' target='_blank'>https://beasiswabilampung/backoffice</a>";
  return $body;
}

function newSlug($data)
{
  $slug = url_title($data, 'dash', TRUE);
  return $slug;
}

function showYears($start, $end, $selected) {
  $tg_awal = date('Y') - $start;
  $tgl_akhir = date('Y') + $end;
  for ($i = $tgl_akhir; $i >= $tg_awal; $i--) {
    echo "<option value='$i'";
    if ($selected == $i) {
      echo "selected";
    }
    echo ">$i</option>";
  }
}

function likertScala($score) {
  $result = 0;
  switch ($score) {
  case 1:
    $result = 49;
    break;
  case 2:
    $result = 59;
    break;
  case 3:
    $result = 69;
    break;
  case 4:
    $result = 79;
    break;
  case 5:
    $result = 100;
    break;
  default:
    $result = 0;
  }
  return $result;
}
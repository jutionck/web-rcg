<?php
defined('BASEPATH') or exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('session', 'database', 'encryption', 'form_validation', 'email', 'upload', 'user_agent');
$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'mipdevp', 'text');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();

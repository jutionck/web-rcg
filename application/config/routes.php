<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Home
$route['default_controller'] = 'home_page';
// About
$route['about'] = 'about_page';
// Portofolio
$route['portofolio'] = 'portofolio_page';
// Roadmap
$route['roadmap'] = 'roadmap_page';
// Contact
$route['contact'] = 'contact_page';

// Members
$route['member/ruang-rehat'] = 'ruang_rehat_page';
$route['member/cipta-griya-kontraktor'] = 'cipta_griya_kontraktor_page';
$route['member/rcg-jogja'] = 'rcg_jogja_page';
$route['member/rcg-bandung'] = 'rcg_bandung_page';
$route['member/rcg-indoestri'] = 'rcg_indoestri_page';

$route['member/ruang-rehat/detail'] = 'ruang_rehat_page/detail';


$route['artikel'] = 'article_page';
$route['artikel/baca/(:any)'] = 'article_page/detail/$1';
$route['artikel/pencarian'] = 'article_page/search';
$route['artikel/cari_tidak_ditemukan'] = 'article_page/searchNotFound';

// BACKOFFICE
$route['backoffice'] = 'backoffice/authentication';
$route['backoffice/auth/login'] = 'backoffice/authentication/login';
$route['backoffice/auth/logout'] = 'backoffice/authentication/logout';

$route['backoffice/admin/dashboard'] = 'backoffice/admin_dashboard';
$route['backoffice/admin'] = 'backoffice/admin_dashboard';

$route['404_override'] = 'err_page';
$route['translate_uri_dashes'] = FALSE;
